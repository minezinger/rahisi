import React from 'react';

import { hashFnv32a } from 'utilities/general';

const renderLevy = ({ amount, currency, description }) => <li key={hashFnv32a(amount + '-' + currency + '-' + description, true)}>{currency} {amount} {description}</li>

const Levy = ({ currency, list }) => {
  return <div className="content">
    <ul style={{ marginTop: 0 }}>
      {list.map(item => renderLevy({ amount: item.amount, currency, description: item.description }))}
    </ul>
  </div>
}

export default Levy;