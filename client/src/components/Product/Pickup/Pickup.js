import React from 'react';
import moment from 'moment';

const renderOption = ({ id, pickupPoint, pickupTime }) => <option key={id} value={id}>
  {pickupPoint.name} - {moment(pickupTime, 'HH:mm:ss.SS').format('HH:mm').toString()}
</option>

const renderControl = ({ disabled, errors, name, onChange, onBlur, options, touched, value }, validationClass) => <div className="control">
  <div className={`select ${validationClass} is-fullwidth`}>
    <select
      disabled={disabled}
      name={name}
      onChange={onChange}
      onBlur={onBlur}
      value={value}>
      {options.map(option => renderOption(option))}
    </select>
    {touched && errors ? <div className="help is-danger">{errors}</div>  : null}
  </div>
</div>

const Pickup = (props) => {
  const { errors, fieldClass, label, touched, value } = props;

  let validationClass = '';
  if (touched && errors) {
    validationClass = 'is-danger';
  } else if (!errors && value) {
    validationClass = 'is-success';
  }

  return <div className={`field ${fieldClass && fieldClass}`}>
    {label ? <label className="label">{label}</label>  : null}
    {renderControl(props, validationClass)}
  </div>
}

export default Pickup;