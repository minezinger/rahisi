import React from 'react';
import { isEmpty } from 'lodash';

import Departure from 'components/Product/Departure/Departure';

const Flavour = ({ currency, details, parentId, passengers }) => {
  return <div className="has-background-white margin-bottom-large padded">
    <h4 className="title is-4">{details.productName}</h4>
    {/* <div>{details.requiredMultiple}</div>
    <div>{details.minUnits}</div>
    <div>{details.minPaxCount}</div>
    <div>{details.maxPaxCount}</div> */}
    <div style={{ maxHeight: '400px', overflowX: 'hidden', overflowY: 'auto' }}>
      {!isEmpty(details.departures) ? details.departures.map(departure => departure.availableUnits > 0 && departure.status === 'OPEN' ? <Departure
        currency={currency}
        details={departure}
        key={details.productId + '-' + departure.date}
        parentId={parentId}
        passengers={passengers}
        productId={details.productId}
        productType={details.productType.toLowerCase()}
      /> : null) : <div>There are no departures for this Flavour within your currently selected date range.</div>}
    </div>
  </div>
}

export default Flavour;