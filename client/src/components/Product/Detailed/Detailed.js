import React from 'react';
import { Link } from 'react-router-dom';
import { isEmpty } from 'lodash';
import moment from 'moment';

import { hashFnv32a } from 'utilities/general';

import CodeBlock from 'components/UI/CodeBlock/CodeBlock';
import Hero from 'components/UI/Hero/Hero';
import List from 'components/UI/List/List';

import Levy from 'components/Product/Levy/Levy';
import Cancellation from 'components/Product/Cancellation/Cancellation';

import iconCheck from 'assets/images/icons/check.png';

const renderOperatingDays = (operatingDaysStr) => {
  return <div className="column">
    <div className="tags">
      {operatingDaysStr.split(',').map(day => <span className="tag is-success is-large" key={day}>{day}</span>)}
    </div>
  </div>
}

const renderImages = (productName, images) => {
  return <div className="columns">
    {images.map(image => <div className="column has-text-centered" key={hashFnv32a(image.urlPreviewSecure, true)}>
      <img alt={productName} className="image" src={image.urlPreviewSecure} style={{ height: '153px' }} />
    </div>)}
  </div>
}

const Detailed = ({ criteria: { passengers, startDate }, product, response }) => {

  const date = startDate ? startDate : moment().add(1, 'day').format('YYYY-MM-DD');
  const paxCount = passengers ? passengers : 1;
  const link = '/be/product/departures/' + product.type.toLowerCase() + '/' + product.id + '/' + date + '/' + paxCount;

  const detailComponents = [];

  detailComponents.push({
    name: 'operating-days',
    content: <div className="columns is-variable is-8">
      <div className="column is-half">
        {product.operatingDays ? <p><strong>Operating Days</strong>: {product.operatingDays}</p> : null}
        {!isEmpty(product.operatingDaysStr) ? renderOperatingDays(product.operatingDaysStr) : null}
      </div>
      <div className="column is-half">
        <div className="explanation-block content">
          <h3>Operating days</h3>
          <p><strong>Operating days</strong> are expressed in two ways. The first is <var>operatingDays</var>, which shows which weekdays a product is operating on. It is encoded as a bit field integer (Monday=2<sup>0</sup>=1, Tuesday=2<sup>1</sup>=2, ... Sunday=2<sup>6</sup>=64, and Daily=1+2+4+8+16+32+64=127). Secondly, as <var>operatingDaysStr</var> which is a textual form of operating days, for example, "Mon,Wed,Fri".</p>
        </div>
      </div>
    </div>
  })

  if (!isEmpty(product.description)) {
    detailComponents.push({
      name: 'description',
      content: <div className="columns is-variable is-8">
        <div className="column is-half">
          {!isEmpty(product.images) && renderImages(product.name, product.images)}
          <h3 className="is-size-3">Description</h3>
          <div className="content">
            {product.description.split('\n\n').map(descr => <p key={hashFnv32a(descr, true)}>{descr}</p>)}
          </div>
        </div>
        <div className="column is-half">
          <div className="explanation-block content">
            <h3>Types of products</h3>
            <p>The are three types (<var>type</var>) of product available - "TOUR", "FLAVOUR", and "OPTION".</p>
            <p><strong>Tours</strong> are the main, top-level products. They contain the different variations of a Tour (Flavours) and any optional add-ons (Options) available. These will contain the information that is consistent across all variations of the Tour, such as the operator name, operator currency, images, cancellation policies, etc.</p>
            <p><strong>Flavours</strong> are the different variations of a Tour and constitute the bulk of our bookable products. They could represent different start times, languages for the Tour, etc. For example, the Flavours on a reef cruise might include "Snorkeling", "1 Introductory Dive", "2 Introductory Dives", and so on.</p>
            <p><strong>Options</strong> are optional extras that can be booked in addition to any bookable product which provides them. They cannot be booked on their own and must accompany a bookable product. For example, a DVD or photos of a skydive, or a digital tour guide booked together with a walking tour. It is not possible to only purchase the DVD without going on the skydive. Options always include a list of ids of the products they can be booked with called <var>masterProductIds</var>.</p>
          </div>
        </div>
      </div>
    })
  }

  if (!isEmpty(product.minAge) || !isEmpty(product.maxAge)) {
    detailComponents.push({
      name: 'age-requirements',
      content: <div className="columns is-variable is-8">
        <div className="column is-half">
          <h3 className="is-size-3">Age Requirements</h3>
          <p><strong>Minimum Age</strong>: {product.minAge ? product.minAge : 'No minimum age.'}</p>
          <p><strong>Maximum Age</strong>: {product.maxAge ? product.maxAge : 'No maximum age.'}</p>
        </div>
        <div className="column is-half">
          <div className="explanation-block content">
            <p><strong>Minimum Age</strong> (<var>minAge</var>): A minimum pax age for this product</p>
            <p><strong>Maximum Age</strong> (<var>maxAge</var>): A maximum pax age for this product</p>
          </div>
        </div>
      </div>
    })
  }

  if (product.requiredMultiple > 0 || product.minUnits > 0 || product.maxUnits > 0) {
    detailComponents.push({
      name: 'unit-requirements',
      content: <div className="columns is-variable is-8">
        <div className="column is-half">
          <h3 className="is-size-3">Unit Requirements</h3>
          <p><strong>Required Multiple</strong>: {product.requiredMultiple}</p>
          <p><strong>Minimum Units</strong>: {product.minUnits}</p>
          <p><strong>Maximum Units</strong> : {product.maxUnits ? product.maxUnits : 'No maximum units.'}</p>
        </div>
        <div className="column is-half">
          <div className="explanation-block content">
            <p><strong>Required Multiple</strong> (<var>requiredMultiple</var>): If this is set and >1, the number of units booked on this particular product must be a multiple of this value. Generally only used where the product is priced and sold per pax (i.e. 1 unit/pax) and each pax has to select the product individually.</p>
            <p><strong>Minimum Units</strong> (<var>minUnits</var>): The minimum number of units that needs to be booked together. Generally only used where the product is priced and sold per pax (i.e. 1 unit/pax) and each pax has to select the product individually.</p>
            <p><strong>Maximum Units</strong> (<var>maxUnits</var>): The maximum number of units that can be booked. If not specified, our maximum number of pax (not necessarily same as units!) still applies.</p>
          </div>
        </div>
      </div>
    })
  }

  if (product.minPaxCount > 0 || product.maxPaxCount > 0) {
    detailComponents.push({
      name: 'paxcount-requirements',
      content: <div className="columns is-variable is-8">
        <div className="column is-half">
          <h3 className="is-size-3">Passenger Count Requirements</h3>
          <p><strong>Minimum Pax Count</strong>: {product.minPaxCount}</p>
          <p><strong>Maximum Pax Count</strong>: {product.maxPaxCount}</p>
        </div>
        <div className="column is-half">
          <div className="explanation-block content">
            <p><strong>Minimum Pax Count</strong> (<var>minPaxCount</var>): The minimum number of pax per booked unit. For example, 1 or 2 for a tour with double bed accommodation, that is priced and sold "by the room", depending on whether it permits single occupancy. Always 1 if products is priced/sold individually per pax.</p>
            <p><strong>Maximum Pax Count</strong> (<var>maxPaxCount</var>): The maximum number of pax per booked unit. For example, 2 for a tour with double bed accommodation, that is priced and sold "by the room". Always 1 if product is priced/sold individually per pax.</p>
          </div>
        </div>
      </div>
    })
  }

  detailComponents.push({
    name: 'highlights',
    content: <div className="columns is-variable is-8">
      <div className="column is-half">
        {!isEmpty(product.highlightsStr) ? <div className="margin-bottom">
          <h3 className="is-size-3">Highlights</h3>
          <List icon={iconCheck} list={product.highlightsStr.split('\n')} />
        </div> : null}
        {!isEmpty(product.includes) ? <div>
          <h3 className="is-size-3">Includes</h3>
          <List icon={iconCheck} list={product.includes.split('\n')} />
        </div> : null}
      </div>
      <div className="column is-half">
        <div className="explanation-block content">
          <h3>Displaying highlights and/or includes</h3>
          <p><strong>Highlights</strong> (<var>highlightsStr</var>) and includes (<var>includes</var>) are both written as lists - each a few words or a short sentence separated by a line break written as <var>\n</var>.</p>
          <p>Highlights present features of the tour that are particularly attractive or stand out - the best features in the sense of "touristic value". Each is a short line summary, emphasising what makes the tour distinctive.</p>
          <p>The purpose of the <strong>includes</strong> field is to explain all of the features that the tour has included in the cost price of the package. It is a list of services such as transport, meals, accommodation, 3rd party fees, etc.</p>
        </div>
      </div>
    </div>
  })

  if (!isEmpty(product.itineraryStr)) {
    detailComponents.push({
      name: 'itinerary',
      content: <div className="columns is-variable is-8">
        <div className="column is-half">
          <h3 className="is-size-3">Itinerary</h3>
          <div className="content">
            {product.itineraryStr.split('\n\n').map(itinerary => <p key={hashFnv32a(itinerary, true)}>
              {itinerary.split('\n').map((item, itemIndex) => <span key={itemIndex}>{item}<br /></span>)}
            </p>)}
          </div>
        </div>
        <div className="column is-half">
          <div className="explanation-block content">
            <h3>Using the itinerary</h3>
            <p>The purpose of the <strong>itinerary</strong> (<var>itineraryStr</var>) is to present a brief schedule explaining what occurs day-by-day on the tour. Typically, it is divided  according to day, time, and places to be visited that day in chronological order.</p>
          </div>
        </div>
      </div>
    })
  }

  if (!isEmpty(product.pickupNotes) && !isEmpty(product.dropoffNotes)) {
    detailComponents.push({
      name: 'pick-up-notes',
      content: <div className="columns is-variable is-8">
        <div className="column is-half">
          <h3 className="is-size-3">Pick-up/Drop-off Information</h3>
          <p><strong>Pick-up Notes</strong>: {product.pickupNotes}</p>
          <p><strong>Drop-off Notes</strong>: {product.dropoffNotes}</p>
        </div>
        <div className="column is-half">
          <div className="explanation-block content">
            <h3>Using pick-up/drop-off data</h3>
            <p><strong>Pick-up Notes</strong> (<var>pickupNotes</var>): Any special notes on pickup, departure etc. For example, "Please be ready to leave at least 15m prior to departure."</p>
            <p><strong>Drop-off Notes</strong> (<var>dropoffNotes</var>): Description of any drop off policy. For example, "Tour ends where it starts" or "Drop off at any of the pickup points".</p>
          </div>
        </div>
      </div>
    })
  }

  detailComponents.push({
    name: 'time-information',
    content: <div className="columns is-variable is-8">
      <div className="column is-half">
        <h3 className="is-size-3">Time and Duration</h3>
        {product.startTime ? <p>
          <strong>Start Time</strong>: {moment(product.startTime, 'HH:mm:ss.SS').format('HH:mm').toString()}
        </p> : null}
        {product.duration ? <p>
          <strong>Duration</strong>: {moment.duration(product.duration).humanize()}
        </p> : null}
      </div>
      <div className="column is-half">
        <div className="explanation-block content">
          <h3>Time and duration information</h3>
          <p><strong>Start time</strong> (<var>startTime</var>): The product's local departure time. This is the default start time for products that do not offer courtesy pickups or for customers that prefer to make their own way to the common meeting and departure point</p>
          <p><strong>Duration</strong> (<var>duration</var>): The duration of the product in milliseconds.</p>
        </div>
      </div>
    </div>
  })

  detailComponents.push({
    name: 'location-information',
    content: <div className="columns is-variable is-8">
      <div className="column is-half">
        <h3 className="is-size-3">Start/End Locations</h3>
        <p><strong>Start Location</strong>: {product.locationStart}</p>
        <p>Coordinates for this start location are also available: {product.latitudeStart}, {product.longitudeStart}</p>
        <p><strong>End Location</strong>: {product.locationEnd}</p>
        <p>Coordinates for this start location are also available: {product.latitudeEnd}, {product.longitudeEnd}</p>
      </div>
      <div className="column is-half">
        <div className="explanation-block content">
          <h3>Location information</h3>
          <p><strong>Start location</strong> (<var>locationStart</var>, <var>latitudeStart</var>, <var>longitudeStart</var>): The product's start location as text, as latitude, and as longitude. Please note that while we try to pin-point the exact start location of the product as closely aspossible, the latitude and longitude fields may only be an approximation.</p>
          <p><strong>End location</strong> (<var>locationEnd</var>, <var>latitudeEnd</var>, <var>longitudeEnd</var>): The product's end location as text, as latitude, and as longitude. Please note that while we try to pin-point the exact end location of the product as closely aspossible, the latitude and longitude fields may only be an approximation.</p>
        </div>
      </div>
    </div>
  })

  if (!isEmpty(product.specialNotes)) {
    detailComponents.push({
      name: 'special-notes',
      content: <div className="columns is-variable is-8">
        <div className="column is-half">
          <h3 className="is-size-3">Special Notes</h3>
          <div className="content">
            <dl>
              {product.specialNotes.split('\n\n').map(note => note.split('\n').map(item => item[item.trim().length - 1] === ':' ? <dt key={hashFnv32a(item, true)}><strong>{item}</strong></dt> : <dd key={hashFnv32a(item, true)}>{item}</dd>))}
            </dl>
          </div>
        </div>
        <div className="column is-half">
          <div className="explanation-block content">
            <h3>Displaying special notes</h3>
            <p>Typically, the <strong>special notes</strong> (<var>specialNotes</var>) will include any pieces of information that does not fit into the other fields. Usually it will be about explaining what the consumer needs to bring along/be aware of/any extraneous conditions.</p>
          </div>
        </div>
      </div>
    })
  }

  if (!isEmpty(product.levies)) {
    detailComponents.push({
      name: 'levies',
      content: <div className="columns is-variable is-8">
        <div className="column is-half">
          <h3 className="is-size-3">Levies</h3>
          <Levy currency={product.operatorCurrency} list={product.levies} />
        </div>
        <div className="column is-half">
          <div className="explanation-block content">
            <h3>Using levies</h3>
            <p><strong>Levies</strong> (<var>levies</var>) are all extra costs which are not included in the tour price. They need to be <strong>displayed in the currency that the operator uses</strong> because they are paid on arrival at the start of a tour. For example, even though an agent may be looking at their tours in AUD, if the operator resides in the US and there are levies on their products, the levies will have to be paid in USD. This is the only point where the price will need to be displayed in a currency other than the one set up for the Outlet Group.</p>
          </div>
        </div>
      </div>
    })
  }

  if (!isEmpty(product.cancellationPolicy)) {
    detailComponents.push({
      name: 'cancellation-policy',
      content: <div className="columns is-variable is-8">
        <div className="column is-half">
          <h3 className="is-size-3">Cancellation Fees</h3>
          <Cancellation list={product.cancellationPolicy} />
        </div>
        <div className="column is-half">
          <div className="explanation-block content">
            <h3>Displaying cancellation fees</h3>
            <p>The <strong>cancellation policy</strong> (<var>cancellationPolicy</var>) is an array of objects that contain a percentage which determines the fee amount (<var>feePerc</var>) and a number of hours that determine when the percentage is applicaple (<var>hoursTillDeparture</var>).</p>
            <p>For an example, see the cancellation fees to the left. Here we have converted the number of hours into something more human readable. The amount of time before a departure is the...</p>
          </div>
        </div>
      </div>
    })
  }

  detailComponents.push({
    name: 'availability-link',
    content: <div className="has-text-centered">
      <Link className="button is-link is-large" to={link}>Check availability</Link>
    </div>
  })

  return <div>
    <Hero containerClass="has-text-centered" type="primary">
      <h1 className="title is-size-2">{product.name}</h1>
      <h2 className="subtitle is-size-4">{product.operatorName}</h2>
    </Hero>

    <div className="section">
      <div className="container">
        <div className="explanation-block">
          <div className="columns is-variable is-8">
            <div className="column is-half">
              <CodeBlock code={response} height="500" language="json" />
            </div>
            <div className="column is-half content">
              <h3>Getting product details</h3>
              <p>An individual product can then be retrieved via <code>GET /products/&#123;productId&#125;</code>. The response for this particular call - for <em>{product.name}</em> with a product id of <var>{product.id}</var> - is shown below.</p>
              <p>More information regarding some of the different values within this response are explained below. When you wish to move forward, just click "Check availability" at the top or bottom of the page.</p>
              <p>You can either scroll down to view the details of the product with information about how to use them and what they represent, or click "Check availability" below to jump to the next step.</p>
              <div className="has-text-centered">
                <Link className="button is-link" to={link}>Check availability</Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    {detailComponents.map((component, index) => {
      if (index % 2 === 0) {
        return <Hero key={component.name} type="light">
          {component.content}
        </Hero>
      } else {
        return <div key={component.name} className="section">
          <div className="container">
            {component.content}
          </div>
        </div>
      }
    })}
  </div>
}

export default Detailed;