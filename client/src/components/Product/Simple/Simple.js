import React from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment';

const Simple = ({ criteria: { passengers, startDate }, currency, product }) => {
  const date = startDate ? startDate : moment().add(1, 'day').format('YYYY-MM-DD');
  const paxCount = passengers ? passengers : 1;
  const adultPricing = product.minRateAdult && product.maxRateAdult ? <div><strong>Adults</strong>&nbsp;from {currency} {product.minRateAdult}</div> : null;
  const childPricing = product.minRateChild && product.maxRateChild ? <div><strong>Children</strong>&nbsp;from {currency} {product.minRateChild}</div> : null;

  return <div className="columns is-variable is-vcentered has-background-white margin-bottom-large">
    <div className="column is-narrow is-paddingless">
      <img alt={product.name + ' by ' + product.operatorName} className="image" src={product.images[0].urlPreviewSecure} style={{ height: '200px' }} />
    </div>
    <div className="column content">
      <h3 className="is-marginless">{product.name}</h3>
      <p>By {product.operatorName}</p>

      <div className="tags">
        {product.operatingDaysStr.split(',').map(day => <span className="tag is-success" key={day}>{day}</span>)}
      </div>
    </div>
    <div className="column is-one-fifth content has-text-right">
      {adultPricing}
      {childPricing}
      <Link className="button is-link margin-top" to={`/be/product/details/${product.type.toLowerCase()}/${product.id}/${date}/${paxCount}`}>More information</Link>
    </div>
  </div>
}

export default Simple;