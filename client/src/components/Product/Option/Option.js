import React from 'react';
import { isEmpty } from 'lodash';
import moment from 'moment';

import Checkbox from 'components/UI/Checkbox/Checkbox';

const Option = ({ currency, details, errors, handleBlur, handleChange, optionIndex, touched, values }) => {
  const { departure } = details;

  return <div className="has-background-white margin-bottom-large padded">
    <h4 className="title is-4">{details.productName.trim()}, {moment(details.productStartTime, 'HH:mm:ss.SSS').format('HH:mm')}</h4>
    <div key={details.productId + '-' + departure.date}>
      <div className="columns" style={{ marginBottom: 0 }}>
        <div className="column is-two-fifths">
          <strong>{moment(departure.date).format('DD MMMM YYYY')}</strong>
        </div>
        <div className="column">
          Status: <span className="has-text-success">{departure.status}</span>
        </div>
        <div className="column">
          Available Units: {departure.availableUnits}
        </div>
      </div>
      <div className="columns" style={{ marginBottom: 0 }}>
        <div className="column">
          <div><u>Net Rates</u></div>
          {departure.departureRates.retailNetRateAdult ? <div>Adults: {currency} {departure.departureRates.retailNetRateAdult}</div> : null}
          {departure.departureRates.retailNetRateChild ? <div>Children: {currency} {departure.departureRates.retailNetRateChild}</div> : null}
        </div>
        <div className="column">
          <div><u>Commission Rates</u></div>
          {departure.departureRates.retailCommissionAmountAdult ? <div>Adults: {currency} {departure.departureRates.retailCommissionAmountAdult}</div> : null}
          {departure.departureRates.retailCommissionAmountChild ? <div>Children: {currency} {departure.departureRates.retailCommissionAmountChild}</div> : null}
        </div>
        <div className="column">
          <div><u>Gross Rates</u></div>
          {departure.departureRates.retailRateAdult ? <div>Adults: {currency} {departure.departureRates.retailRateAdult}</div> : null}
          {departure.departureRates.retailRateChild ? <div>Children: {currency} {departure.departureRates.retailRateChild}</div> : null}
        </div>
      </div>
      <div className="columns margin-bottom">
        {values.cartItems.map(cartItem => <div className="column is-narrow" key={`${details.productId}-${departure.date}-${cartItem.paxIndex}`}>
          <Checkbox
            className="button is-primary"
            errors={!isEmpty(errors.cartItems) ? errors.cartItems : undefined}
            label={`Passenger ${cartItem.paxIndex + 1}`}
            name={`cartItems[${cartItem.paxIndex}].options[${optionIndex}].selected`}
            onChange={handleChange}
            onBlur={handleBlur}
            touched={!isEmpty(touched.cartItems) ? touched.cartItems : undefined}
            value={`${details.productId}-${departure.date}-${cartItem.paxIndex}`}
          />
        </div>)}
      </div>
    </div>
  </div>
}

export default Option;