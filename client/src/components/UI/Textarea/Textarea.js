import React from 'react';

const renderControl = ({ disabled, errors, name, onChange, onBlur, placeholder, touched, value }, validationClass) => <div className="control">
  <textarea
    className={`textarea ${validationClass}`}
    disabled={disabled}
    name={name}
    onChange={onChange}
    onBlur={onBlur}
    placeholder={placeholder}
    value={value}>
  </textarea>
  {touched && errors ? <div className="help is-danger">{errors}</div> : null}
</div>

const Textarea = ({ disabled, errors, fieldClass, label, name, onChange, onBlur, placeholder, touched, value }) => {
  let validationClass = '';
  if (touched && errors) {
    validationClass = 'is-danger';
  } else if (!errors && value) {
    validationClass = 'is-success';
  }

  return <div className={`field ${fieldClass && fieldClass}`}>
    {label ? <label className="label">{label}</label> : null}
    {renderControl({ disabled, errors, name, onChange, onBlur, placeholder, touched, value }, validationClass)}
  </div>
}

export default Textarea;