import React from 'react';

const Hero = ({ bold, children, containerClass, foot, head, size, type }) => {
  return <div className={`hero ${bold ? 'is-bold' : ''} ${size ? 'is-' + size : ''} ${type ? 'is-' + type : ''}`}>
    {head ? <div className="hero-head">{head}</div> : null}
    <div className="hero-body">
      <div className={`container ${containerClass}`}>
        {children}
      </div>
    </div>
    {foot ? <div className="hero-foot">{foot}</div> : null}
  </div>
}

export default Hero;
