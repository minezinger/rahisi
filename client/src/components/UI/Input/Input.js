import React from 'react';

const renderIcon = (icon, pos) => <span className={`icon is-small is-${pos}`}>
  {/* TODO: Render an icon here */}
</span>

const renderControl = ({ disabled, errors, iconLeft, iconRight, name, onChange, onBlur, placeholder, touched, type, value }, validationClass) => <div className={`control ${iconLeft && 'has-icons-left'} ${iconRight && 'has-icons-right'}`}>
  <input
    className={`input ${validationClass}`}
    disabled={disabled}
    name={name}
    onChange={onChange}
    onBlur={onBlur}
    placeholder={placeholder}
    type={type}
    value={value}
  />
  {iconLeft ? renderIcon(iconLeft, 'left') : null}
  {iconRight ? renderIcon(iconRight, 'right') : null}
  {touched && errors ? <div className="help is-danger">{errors}</div> : null}
</div>

const Input = (props) => {
  const { errors, fieldClass, horizontal, label, touched, value } = props;

  let validationClass = '';
  if (touched && errors) {
    validationClass = 'is-danger';
  } else if (!errors && value) {
    validationClass = 'is-success';
  }

  if (horizontal) {
    return <div className={`field ${fieldClass && fieldClass} is-horizontal`}>
      <div className="field-label is-normal">
        {label ? <label className="label">{label}</label> : null}
      </div>
      <div className="field-body">
        <div className="field">
          {renderControl(props, validationClass)}
        </div>
      </div>
    </div>
  }

  return <div className={`field ${fieldClass && fieldClass}`}>
    {label ? <label className="label">{label}</label> : null}
    {renderControl(props, validationClass)}
  </div>
}

export default Input;