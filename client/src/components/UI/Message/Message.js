import React from 'react';

const Message = ({ children, closeable, header, type }) => {
  return <article className={`message ${type ? 'is-' + type : ''}`}>
    {header ? <div className="message-header">
      <p>{header}</p>
      {closeable ? <button className="delete" aria-label="delete"></button> : null}
    </div> : null}
    <div className="message-body">
      <div className="content">
        {children}
      </div>
    </div>
  </article>
}

export default Message;