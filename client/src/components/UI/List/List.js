import React from 'react';

import { hashFnv32a } from 'utilities/general';

import './List.css';

const List = ({ icon, list }) => {
  const iconStyleLI = {
    background: `url(${icon}) no-repeat left 4px`
  }

  return <div className="content">
    <ul className="icon-style-ul">
      {list.map(item => <li className="icon-style-li" style={icon ? iconStyleLI : {}} key={hashFnv32a(item, true)}>
        {item}
      </li>)}
    </ul>
  </div>
}

export default List;