import React from 'react';

import './Notification.css';

const Notification = ({ children, closeable, contained, spinner, type }) => {
  const notification = <div className={`notification ${type ? 'is-' + type : 'is-info'} is-clearfix`}>
    {closeable ? <button className="delete"></button> : null}
    {spinner ? <div className="spinner is-pulled-left"></div> : null}
    {children}
  </div>;

  if (contained) {
    return <div className="section">
      <div className="container">
        {notification}
      </div>
    </div>
  } else {
    return notification;
  }
}

export default Notification;