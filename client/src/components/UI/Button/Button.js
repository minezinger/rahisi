import React from 'react';

const Button = ({ children, className, clickEvent, disabled, type }) => {
  return <button className={`button ${className ? className : ''}`} disabled={disabled} onClick={clickEvent} type={type}>
    {children}
  </button>
}

export default Button;