import React from 'react';
import SyntaxHighlighter from 'react-syntax-highlighter';

import style from './style.js';

const CodeBlock = ({ code, height, language }) => {
  return <div style={height ? { height: height + 'px', overflow: 'auto', resize: 'vertical' } : {}}>
    <SyntaxHighlighter language={language} style={style}>{code}</SyntaxHighlighter>
  </div>
}

export default CodeBlock;