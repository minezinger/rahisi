import React from 'react';
import { Link } from 'react-router-dom';

import Button from 'components/UI/Button/Button';
import Hero from 'components/UI/Hero/Hero';

import Item from 'components/Reservation/Item/Item';

const Detailed = ({ details: { globalRef, grandTotalRetailOverride, id, reservationItems, retailCurrency }, getTickets }) => {
  return <div>
    <Hero containerClass="has-text-centered" type="primary">
      <h1 className="title is-size-2">Reservation</h1>
      <div className="columns is-size-4">
        <div className="column">
          Reference: {globalRef}
        </div>
        <div className="column">
          Total: {retailCurrency} {grandTotalRetailOverride}
        </div>
      </div>
    </Hero>

    <div className="section">
      <div className="container">
        {reservationItems.map(item => <Item currency={retailCurrency} details={item} fullDetails={true} key={item.id} />)}

        <div className="field is-grouped is-grouped-right margin-top">
          <div className="control">
            <Link className="button is-link" to="/be/reservations">Back to Reservations</Link>
          </div>
          <div className="control">
            <Button className="is-primary" clickEvent={() => { getTickets(id) }} type="button">Download tickets</Button>
          </div>
        </div>
      </div>
    </div>
  </div>
}

export default Detailed;