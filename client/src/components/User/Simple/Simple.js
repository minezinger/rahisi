import React from 'react';
import { Link } from 'react-router-dom';

const UserSimple = ({ details: { email, id, name, role } }) => {
  return <div className="has-background-white content margin-bottom-large padded-large">
    <h3>{name}</h3>
    <div>
      <strong>Email:</strong> {email}
    </div>
    <div>
      <strong>Role:</strong> {role}
    </div>
    <div className="field is-grouped is-grouped-right">
      <div className="control">
        <Link className="button is-link" to={`/be/user/${id}`}>More Details</Link>
      </div>
    </div>
  </div>
}

export default UserSimple;