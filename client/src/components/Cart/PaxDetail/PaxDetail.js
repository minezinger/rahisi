import React from 'react';
import { isEmpty } from 'lodash';

import Input from 'components/UI/Input/Input';
import Select from 'components/UI/Select/Select';
import Textarea from 'components/UI/Textarea/Textarea';

const PaxDetail = ({ countries, errors, handleBlur, handleChange, languages, passenger, paxDetails, touched, values }) => {
  const index = passenger.paxIndex;
  const thisPaxError = !isEmpty(errors.paxes) ? errors.paxes[index] : {};
  const thisPaxTouched = !isEmpty(touched.paxes) ? touched.paxes[index] : {};
  const thisPaxValues = !isEmpty(values.paxes) ? values.paxes[index] : {};
  const thisCartItemError = !isEmpty(errors.cartItems) ? errors.cartItems[index] : {};
  const thisCartItemTouched = !isEmpty(touched.cartItems) ? touched.cartItems[index] : {};
  const thisCartItemValues = !isEmpty(values.cartItems) ? values.cartItems[index] : {};

  return <div className="has-background-white margin-bottom-large padded-large">
    <h4 className="is-size-4">Passenger {index + 1}</h4>

    <div className="columns is-variable is-3">
      {paxDetails.name ? <div className="column">
        <Select
          errors={!isEmpty(thisPaxError) ? thisPaxError.salutation : undefined}
          label="Salutation"
          name={`paxes[${index}].salutation`}
          onChange={handleChange}
          onBlur={handleBlur}
          options={[{
            text: 'Miss',
            value: 'MISS'
          }, {
            text: 'Mr',
            value: 'MR'
          }, {
            text: 'Mrs',
            value: 'MRS'
          }, {
            text: 'Ms',
            value: 'MS'
          }]}
          touched={!isEmpty(thisPaxTouched) ? thisPaxTouched.salutation : undefined}
          value={!isEmpty(thisPaxValues) ? thisPaxValues.salutation : undefined}
        />
      </div> : null}
      {paxDetails.name ? <div className="column">
        <Input
          errors={!isEmpty(thisPaxError) ? thisPaxError.firstName : undefined}
          label="First Name"
          name={`paxes[${index}].firstName`}
          onChange={handleChange}
          onBlur={handleBlur}
          touched={!isEmpty(thisPaxTouched) ? thisPaxTouched.firstName : undefined}
          type="text"
          value={!isEmpty(thisPaxValues) ? thisPaxValues.firstName : undefined}
        />
      </div> : null}
      {paxDetails.name ? <div className="column">
        <Input
          errors={!isEmpty(thisPaxError) ? thisPaxError.lastName : undefined}
          label="Last Name"
          name={`paxes[${index}].lastName`}
          onChange={handleChange}
          onBlur={handleBlur}
          touched={!isEmpty(thisPaxTouched) ? thisPaxTouched.lastName : undefined}
          type="text"
          value={!isEmpty(thisPaxValues) ? thisPaxValues.lastName : undefined}
        />
      </div> : null}
    </div>

    <div className="columns is-variable is-3">
      <div className="column">
        <Input
          errors={!isEmpty(thisPaxError) ? thisPaxError.age : undefined}
          label="Age"
          name={`paxes[${index}].age`}
          onChange={handleChange}
          onBlur={handleBlur}
          touched={!isEmpty(thisPaxTouched) ? thisPaxTouched.age : undefined}
          type="number"
          value={!isEmpty(thisPaxValues) ? thisPaxValues.age : undefined}
        />
      </div>
      {paxDetails.dob ? <div className="column">
        <Input
          errors={!isEmpty(thisPaxError) ? thisPaxError.dob : undefined}
          label="D.O.B."
          name={`paxes[${index}].dob`}
          onChange={handleChange}
          onBlur={handleBlur}
          placeholder="YYYY-MM-DD"
          touched={!isEmpty(thisPaxTouched) ? thisPaxTouched.dob : undefined}
          type="text"
          value={!isEmpty(thisPaxValues) ? thisPaxValues.dob : undefined}
        />
      </div> : null}
      {paxDetails.country ? <div className="column">
        <Select
          errors={!isEmpty(thisPaxError) ? thisPaxError.country : undefined}
          label="Country"
          name={`paxes[${index}].country`}
          onChange={handleChange}
          onBlur={handleBlur}
          options={countries}
          optionText="names.en"
          optionValue="code"
          touched={!isEmpty(thisPaxTouched) ? thisPaxTouched.country : undefined}
          value={!isEmpty(thisPaxValues) ? thisPaxValues.country : undefined}
        />
      </div> : null}
      {paxDetails.language ? <div className="column">
        <Select
          errors={!isEmpty(thisPaxError) ? thisPaxError.language : undefined}
          label="Language"
          name={`paxes[${index}].language`}
          onChange={handleChange}
          onBlur={handleBlur}
          options={languages}
          optionText="names.en"
          optionValue="code"
          touched={!isEmpty(thisPaxTouched) ? thisPaxTouched.language : undefined}
          value={!isEmpty(thisPaxValues) ? thisPaxValues.language : undefined}
        />
      </div> : null}
      {paxDetails.nationality ? <div className="column">
        <Select
          errors={!isEmpty(thisPaxError) ? thisPaxError.nationality : undefined}
          label="Nationality"
          name={`paxes[${index}].country`}
          onChange={handleChange}
          onBlur={handleBlur}
          options={countries}
          optionText="names.en"
          optionValue="code"
          touched={!isEmpty(thisPaxTouched) ? thisPaxTouched.nationality : undefined}
          value={!isEmpty(thisPaxValues) ? thisPaxValues.nationality : undefined}
        />
      </div> : null}
    </div>

    {paxDetails.passport ? <div className="columns is-variable is-3">
      <div className="column is-one-fifth">
        <Input
          errors={!isEmpty(thisPaxError) ? thisPaxError.passportNumber : undefined}
          label="Passport Number"
          name={`paxes[${index}].passportNumber`}
          onChange={handleChange}
          onBlur={handleBlur}
          touched={!isEmpty(thisPaxTouched) ? thisPaxTouched.passportNumber : undefined}
          type="text"
          value={!isEmpty(thisPaxValues) ? thisPaxValues.passportNumber : undefined}
        />
      </div>
      <div className="column">
        <Input
          errors={!isEmpty(thisPaxError) ? thisPaxError.passportIssued : undefined}
          label="Passport Issue Date"
          name={`paxes[${index}].passportIssued`}
          onChange={handleChange}
          onBlur={handleBlur}
          touched={!isEmpty(thisPaxTouched) ? thisPaxTouched.passportIssued : undefined}
          type="text"
          value={!isEmpty(thisPaxValues) ? thisPaxValues.passportIssued : undefined}
        />
      </div>
      <div className="column">
        <Input
          errors={!isEmpty(thisPaxError) ? thisPaxError.passportExpiry : undefined}
          label="Passport Expiry Date"
          name={`paxes[${index}].passportExpiry`}
          onChange={handleChange}
          onBlur={handleBlur}
          touched={!isEmpty(thisPaxTouched) ? thisPaxTouched.passportExpiry : undefined}
          type="text"
          value={!isEmpty(thisPaxValues) ? thisPaxValues.passportExpiry : undefined}
        />
      </div>
    </div> : null}

    <div className="columns is-variable is-3">
      {paxDetails.email ? <div className="column">
        <Input
          errors={!isEmpty(thisPaxError) ? thisPaxError.email : undefined}
          label="Email"
          name={`paxes[${index}].email`}
          onChange={handleChange}
          onBlur={handleBlur}
          touched={!isEmpty(thisPaxTouched) ? thisPaxTouched.email : undefined}
          type="email"
          value={!isEmpty(thisPaxValues) ? thisPaxValues.email : undefined}
        />
      </div> : null}
      {paxDetails.phone ? <div className="column">
        <Input
          errors={!isEmpty(thisPaxError) ? thisPaxError.phone : undefined}
          label="Phone"
          name={`paxes[${index}].phone`}
          onChange={handleChange}
          onBlur={handleBlur}
          touched={!isEmpty(thisPaxTouched) ? thisPaxTouched.phone : undefined}
          type="tel"
          value={!isEmpty(thisPaxValues) ? thisPaxValues.phone : undefined}
        />
      </div> : null}
      {paxDetails.mobile ? <div className="column">
        <Input
          errors={!isEmpty(thisPaxError) ? thisPaxError.mobile : undefined}
          label="Mobile"
          name={`paxes[${index}].mobile`}
          onChange={handleChange}
          onBlur={handleBlur}
          touched={!isEmpty(thisPaxTouched) ? thisPaxTouched.mobile : undefined}
          type="tel"
          value={!isEmpty(thisPaxValues) ? thisPaxValues.mobile : undefined}
        />
      </div> : null}
    </div>

    {paxDetails.address ? <div className="columns is-variable is-3">
      <div className="column is-two-fifths">
        <Input
          errors={!isEmpty(thisPaxError) ? thisPaxError.address1 : undefined}
          label="Address"
          name={`paxes[${index}].address1`}
          onChange={handleChange}
          onBlur={handleBlur}
          touched={!isEmpty(thisPaxTouched) ? thisPaxTouched.address1 : undefined}
          type="text"
          value={!isEmpty(thisPaxValues) ? thisPaxValues.address1 : undefined}
        />
      </div>
      <div className="column is-one-fifth">
        <Input
          errors={!isEmpty(thisPaxError) ? thisPaxError.city : undefined}
          label="City"
          name={`paxes[${index}].city`}
          onChange={handleChange}
          onBlur={handleBlur}
          touched={!isEmpty(thisPaxTouched) ? thisPaxTouched.city : undefined}
          type="text"
          value={!isEmpty(thisPaxValues) ? thisPaxValues.city : undefined}
        />
      </div>
      <div className="column">
        <Input
          errors={!isEmpty(thisPaxError) ? thisPaxError.state : undefined}
          label="State"
          name={`paxes[${index}].state`}
          onChange={handleChange}
          onBlur={handleBlur}
          touched={!isEmpty(thisPaxTouched) ? thisPaxTouched.state : undefined}
          type="text"
          value={!isEmpty(thisPaxValues) ? thisPaxValues.state : undefined}
        />
      </div>
      <div className="column">
        <Input
          errors={!isEmpty(thisPaxError) ? thisPaxError.postcode : undefined}
          label="Postcode"
          name={`paxes[${index}].postcode`}
          onChange={handleChange}
          onBlur={handleBlur}
          touched={!isEmpty(thisPaxTouched) ? thisPaxTouched.postcode : undefined}
          type="text"
          value={!isEmpty(thisPaxValues) ? thisPaxValues.postcode : undefined}
        />
      </div>
    </div> : null}

    <div className="columns is-variable is-3">
      <div className="column is-half">
        <Textarea
          errors={!isEmpty(thisCartItemError) ? thisCartItemError.paxNote : undefined}
          label="Notes for the passenger"
          name={`cartItems[${index}].paxNote`}
          onChange={handleChange}
          onBlur={handleBlur}
          touched={!isEmpty(thisCartItemTouched) ? thisCartItemTouched.paxNote : undefined}
          value={!isEmpty(thisCartItemValues) ? thisCartItemValues.paxNote : undefined}
        />
      </div>
      <div className="column is-half">
        <Textarea
          errors={!isEmpty(thisCartItemError) ? thisCartItemError.operatorNote : undefined}
          label="Notes for the operator"
          name={`cartItems[${index}].operatorNote`}
          onChange={handleChange}
          onBlur={handleBlur}
          touched={!isEmpty(thisCartItemTouched) ? thisCartItemTouched.operatorNote : undefined}
          value={!isEmpty(thisCartItemValues) ? thisCartItemValues.operatorNote : undefined}
        />
      </div>
    </div>

  </div>
}

export default PaxDetail;