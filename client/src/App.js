import React, { Component } from 'react';
import { connect } from 'react-redux';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import PrivateRoute from 'PrivateRoute';
import Index from 'containers/Index';

class App extends Component {
  render() {
    const { user: { isAuthenticated } } = this.props;

    return <BrowserRouter>
      <Switch>
        <PrivateRoute component={Index} isAuthenticated={isAuthenticated} path="/be" />
        <Route component={Index} path="/" />
      </Switch>
    </BrowserRouter>
  }
}

const mapStateToProps = state => ({
  user: state.user
})

export default connect(mapStateToProps)(App);