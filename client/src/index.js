import React from 'react';
import { render } from 'react-dom';
import registerServiceWorker from './registerServiceWorker';

import App from './App';
import Root from './Root';

import './assets/styles/theme.css';
import './assets/styles/general.css';

const target = document.getElementById('root');

render(<Root>
  <App />
</Root>, target);
registerServiceWorker();