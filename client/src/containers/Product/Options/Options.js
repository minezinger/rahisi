import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import { Formik } from 'formik';
import { isEmpty } from 'lodash';
import moment from 'moment';
import * as Yup from 'yup';

import { getProductDeparturesMulti, getProduct, handleOptions } from 'store/actions/actions';

import Button from 'components/UI/Button/Button';
import CodeBlock from 'components/UI/CodeBlock/CodeBlock';
import Hero from 'components/UI/Hero/Hero';
import Notification from 'components/UI/Notification/Notification';

import Option from 'components/Product/Option/Option';

const validationSchema = Yup.object().shape({})

class ProductOptions extends Component {
  componentDidMount() {
    const { getProductDeparturesMulti, getProduct, match: { params: { passengers, parentId, productId, startDate } }, product: { details }, search: { criteria } } = this.props;

    /** If we don't already have the product details loaded, we're making sure
     *  to get them. We're setting includePickups to false because we do not need
     *  to load them yet.
     */
    if (isEmpty(details) || (parseInt(details.id, 10) !== parseInt(productId, 10) && parseInt(details.id, 10) !== parseInt(parentId, 10))) {
      getProduct(parentId || productId, { includePickups: false });
    }

    /** Here we're doing a departures-multi check. This can be used on a top-level
     *  Tour and will get availability for that Tour (if it's bookable) or its
     *  Flavours.
     */
    getProductDeparturesMulti(parentId || productId, criteria ? criteria : { endDate: moment().add(15, 'days').format('YYYY-MM-DD'), passengers, startDate })
  }

  render() {
    const { handleOptions, history, match: { params: { parentId, passengers, productId, productType, startDate } }, product: { departures, details } } = this.props;

    if (isEmpty(departures) || isEmpty(details)) {
      return <Notification contained={true} spinner={true}>Loading options...</Notification>
    }

    const options = [];
    departures.map(product => {
      if (product.productType === 'OPTION') {
        const foundOptions = product.departures.find(d => d.date === startDate && d.availableUnits > parseInt(passengers, 10) && d.status === 'OPEN');
        if (foundOptions) {
          options.push({
            ...product,
            departures: undefined,
            departure: foundOptions
          })
        }
      }
    });

    const passengerCount = parseInt(passengers, 10);
    const cartItems = [];
    for (let i = 0; i < passengerCount; i++) {
      /**
       * cartItems will be the array of products that we POST with the cart. These
       * represent each passenger's product selections.
       */
      cartItems.push({
        paxIndex: i,
        options: options.map((option, index) => {
          return {
            departure: option.departure,
            optionIndex: index,
            productId: option.productId,
            productName: option.productName,
            productStartTime: option.productStartTime,
            retailCurrency: option.retailCurrency,
            selected: false
          }
        })
      })
    }

    return <div>
      <Hero containerClass="has-text-centered" type="primary">
        <h1 className="is-size-2">{details.name}</h1>
        <h2 className="subtitle is-size-4">{details.operatorName}</h2>
      </Hero>

      <Hero type="light">
        <h3 className="is-size-3">Options</h3>
        <div className="content">
          <p>Here we can add some optional extras to our select tour or flavour. Just as a recap, the definition of an option is as follows:</p>
          <p><strong>Options</strong> are optional extras that can be booked in addition to any bookable product which provides them. They cannot be booked on their own and must accompany a bookable product. For example, a DVD or photos of a skydive, or a digital tour guide booked together with a walking tour. It is not possible to only purchase the DVD without going on the skydive. Options always include a list of ids of the products they can be booked with called <var>masterProductIds</var>.</p>
        </div>
      </Hero>

      <div className="section">
        <div className="container">
          <div className="columns is-variable is-8">
            <div className="column is-three-fifths">

              <Formik
                initialValues={{ cartItems }}
                validationSchema={validationSchema}
                onSubmit={({ cartItems }) => {
                  let link = '/be/product/pickups/tour/';
                  if (productType === 'flavour' || parentId) {
                    link += parentId + '/flavour/' + productId;
                  } else {
                    link += productId;
                  }
                  link += '/' + startDate + '/' + passengers;
                  handleOptions(passengers, { productId, startDate }, cartItems, history, link);
                }}
                render={({
                  values,
                  errors,
                  touched,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                }) => <form onSubmit={handleSubmit}>
                    {isEmpty(options) ? <div className="content">
                      <p>There are no options for this tour. However, this would be where we display them if there were any.</p>
                      <p>To see an example with options, please go back to either the <Link to="/be/destination">Destinations</Link> or <Link to="/be/operator">Operators</Link> search and select a different product. Otherwise, click the button below to continue.</p>
                    </div> : options.map((option, index) => <Option
                      currency={option.retailCurrency}
                      details={option}
                      errors={errors}
                      handleBlur={handleBlur}
                      handleChange={handleChange}
                      key={option.productId}
                      optionIndex={index}
                      startDate={startDate}
                      touched={touched}
                      values={values}
                    />)}
                    <div className="field is-grouped is-grouped-right margin-top">
                      <div className="control">
                        <Link className="button is-link" to={`/be/product/details/tour/${productId}/${startDate}/${passengers}`}>Back to product details</Link>
                      </div>
                      <div className="control">
                        <Button className="is-primary" type="submit">Continue</Button>
                      </div>
                    </div>
                  </form>
                }
              />

            </div>
            <div className="column is-two-fifths">
              <div className="explanation-block content">
                <h3>Checking for availability</h3>
                <p>Depending on how you want to order the booking process, you will then need to check availability by using either <code>GET /products/&#123;productId&#125;/departures</code> or <code>GET /products/&#123;productId&#125;/departures-multi</code>. Both of these return <strong>cached availability</strong>.</p>
                <code>GET /products/&#123;productId&#125;/departures</code>
                <p>This can be used on bookable products, either a Tour with no Flavours or a Flavour. If no <var>startDate</var> and <var>endDate</var> are specified, the system returns the first (soonest) open departure with any availability. You can use the parameters <var>minUnits</var> and <var>limit</var> to further influence this behaviour.</p>
                <code>GET /products/&#123;productId&#125;/departures-multi</code>
                <p>This can be used on a top-level Tour and will get availability for that Tour (if it's bookable) or its Flavours.</p>
                <p>In this case, the behaviour of <var>limit</var> needs to be considered carefully. If the Tour has Flavours, the <var>limit</var> will be applied on a Flavour by Flavour basis. This means that the results could possibly contain availability for "Flavour A" across the next 14 days and results for "Flavour B" across the next 7 days. However, this does not mean that "Flavour B" is not available in the following 7 days, just that it hit its <var>limit</var> value before "Flavour A".</p>
              </div>
              <div className="explanation-block content">
                <h3>Displaying availability</h3>
                <p>The departures on the left have been returned as the following.</p>
                <CodeBlock code={JSON.stringify(departures, null, 2)} height="500" language="json" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  }
}

const mapStateToProps = state => ({
  apiUser: state.apiUser,
  message: state.message,
  product: state.product,
  search: state.search
})

export default connect(mapStateToProps, { getProductDeparturesMulti, getProduct, handleOptions })(withRouter(ProductOptions));