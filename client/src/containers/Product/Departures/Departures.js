import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { find, isEmpty } from 'lodash';
import moment from 'moment';

import { getProductDeparturesMulti, getProduct } from 'store/actions/actions';

import CodeBlock from 'components/UI/CodeBlock/CodeBlock';
import Hero from 'components/UI/Hero/Hero';
import Notification from 'components/UI/Notification/Notification';

import Flavour from 'components/Product/Flavour/Flavour';

class ProductDepartures extends Component {
  componentDidMount() {
    const { getProductDeparturesMulti, getProduct, match: { params: { passengers, productId, startDate } }, product: { details }, search: { criteria } } = this.props;

    /** If we don't already have the product details loaded, we're making sure
     *  to get them. We're setting includePickups to false because we do not need
     *  to load them yet.
     */
    if (isEmpty(details) || parseInt(details.id, 10) !== parseInt(productId, 10)) {
      getProduct(productId, { includePickups: false });
    }

    /** Here we're doing a departures-multi check. This can be used on a top-level
     *  Tour and will get availability for that Tour (if it's bookable) or its
     *  Flavours.
     */
    getProductDeparturesMulti(productId, criteria ? criteria : { endDate: moment().add(15, 'days').format('YYYY-MM-DD'), passengers, startDate })
  }

  render() {
    const { apiUser: { outletGroup }, match: { params: { passengers, parentId, productId, startDate } }, product: { departures, details }, search: { criteria } } = this.props;

    if (isEmpty(departures) || isEmpty(details)) {
      return <Notification contained={true} spinner={true}>Loading departures...</Notification>
    }

    return <div>
      <Hero containerClass="has-text-centered" type="primary">
        <h1 className="is-size-2">{details.name}</h1>
        <h2 className="subtitle is-size-4">{details.operatorName}</h2>
      </Hero>

      <Hero type="light">
        <h3 className="is-size-3">Departure Dates and Flavours</h3>
        <div className="content">
          <p>Please select a bookable product - a tour or flavour - and a date to continue. The available units, prices - including that of adults and children separately where applicable - and levies, if any, are displayed with each date. Just as a recap, the difference between a tour and a flavour are as follows:</p>
          <p><strong>Tours</strong> are the main, top-level products. They contain the different variations of a Tour (Flavours) and any optional add-ons (Options) available. These will contain the information that is consistent across all variations of the Tour, such as the operator name, operator currency, images, cancellation policies, etc.</p>
          <p><strong>Flavours</strong> are the different variations of a Tour and constitute the bulk of our bookable products. They could represent different start times, languages for the Tour, etc. For example, the Flavours on a reef cruise might include "Snorkeling", "1 Introductory Dive", "2 Introductory Dives", and so on.</p>
          <Link className="button is-link" to={`/be/product/details/tour/${productId}/${startDate}/${passengers}`}>Back to product details</Link>
        </div>
      </Hero>

      <div className="section">
        <div className="container">
          <div className="columns is-variable is-8">
            <div className="column is-three-fifths">
              {departures.map((product) => {
                if (product.productType === 'TOUR' || product.productType === 'FLAVOUR') {
                  const flavour = find(details.flavours, { 'id': product.productId });
                  return <Flavour
                    currency={outletGroup.currency}
                    details={{
                      ...flavour,
                      ...product
                    }}
                    key={product.productId}
                    parentId={parentId || details.id}
                    productId={productId}
                    passengers={!isEmpty(criteria) ? criteria.passengers : passengers}
                  />
                }
              })}
            </div>
            <div className="column is-two-fifths">
              <div className="explanation-block content">
                <h3>Checking for availability</h3>
                <p>Depending on how you want to order the booking process, you will then need to check availability by using either <code>GET /products/&#123;productId&#125;/departures</code> or <code>GET /products/&#123;productId&#125;/departures-multi</code>. Both of these return <strong>cached availability</strong>.</p>
                <code>GET /products/&#123;productId&#125;/departures</code>
                <p>This can be used on bookable products, either a Tour with no Flavours or a Flavour. If no <var>startDate</var> and <var>endDate</var> are specified, the system returns the first (soonest) open departure with any availability. You can use the parameters <var>minUnits</var> and <var>limit</var> to further influence this behaviour.</p>
                <code>GET /products/&#123;productId&#125;/departures-multi</code>
                <p>This can be used on a top-level Tour and will get availability for that Tour (if it's bookable) or its Flavours.</p>
                <p>In this case, the behaviour of <var>limit</var> needs to be considered carefully. If the Tour has Flavours, the <var>limit</var> will be applied on a Flavour by Flavour basis. This means that the results could possibly contain availability for "Flavour A" across the next 14 days and results for "Flavour B" across the next 7 days. However, this does not mean that "Flavour B" is not available in the following 7 days, just that it hit its <var>limit</var> value before "Flavour A".</p>
              </div>
              <div className="explanation-block content">
                <h3>Displaying availability</h3>
                <p>The departures on the left have been returned as the following.</p>
                <CodeBlock code={JSON.stringify(departures, null, 2)} height="500" language="json" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  }
}

const mapStateToProps = state => ({
  apiUser: state.apiUser,
  message: state.message,
  product: state.product,
  search: state.search
})

export default connect(mapStateToProps, { getProductDeparturesMulti, getProduct })(ProductDepartures);