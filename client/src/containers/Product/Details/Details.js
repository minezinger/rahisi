import React, { Component } from 'react';
import { connect } from 'react-redux';
import { isEmpty } from 'lodash';

import { getProduct } from 'store/actions/actions';

import Notification from 'components/UI/Notification/Notification';

import Detailed from 'components/Product/Detailed/Detailed';

class ProductDetails extends Component {
  componentDidMount() {
    const { getProduct, match: { params: { productId } }, product: { details } } = this.props;

    /** If we don't already have the product details loaded, we're making sure
     *  to get them. We're setting includePickups to false because we do not need
     *  to load them yet.
     */
    if (isEmpty(details) || parseInt(details.id, 10) !== parseInt(productId, 10)) {
      getProduct(productId, { includePickups: false });
    }
  }

  render() {
    const { match: { params: { passengers, productId, startDate } }, product: { details, response } } = this.props;

    if (isEmpty(details) || parseInt(details.id, 10) !== parseInt(productId, 10)) {
      return <Notification contained={true} spinner={true}>Loading product...</Notification>
    }

    return <Detailed criteria={{ passengers, startDate }} product={details} response={response} />
  }
}

const mapStateToProps = state => ({
  message: state.message,
  product: state.product
})

export default connect(mapStateToProps, { getProduct })(ProductDetails);