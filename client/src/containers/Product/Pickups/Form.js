import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import { Formik } from 'formik';
import { isEmpty } from 'lodash';
import * as Yup from 'yup';

import { handlePickups } from 'store/actions/actions';

import Button from 'components/UI/Button/Button';
import Notification from 'components/UI/Notification/Notification';

import Pickup from 'components/Product/Pickup/Pickup';

const validationSchema = Yup.object().shape({})

class PickupForm extends Component {
  render() {
    const { link, handlePickups, history, parentId, passengers, paxSelections, pickups, productId, startDate } = this.props;

    if (isEmpty(pickups)) {
      return <Notification contained={true} spinner={true}>Loading pickups...</Notification>
    }

    const passengerCount = parseInt(passengers, 10);
    const cartItems = [];
    for (let i = 0; i < passengerCount; i++) {
      /**
       * cartItems will be the array of products that we POST with the cart. These
       * represent each passenger's product selections.
       */
      cartItems.push({
        paxIndex: i,
        pickupId: pickups[0].id
      })
    }

    return <Formik
      initialValues={{ cartItems }}
      validationSchema={validationSchema}
      onSubmit={({ cartItems }) => {
        handlePickups(cartItems, paxSelections, history, link);
      }}
      render={({
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
      }) => <form onSubmit={handleSubmit}>
          {cartItems.map(cartItem => {
            const index = cartItem.paxIndex;
            const thisCartItemError = !isEmpty(errors.cartItems) ? errors.cartItems[index] : {};
            const thisCartItemTouched = !isEmpty(touched.cartItems) ? touched.cartItems[index] : {};
            const thisCartItemValues = !isEmpty(values.cartItems) ? values.cartItems[index] : {};

            return <Pickup
              errors={!isEmpty(thisCartItemError) ? thisCartItemError.pickupId : undefined}
              key={cartItem.paxIndex}
              label={`Pickup for passenger ${index + 1}`}
              name={`cartItems[${index}].pickupId`}
              onChange={handleChange}
              onBlur={handleBlur}
              options={pickups}
              touched={!isEmpty(thisCartItemTouched) ? thisCartItemTouched.pickupId : undefined}
              value={!isEmpty(thisCartItemValues) ? thisCartItemValues.pickupId : undefined}
            />
          })}
          <div className="field is-grouped is-grouped-right">
            <div className="control">
              <Link className="button is-link" to={`/be/product/details/tour/${parentId || productId}/${startDate}/${passengers}`}>Back to product details</Link>
            </div>
            <div className="control">
              <Button className="is-primary" type="submit">Continue</Button>
            </div>
          </div>
        </form>
      }
    />
  }
}

const mapStateToProps = state => ({
  search: state.search
})

export default connect(mapStateToProps, { handlePickups })(withRouter(PickupForm));