import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { isEmpty } from 'lodash';
import moment from 'moment';

import { getProduct } from 'store/actions/actions';

import CodeBlock from 'components/UI/CodeBlock/CodeBlock';
import Hero from 'components/UI/Hero/Hero';
import Notification from 'components/UI/Notification/Notification';

import PickupForm from 'containers/Product/Pickups/Form';

class ProductPickups extends Component {
  componentDidMount() {
    const { getProduct, match: { params: { productId } } } = this.props;
    /** We're getting the product details again even if we already have them because
     *  we haven't used includePickups in any of the previous GETs.
     */
    getProduct(productId, { includePickups: true });
  }

  render() {
    const { match: { params: { parentId, passengers, productId, startDate } }, product: { details, loading, paxSelections } } = this.props;

    if (isEmpty(details) || loading) {
      return <Notification contained={true} spinner={true}>Loading pickups...</Notification>
    }

    const productType = details.type.toLowerCase();
    let link = '/be/cart/';
    if (productType === 'flavour') {
      link += 'tour/' + parentId + '/' + productType + '/' + productId;
    } else {
      link += productType + '/' + productId;
    }
    link += '/' + startDate + '/' + passengers;

    return <div>
      <Hero containerClass="has-text-centered" type="primary">
        <h1 className="is-size-2">{details.name}</h1>
        <h2 className="subtitle is-size-4">{moment(startDate).format('dddd, DD MMMM YYYY')}</h2>
      </Hero>

      <div className="section">
        <div className="container">
          <div className="columns is-variable is-8">
            <div className="column is-half">
              <h3 className="is-size-3">Pickups</h3>
              {!isEmpty(details.pickups) ? <div>
                <div className="content">
                  <p>Please select a pickup for each passenger to continue.</p>
                </div>
                <PickupForm
                  link={link}
                  parentId={parentId}
                  passengers={passengers}
                  paxSelections={paxSelections}
                  pickups={details.pickups}
                  productId={productId}
                  startDate={startDate}
                />
              </div> : <div className="content">
                  <p>There are no pickups for this tour. However, this would be where we display them if there were any.</p>
                  <p>To see an example with pickups, please go back to either the <Link to="/be/destination">Destinations</Link> or <Link to="/be/operator">Operators</Link> search and select a different product. Otherwise, click the button below to continue.</p>
                  <div className="field is-grouped is-grouped-right">
                    <div className="control">
                      <Link className="button is-link" to={`/be/product/details/tour/${parentId || productId}/${startDate}/${passengers}`}>Back to product details</Link>
                    </div>
                    <div className="control">
                      <Link className="button is-primary" to={link}>Continue</Link>
                    </div>
                  </div>
                </div>}
            </div>
            <div className="column is-half">
              <div className="explanation-block content">
                <h3>Getting and using pickups</h3>
                <p>Pickups come with the product details via <code>GET /products/&#123;productId&#125;</code> if <var>includePickups</var> is set to <var>true</var> (the default).</p>
                {!isEmpty(details.pickups) ? <div>
                  <p>The pickups we are displaying to the left come as shown below.</p>
                  <CodeBlock code={JSON.stringify(details.pickups, null, 2)} height="300" language="json" />
                </div> : <p>However, since there are no pickups for this tour, we cannot display any.</p>}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  }
}

const mapStateToProps = state => ({
  apiUser: state.apiUser,
  message: state.message,
  product: state.product
})

export default connect(mapStateToProps, { getProduct })(ProductPickups);