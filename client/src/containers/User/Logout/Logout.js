import React, { Component } from 'react';
import { connect } from 'react-redux';

import { logoutUser } from 'store/actions/actions';

class Logout extends Component {
  componentDidMount() {
    const { logoutUser } = this.props;
    logoutUser();
  }

  render() {
    return <div className="section">
      <div className="container">
        <p>You have been logged out.</p>
      </div>
    </div>
  }
}

export default connect(null, { logoutUser })(Logout);