import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Formik } from 'formik';
import { isEmpty } from 'lodash';
import * as Yup from 'yup';

import { postUserLogin } from 'store/actions/actions';

import Button from 'components/UI/Button/Button';
import Input from 'components/UI/Input/Input';
import Notification from 'components/UI/Notification/Notification';

const validationSchema = Yup.object().shape({
  email: Yup.string()
    .email('E-mail is not valid')
    .required('E-mail is required'),
  password: Yup.string()
    .min(6, 'Password has to be longer than 6 characters')
    .required('Password is required')
})

class Login extends Component {
  render() {
    const { message, history, postUserLogin } = this.props;
    const { from: { pathname } } = this.props.location.state || { from: { pathname: '/' } };

    const authWarning = pathname.includes('/be/') ? <Notification type="warning">You must be logged in to view that!</Notification> : null;

    return <div>
      {authWarning}
      <h4 className="is-size-4">Login</h4>
      {/*
          The benefit of the render prop approach is that you have full access to
          React's state, props, and composition model. Thus there is no need to map
          outer props to values... you can just set the initial values, and if they
          depend on props/state then --boom-- you can directly access props/state.
          The render prop accepts your inner form component, which you can define
          separately or inline totally up to you:
          - `<Formik render={props => <form>...</form>}>`
          - `<Formik component={InnerForm}>`
          - `<Formik>{props => <form>...</form>}</Formik>` (identical to as render, just written differently)
        */}
      <Formik
        initialValues={{
          email: 'test@livn.world',
          password: 'password'
        }}
        validationSchema={validationSchema}
        onSubmit={(values) => {
          postUserLogin(values, history)
        }}
        render={({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
        }) => <form onSubmit={handleSubmit}>
            <Input
              errors={errors.email}
              horizontal={true}
              label="Email"
              name="email"
              onChange={handleChange}
              onBlur={handleBlur}
              touched={touched.email}
              type="email"
              value={values.email}
            />
            <Input
              errors={errors.password}
              horizontal={true}
              label="Password"
              name="password"
              onChange={handleChange}
              onBlur={handleBlur}
              touched={touched.password}
              type="password"
              value={values.password}
            />

            {!isEmpty(message.text) ? <Notification type={message.type}>{message.text}</Notification> : null}

            <div className="field is-grouped is-grouped-right">
              <div className="control">
                <Button className="is-primary" type="submit">
                  Submit
                </Button>
              </div>
            </div>
          </form>
        }
      />
    </div>
  }
}

const mapStateToProps = state => ({
  message: state.message
})

export default connect(mapStateToProps, { postUserLogin })(withRouter(Login));