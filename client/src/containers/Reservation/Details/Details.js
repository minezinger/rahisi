import React, { Component } from 'react';
import { connect } from 'react-redux';
import { isEmpty } from 'lodash';

import { getReservation, getReservationTickets } from 'store/actions/actions';

import Notification from 'components/UI/Notification/Notification';

import Detailed from 'components/Reservation/Detailed/Detailed';

class ReservationDetails extends Component {
  componentDidMount() {
    const { getReservation, match: { params: { reservationId } }, reservation: { details } } = this.props;

    /** If we don't already have the reservation details loaded, we're making
     *  sure to get them.
     */
    if (isEmpty(details) || parseInt(details.id, 10) !== parseInt(reservationId, 10)) {
      getReservation(reservationId);
    }
  }
  render() {
    const { getReservationTickets, message, reservation: { details, loading } } = this.props;

    if (!isEmpty(message.text)) {
      return <Notification contained={true} type={message.type}>
        {message.text}
      </Notification>
    }

    if (isEmpty(details) || loading) {
      return <Notification contained={true} spinner={true}>Loading reservation...</Notification>
    }

    return <Detailed details={details} getTickets={getReservationTickets} />
  }
}

const mapStateToProps = state => ({
  message: state.message,
  reservation: state.reservation
})

export default connect(mapStateToProps, { getReservation, getReservationTickets })(ReservationDetails);