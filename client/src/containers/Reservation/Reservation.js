import React, { Component } from 'react';
import { connect } from 'react-redux';

import Hero from 'components/UI/Hero/Hero';
import Results from 'containers/Reservation/Results/Results';
import Search from 'containers/Reservation/Search/Search';

class Reservation extends Component {
  render() {
    const { reservation: { list } } = this.props;

    return <div>
      <Hero type="primary">
        <h1 className="is-size-2">Reservations</h1>
        <h2 className="subtitle is-size-4">Find reservations by passenger last name or email</h2>
      </Hero>

      <Hero type="light">
        <Search />
      </Hero>

      <div className="section">
        <div className="container">
          <Results list={list} />
        </div>
      </div>
    </div>
  }
}

const mapStateToProps = state => ({
  reservation: state.reservation
})

export default connect(mapStateToProps)(Reservation);