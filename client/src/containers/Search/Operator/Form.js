import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Formik } from 'formik';
import * as Yup from 'yup';

import { getOperatorProducts } from 'store/actions/actions';

import Button from 'components/UI/Button/Button';
import Select from 'components/UI/Select/Select';

const validationSchema = Yup.object().shape({
  operator: Yup.string().required()
})

class OperatorForm extends Component {
  render() {
    const { getOperatorProducts, search: { operators } } = this.props;

    return <Formik
      initialValues={{
        operator: operators[0].id
      }}
      validationSchema={validationSchema}
      onSubmit={(values) => {
        getOperatorProducts(values)
      }}
      render={({
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
      }) => <form onSubmit={handleSubmit}>
          <Select
            errors={errors.operator}
            label="Operator"
            name="operator"
            onChange={handleChange}
            onBlur={handleBlur}
            options={operators}
            optionText="name"
            optionValue="id"
            touched={touched.operator}
            value={values.operator}
          />
          <div className="field is-grouped is-grouped-right">
            <div className="control">
              <Button className="is-primary" type="submit">Search</Button>
            </div>
          </div>
        </form>
      }
    />
  }
}

const mapStateToProps = state => ({
  search: state.search
})

export default connect(mapStateToProps, { getOperatorProducts })(OperatorForm);