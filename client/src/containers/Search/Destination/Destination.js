import React, { Component } from 'react';
import { connect } from 'react-redux';
import { isEmpty } from 'lodash';

import { getDestinations } from 'store/actions/actions';

import CodeBlock from 'components/UI/CodeBlock/CodeBlock';
import Hero from 'components/UI/Hero/Hero';
import Notification from 'components/UI/Notification/Notification';

import DestinationForm from 'containers/Search/Destination/Form';
import Results from 'containers/Search/Results/Results';

class SearchDestination extends Component {
  componentDidMount() {
    const { getDestinations } = this.props;
    getDestinations();
  }

  render() {
    const { search: { destinations, responseDestination, resultsDestination } } = this.props;

    if (isEmpty(destinations)) {
      return <Notification contained={true} spinner={true}>Loading destinations...</Notification>
    }

    return <div>
      <Hero type="primary">
        <h1 className="is-size-2">Search by destination</h1>
        <h2 className="subtitle is-size-4">Using airports and city coordinates</h2>
      </Hero>

      <div className="section">
        <div className="container">
          <div className="explanation-block">
            <div className="columns is-variable is-8">
              <div className="column is-half content">
                <h3>Getting lists of cities and airports</h3>
                <p>Searching for products by location is made easier using the lists of provided cities and airports, using <code>GET /products/cities</code> and <code>GET /products/airports</code> respectively, which have all the details you need to display these in the UI and then use them to conduct a search.</p>
                <p>In this sample application, we have combined those two lists to create one, shown below.</p>
                <CodeBlock code={responseDestination} height="365" language="json" />
              </div>
              <div className="column is-half content">
                <h3>Searching by location</h3>
                <p>You can then search for products by location using <code>GET /products/search</code>. Whilst the minimum required parameters are the fts, location, or airport, keep in mind that using fts on its own will search for keywords in every location in the world. Because of this, pairing it with either a location or airport value will result in a response that is much easier for the user to handle.</p>
                <p>Doing a search without a filter - such as <var>startDate</var>, <var>endDate</var>, <var>requiredUnits</var> - will return a list of all products that meet the criteria. So there are many ways in which this call can be used.</p>
                <p>Some other useful things to note are:</p>
                <ul>
                  <li>The <var>includeFullDetails</var> option needs to be set to true in order to get min/max pricing for the returned products.</li>
                  <li>The <var>treeView</var> option is quite useful as flavours end up nested under the parent product. Otherwise, only directly bookable products are returned.</li>
                  <li>The <var>includePickups</var> option slows things down a lot if set to true. A better method to approach this would be to have it set to false, then individually get the pickups for a product that the user has already shown interest in (by viewing it).</li>
                  <li>The maximum for <var>requiredUnits</var> is 10.</li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>

      <Hero type="light">
        <DestinationForm />
      </Hero>

      <Results list={resultsDestination} />
    </div>
  }
}

const mapStateToProps = state => ({
  search: state.search
})

export default connect(mapStateToProps, { getDestinations })(SearchDestination);