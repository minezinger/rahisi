import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Formik } from 'formik';
import { isEmpty } from 'lodash';
import * as Yup from 'yup';
import moment from 'moment';

import { getSearch } from 'store/actions/actions';

import Button from 'components/UI/Button/Button';
import Input from 'components/UI/Input/Input';
import Select from 'components/UI/Select/Select';

const validationSchema = Yup.object().shape({
  destination: Yup.string().required(),
  endDate: Yup.date(),
  keywords: Yup.string().trim(),
  passengers: Yup.number().min(1).max(10),
  startDate: Yup.date()
})

class DestinationForm extends Component {
  render() {
    const { getSearch, search: { destinations } } = this.props;

    return <div>
      <Formik
        initialValues={{
          country: destinations[0].code,
          destination: destinations[0].destinations[0].coordinates,
          endDate: moment().add(15, 'days').format('YYYY-MM-DD'),
          keywords: '',
          passengers: 2,
          startDate: moment().add(1, 'days').format('YYYY-MM-DD')
        }}
        validationSchema={validationSchema}
        onSubmit={(values, { setSubmitting }) => {
          getSearch(values)
          setSubmitting(false)
        }}
        render={({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
        }) => {
          let selectedDestinations = [{ destinations: { coordinates: 0, name: 'Please select a country' } }];
          if (values.country) {
            const foundDestinations = destinations.find(d => d.code === values.country);
            if (!isEmpty(foundDestinations)) {
              selectedDestinations = foundDestinations.destinations;
              const check = foundDestinations.destinations.find(d => d.coordinates === values.destination);
              if (isEmpty(check)) {
                values.destination = foundDestinations.destinations[0].coordinates;
              }
            }
          }

          return <form onSubmit={handleSubmit}>
            <div className="columns">
              <div className="column is-half">
                <Input
                  errors={errors.startDate}
                  label="Start Date"
                  name="startDate"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  touched={touched.startDate}
                  type="text"
                  value={values.startDate}
                />
              </div>
              <div className="column is-half">
                <Input
                  errors={errors.endDate}
                  label="End Date"
                  name="endDate"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  touched={touched.endDate}
                  type="text"
                  value={values.endDate}
                />
              </div>
            </div>
            <div className="columns">
              <div className="column is-half">
                <Select
                  errors={errors.country}
                  label="Country"
                  name="country"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  options={destinations}
                  optionText="country"
                  optionValue="code"
                  touched={touched.country}
                  value={values.country}
                />
              </div>
              <div className="column is-half">
                <Select
                  errors={errors.destination}
                  label="Destination"
                  name="destination"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  options={selectedDestinations}
                  optionText="name"
                  optionValue="coordinates"
                  touched={touched.destination}
                  value={values.destination}
                />
              </div>
            </div>
            <div className="columns">
              <div className="column is-one-third">
                <Input
                  errors={errors.passengers}
                  label="Passengers"
                  name="passengers"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  touched={touched.passengers}
                  type="number"
                  value={values.passengers}
                />
              </div>
              <div className="column is-two-thirds">
                <Input
                  errors={errors.keywords}
                  label="Keywords"
                  name="keywords"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  touched={touched.keywords}
                  type="text"
                  value={values.keywords}
                />
              </div>
            </div>

            <div className="field is-grouped is-grouped-right">
              <div className="control">
                <Button className="is-primary" disabled={isSubmitting} type="submit">
                  Search
                </Button>
              </div>
            </div>
          </form>
        }
        }
      />
    </div>
  }
}

const mapStateToProps = state => ({
  search: state.search
})

export default connect(mapStateToProps, { getSearch })(DestinationForm);