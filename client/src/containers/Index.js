import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Route } from 'react-router-dom';

import { routes } from 'routes/routes';
import { getAPIUser } from 'store/actions/actions';

import Footer from 'hoc/Layout/Footer/Footer';
import Header from 'hoc/Layout/Header/Header';

class Index extends Component {
  componentDidMount() {
    const { getAPIUser, user: { isAuthenticated } } = this.props;
    if (isAuthenticated) {
      getAPIUser();
    }
  }

  render() {
    const { location: { pathname } } = this.props;
    
    return <div>
      <div className="main-content">
        <Header currentLocation={pathname} />
        {routes.map(route => {
          return <Route
            key={route.id}
            path={route.path}
            exact={route.exact}
            component={route.component}
          />
        })}
      </div>
      <Footer />
    </div>
  }
}

const mapStateToProps = state => ({
  apiUser: state.apiUser,
  user: state.user
})

export default connect(mapStateToProps, { getAPIUser })(Index);