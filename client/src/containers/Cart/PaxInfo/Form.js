import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import { Formik } from 'formik';
import { isEmpty } from 'lodash';
import * as Yup from 'yup';

import { postCart } from 'store/actions/actions';

import Button from 'components/UI/Button/Button';
import Notification from 'components/UI/Notification/Notification';

import PaxDetail from 'components/Cart/PaxDetail/PaxDetail';

const validationSchema = Yup.object().shape({
  paxes: Yup.array().of(Yup.object().shape({
    address: Yup.string(),
    age: Yup.number().min(0, 'Age must be a number between 0 and 120').max(120, 'Age must be a number between 0 and 120'),
    country: Yup.string(),
    dob: Yup.date(),
    email: Yup.string().email(),
    firstName: Yup.string().trim(),
    language: Yup.string(),
    lastName: Yup.string().trim(),
    mobile: Yup.string(),
    nationality: Yup.string(),
    operatorNote: Yup.string().max(250),
    paxNote: Yup.string().max(250),
    passportExpiry: Yup.date(),
    passportIssued: Yup.date(),
    passportNumber: Yup.string(),
    phone: Yup.string(),
    salutation: Yup.string()
  }))
})

class PaxInfoForm extends Component {
  render() {
    const { data: { countries, languages }, history, match: { params: { parentId, pickupList, productDate, productId } }, message, passengers, postCart, product: { paxDetails, paxSelections } } = this.props;

    const passengerCount = parseInt(passengers, 10);
    const paxes = [];
    const cartItems = !isEmpty(paxSelections) ? paxSelections : [];
    const populateCartItems = isEmpty(cartItems);
    const initialValues = [];
    for (let i = 0; i < passengerCount; i++) {
      /**
       * There are three possible values for each pax detail: 'NOT_REQ', 'ONCE', 'ALL'.
       * We don't want the user to have to fill out unnecessary details, so we're only
       * interested in two cases:
       * - where the value is 'ALL', or
       * - where the value is 'ONCE' and it's the first (a.k.a. "Lead") passenger
       * See https://dev1.livngds.com/livngds/api-docs/#!/products/getProductPaxDetails for more
       * information.
       */
      let currentPaxDetails = {};
      let currentInitialValues = {};
      for (const key in paxDetails) {
        const value = paxDetails[key];
        if (value === 'ALL' || (value === 'ONCE' && i === 0)) {
          currentPaxDetails[key] = value;
          /**
           * Setting some basic initial values for the select elements.
           */
          if (key === 'country' || key === 'nationality') {
            currentInitialValues[key] = 'AU';
          } else if (key === 'language') {
            currentInitialValues[key] = 'en';
          }
        }
      }
      /**
       * If the page was refreshed and the selected product information was lost, we are getting
       * that information from the route. However, at the moment, this means that any selected
       * options are lost.
       */
      if (populateCartItems) {
        cartItems.push({
          paxIndex: i,
          productId,
          productDate,
          pickupId: pickupList ? pickupList.split(',')[i] : undefined
        })
      }
      /**
       * paxes will be the array of products that we POST with the cart. These hold
       * the completed passenger details.
       */
      paxes.push({
        paxIndex: i,
        paxDetails: currentPaxDetails
      })
      /**
       * Setting some basic initial values for the select elements.
       */
      initialValues.push({
        ...currentInitialValues,
        salutation: 'MISS',
        firstName: 'TestFN',
        lastName: 'TestLN',
        email: 'support@livnholidays.com',
        mobile: '92644411',
        dob: '1989-05-20',
        age: 29
      })
    }

    return <Formik
      initialValues={{ cartItems, paxes: initialValues }}
      validationSchema={validationSchema}
      onSubmit={(values, { setSubmitting }) => {
        postCart({ cartItems: values.cartItems, parentId, paxes: values.paxes, productId }, history)
        setSubmitting(false)
      }}
      render={({
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
      }) => <form onSubmit={handleSubmit}>
          {paxes.map(pax => {
            return <PaxDetail
              countries={countries}
              errors={errors}
              handleBlur={handleBlur}
              handleChange={handleChange}
              key={pax.paxIndex}
              languages={languages}
              passenger={pax}
              paxDetails={pax.paxDetails}
              touched={touched}
              values={values}
            />
          })}

          {!isEmpty(message.text) ? <Notification type={message.type}>{message.text}</Notification> : null}

          <div className="field is-grouped is-grouped-right margin-top">
            <div className="control">
              <Link className="button is-link" to={`/be/product/details/tour/${parentId || productId}/${productDate}/${passengers}`}>Back to product details</Link>
            </div>
            <div className="control">
              <Button className="is-primary" disabled={isSubmitting} type="submit">Continue</Button>
            </div>
          </div>
        </form>
      }
    />
  }
}

const mapStateToProps = state => ({
  data: state.data,
  message: state.message,
  product: state.product
})

export default connect(mapStateToProps, { postCart })(withRouter(PaxInfoForm));