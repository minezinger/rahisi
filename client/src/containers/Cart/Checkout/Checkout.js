import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { isEmpty } from 'lodash';
import moment from 'moment';

import { getCart, getCartCheckout } from 'store/actions/actions';

import Button from 'components/UI/Button/Button';
import CodeBlock from 'components/UI/CodeBlock/CodeBlock';
import Hero from 'components/UI/Hero/Hero';
import Notification from 'components/UI/Notification/Notification';

import CartError from 'components/Cart/Error/Error';

class CartCheckout extends Component {
  componentDidMount() {
    const { cart: { details, loading }, getCart, match: { params: { cartId } } } = this.props;

    /** If we don't already have the cart loaded, we're making sure to get it. */
    if (!loading && (isEmpty(details) || parseInt(details.id, 10) !== parseInt(cartId, 10))) {
      getCart(cartId);
    }
  }

  render() {
    const { cart: { details, loading, postCart, responseCart, responseError }, getCartCheckout, history, match: { params: { cartId, parentId } }, message } = this.props;

    if (!isEmpty(message.text)) {
      return <CartError message={message} parentId={parentId} responseError={responseError} />
    }

    if (isEmpty(details)) {
      return <Notification contained={true} spinner={true}>Loading cart...</Notification>
    }

    if (loading) {
      return <Notification contained={true} spinner={true}>Checking out the cart...</Notification>
    }

    let link = '/be/cart/tour/';
    if (parentId) {
      link += parentId + '/flavour/' + details.cartItems[0].productId;
    } else {
      link += details.cartItems[0].productId;
    }
    link += '/' + details.cartItems[0].productDate + '/' + details.paxes.length;

    const postCartJSON = postCart ? <div className="column is-half">
      <div className="explanation-block content">
        <p>In order to post the cart, what we sent to the Livn API is shown below.</p>
        <CodeBlock code={postCart} height="400" language="json" />
      </div>
    </div> : null;
    const responseCartJSON = responseCart ? <div className="column is-half">
      <div className="explanation-block content">
        <p>The response from us successfully posted the cart is shown below.</p>
        <CodeBlock code={responseCart} height="400" language="json" />
      </div>
    </div> : null;

    return <div>
      <Hero containerClass="has-text-centered" type="primary">
        <h1 className="title is-size-2">{details.cartItems[0].productName}</h1>
        <h2 className="subtitle is-size-4">{moment(details.cartItems[0].productDate).format('dddd, DD MMMM YYYY')}</h2>
      </Hero>

      <Hero type="light">
        <div className="explanation-block content">
          <h3>Posting the cart</h3>
          <p>We posted the cart after clicking the <strong>"Continue"</strong> button on the last screen. Since it was successful, we have now come to this screen. This extra step isn't necessary in a live booking application, but here it serves as a nice pit stop to explain the posting and checking out of the cart.</p>
          <p>This posting of the cart is the first step to booking products and is done via <code>POST /carts</code> with all of the required information, including passenger details. At this point, the Livn API does an availability and rates check. Therefore, it's possible that either or both of these could change between any earlier checks and the posting of the cart.</p>
          <p>Keep in mind that the maximum number of passengers per cart is 10.</p>
        </div>
      </Hero>

      <div className="section">
        <div className="container">
          <div className="columns is-variable is-8">
            {postCartJSON}
            {responseCartJSON}
          </div>
        </div>
      </div>

      <Hero type="light">
        <div className="explanation-block content">
          <h3>Checking out the cart</h3>
          <p>The next step of checking out the cart is done using <code>GET /carts/&#123;cartId&#125;/checkout</code>. This process involves communication with supplier APIs and therefore may take a while to complete.</p>
          <p>By default the checkout is handled synchronously, i.e. it will not return your response until that reservation process is completed, unless the Synchronous header is used to explicitly opt for asynchronous processing, in which case you need to poll the cart again to check for its status.</p>
          <p>The status of the returned cart will determine which steps should be taken next.</p>
          <dl>
            <dt><var>PENDING_AUTHORISATION</var></dt>
            <dd>Confirmed reservations have been made with the external booking systems and the transaction is waiting for your re-confirmation. Failure to re-confirm within the allowed time (default: 40 minutes) will cause the entire transaction to be rolled back and all external reservations to be cancelled. <u>Next action:</u> Re-confirm successful checkout and abort rollback timeout by calling method <code>authorisePayment</code>.</dd>
            <dt><var>PENDING_CREDIT_CARD_PAYMENT</var></dt>
            <dd>Confirmed reservations have been made with the external booking systems and the transaction is waiting for your credit card payment. Failure to finalise payment within the allowed time (default: 40 minutes) will cause the entire transaction to be rolled back and all external reservations to be cancelled. <u>Next action:</u> Make credit card payment and abort rollback timeout by POST-ing payment details to <code>/carts/&#123;cartId&#125;/ccPayment</code>.</dd>
            <dt><var>COMPLETED</var></dt>
            <dd>The checkout has been completed (and where necessary re-confirmed). <u>Next action:</u> None as part of checkout. Ready to retrieve reservation details, print tickets, cancel reservations etc.</dd>
            <dt><var>FAILED_CHECKOUT</var></dt>
            <dd>A problem occurred during checkout. <u>Next action:</u> Cart becomes obsolete</dd>
          </dl>

          <p>We will checkout the cart after the <strong>"Book &amp; Pay"</strong> button is clicked. When the cart is checked out, the reservations are being held. This means that up until this point, it can be very easy for the customer to change their mind. On the other hand, this means that we do recommend saving checkout for a stage when the customer seems committed to booking the product.</p>
        </div>
      </Hero>

      <div className="section">
        <div className="container">
          <div className="explanation-block content">
            <h3>Complete the booking</h3>
            <p>The next step will be to simply complete the booking by checking out the cart as described above.</p>
            <p>We do also provide a demonstration of our credit card method within the code, but not within this live demo application. To find more information about the credit card payment method, you can either check out this project's' <a href="https://gitlab.com/livnDevelopers/rahisi" rel="noopener noreferrer" target="_blank">GitLab Repository</a>.</p>
            <div className="field is-grouped is-grouped-centered">
              <div className="control">
                <Link className="button is-link" to={link}>Back to passenger details</Link>
              </div>
              <div className="control">
                <Button className="is-primary" clickEvent={() => getCartCheckout({ cartId, parentId }, history, 'NONE')} type="button">Checkout &#38; confirm the booking</Button>
              </div>
              {/* <div className="control">
                <Button className="is-text" clickEvent={() => getCartCheckout({ cartId, parentId }, history, 'CREDIT_CARD')} type="button">Credit card payment</Button>
              </div> */}
            </div>
          </div>
        </div>
      </div>
    </div>
  }
}

const mapStateToProps = state => ({
  cart: state.cart,
  message: state.message
})

export default connect(mapStateToProps, { getCart, getCartCheckout })(CartCheckout);