import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Formik } from 'formik';
import { isEmpty } from 'lodash';
import * as Yup from 'yup';

import { postCartCCPayment } from 'store/actions/actions';

import Button from 'components/UI/Button/Button';
import Input from 'components/UI/Input/Input';
import Notification from 'components/UI/Notification/Notification';

const validationSchema = Yup.object().shape({
  cvc: Yup.number().required(),
  expMonth: Yup.number().required(),
  expYear: Yup.number().required(),
  number: Yup.number().required()
})

class PaymentForm extends Component {
  render() {
    const { cartId, currency, details, history, message, parentId, postCartCCPayment } = this.props;

    if (isEmpty(details)) {
      return <Notification contained={true} spinner={true}>Retrieving retail totals...</Notification>
    }

    return <div className="has-background-white padded-large">
      <div className="is-size-4">Total: {currency} {details.netAmount}</div>
      <hr />
      <Formik
        initialValues={{
          cvc: '242',
          expMonth: '03',
          expYear: '2020',
          number: '4242424242424242'
        }}
        validationSchema={validationSchema}
        onSubmit={(values, { setSubmitting }) => {
          postCartCCPayment(cartId, {
            ...values,
            amount: details.netAmount,
            currency
          }, parentId, history)
          setSubmitting(false)
        }}
        render={({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
        }) => <form onSubmit={handleSubmit}>
            <div className="columns">
              <div className="column">
                <Input
                  errors={errors.number}
                  label="Card Number"
                  name="number"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  touched={touched.number}
                  type="number"
                  value={values.number}
                />
              </div>
            </div>
            <div className="columns">
              <div className="column">
                <Input
                  errors={errors.expMonth}
                  label="Expiry Month"
                  name="expMonth"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  placeholder="MM"
                  touched={touched.expMonth}
                  type="number"
                  value={values.expMonth}
                />
              </div>
              <div className="column">
                <Input
                  errors={errors.expYear}
                  label="Expiry Year"
                  name="expYear"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  placeholder="YYYY"
                  touched={touched.expYear}
                  type="number"
                  value={values.expYear}
                />
              </div>
              <div className="column">
                <Input
                  errors={errors.cvc}
                  label="CVC"
                  name="cvc"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  touched={touched.cvc}
                  type="number"
                  value={values.cvc}
                />
              </div>
            </div>

            {!isEmpty(message.text) ? <Notification type={message.type}>{message.text}</Notification> : null}

            <div className="field is-grouped is-grouped-right">
              <div className="control">
                <Button className="is-primary" disabled={isSubmitting} type="submit">Submit</Button>
              </div>
            </div>
          </form>
        }
      />
    </div>
  }
}

const mapStateToProps = state => ({
  message: state.message
})

export default connect(mapStateToProps, { postCartCCPayment })(withRouter(PaymentForm));