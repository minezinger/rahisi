import React, { Component } from 'react';
import { connect } from 'react-redux';
import { isEmpty } from 'lodash';

import CodeBlock from 'components/UI/CodeBlock/CodeBlock';
import Notification from 'components/UI/Notification/Notification';

class CartTerms extends Component {
  render() {
    const { distributor, operatorTerms } = this.props;

    if (isEmpty(distributor.tnc) && isEmpty(operatorTerms)) {
      return <Notification contained={true} spinner={true}>Retrieving terms and conditions...</Notification>
    }

    const termBoxStyle = { height: '300px', overflow: 'auto', paddingRight: '10px' };
    const operatorElement = !isEmpty(operatorTerms) ? <div className="card-content">
      <h2 className="is-size-3">Operator Terms &#38; Conditions</h2>
      <div className="is-size-7 has-text-justified" dangerouslySetInnerHTML={{ __html: operatorTerms }} style={termBoxStyle}></div>
    </div> : null;

    return <div className="has-background-white margin-bottom-large padded-large">
      <h2 className="is-size-3">Distributor Terms &#38; Conditions</h2>
      <div className="is-size-7 has-text-justified margin-bottom" dangerouslySetInnerHTML={{ __html: distributor.tnc.content }} style={termBoxStyle}></div>
      <p>The JSON used to display the above terms came from the <code>GET /user</code> route. This returned the details for our Livn API user, under <var>user.tnc</var>, shown below.</p>
      <CodeBlock code={JSON.stringify(distributor, null, 2)} height="300" language="json" />

      {operatorElement}
    </div>
  }
}

const mapStateToProps = state => ({
  product: state.product
})

export default connect(mapStateToProps)(CartTerms);