import React, { Component } from 'react';
import { connect } from 'react-redux';
import { isEmpty } from 'lodash';

import { getCart, getCartTickets } from 'store/actions/actions';

import CodeBlock from 'components/UI/CodeBlock/CodeBlock';
import Hero from 'components/UI/Hero/Hero';
import Notification from 'components/UI/Notification/Notification';

import Simple from 'components/Reservation/Simple/Simple';

class CartComplete extends Component {
  componentDidMount() {
    const { cart: { details }, getCart, match: { params: { cartId } } } = this.props;

    /** If we don't already have the cart loaded, we're making sure to get it. */
    if (isEmpty(details) || parseInt(details.id, 10) !== parseInt(cartId, 10)) {
      getCart(cartId);
    }
  }

  render() {
    const { cart: { details, responseCart }, getCartTickets, match: { params: { cartId } } } = this.props;

    if (isEmpty(details) || parseInt(details.id, 10) !== parseInt(cartId, 10)) {
      return <Notification contained={true} spinner={true}>Retrieving cart...</Notification>
    }

    return <div>
      <Hero containerClass="has-text-centered" type="primary">
        <h1 className="title is-size-2">Reservation Complete</h1>
        <h2 className="subtitle is-size-4">Reference: {details.reservations[0].globalRef}</h2>
      </Hero>

      <div className="section">
        <div className="container">
          <div className="columns">
            <div className="column is-half">
              {details.reservations.map(reservation => <Simple details={reservation} fullDetails={true} getTickets={getCartTickets} id={cartId} key={reservation.id} />)}
            </div>
            <div className="column is-half">
              <div className="explanation-block content">
                <h3>The completed cart</h3>
                <CodeBlock code={responseCart} height="400" language="json" />
              </div>
              <div className="explanation-block content">
                <h3>Getting vouchers</h3>
                <p>A collated PDF of all vouchers can then be obtained through <code>GET /carts/&#123;cartId&#125;/tickets</code>. Furthermore, you can now use <code>GET /carts/&#123;cartId&#125;/reservations</code> to get the reservation details for the booked items so that you can also use any of the reservation calls.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  }
}

const mapStateToProps = state => ({
  cart: state.cart
})

export default connect(mapStateToProps, { getCart, getCartTickets })(CartComplete);