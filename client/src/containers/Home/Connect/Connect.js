import React from 'react';

import Hero from 'components/UI/Hero/Hero';

const Connect = () => {
  return <div>
    <Hero containerClass="has-text-centered" size="medium" type="primary">
      <h1 className="title is-size-1">Getting Connected</h1>
      <h3 className="subtitle is-size-3">The Livn API is your one stop shop for all tours and activities</h3>
    </Hero>

    <div className="section">
      <div className="container content">
        <p>Start by checking out our comprehensive API documentation. There you can sign up on your own for access to our sandbox environment in which you can interact with our API and make some basic calls against our demo data set. Please note that our sandbox environment is offered "as-is" and we do not offer support for evaluation users trialling our API.</p>
        <p>Once you and your commercial colleagues decide to go ahead implementing an integration with the Livn API, please contact our commercial team to put in place an agreement between Livn and you.</p>
        <p>Our team will set up a development environment for you tailored to your specific business requirements. Examples of such specific requirements include your choice of currency, booking fees, credit terms, and whether you intend to transact with suppliers under a straightforward wholesale reseller model, or a "direct connect" model more suited to with large volume distributors with existing agreements with product suppliers.</p>
        <h3 className="is-size-3">Next steps</h3>
        <p>As your project progresses, we will do our best to help you implement your API client with our knowledge base and providing one-to-one access to our developer support team. As part of this process we are happy to assist with end-to-end testing and troubleshooting should you find yourself stuck in your implementation efforts. After not too long though, you should hopefully find yourself with an integration that passes all tests, and is ready to go live!</p>
        <p>The implementation phase is concluded by a quality control, or "production certification" step. This is where we at Livn complete a set of independent tests of your integration to double check that there are no open issues remaining that prevents us from giving you access to our production environment, and starting the route all your calls to our live product warehouse and booking engine.</p>
        <p>We will migrate your integration to our production environment, after which your new integration is live.</p>
      </div>
    </div>
  </div>
}

export default Connect;