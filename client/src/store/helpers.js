import axios from 'axios';
import { isEmpty, isString } from 'lodash';

export const download = (data, filename, mime) => {
  var blob = new Blob([data], { type: mime || 'application/octet-stream' });
  if (typeof window.navigator.msSaveBlob !== 'undefined') {
    window.navigator.msSaveBlob(blob, filename);
  }
  else {
    var blobURL = window.URL.createObjectURL(blob);
    var tempLink = document.createElement('a');
    tempLink.style.display = 'none';
    tempLink.href = blobURL;
    tempLink.setAttribute('download', filename);
    if (typeof tempLink.download === 'undefined') {
      tempLink.setAttribute('target', '_blank');
    }
    document.body.appendChild(tempLink);
    tempLink.click();
    document.body.removeChild(tempLink);
    window.URL.revokeObjectURL(blobURL);
  }
}

export const errorHandler = (dispatch, error, type) => {
  if (error.response) {
    const { response: { data } } = error;
    if (data === 'Unauthorized') {
      dispatch({ type, payload: { message: 'You are not authorised to complete this action. Please make sure that you are logged in.' } })
    } else {
      dispatch({ type, payload: data })
    }
  } else if (!isEmpty(error) && error.config) {
    dispatch({ type, payload: { message: 'There was an problem connecting to the Rahisi server. Please wait a few minutes before refreshing the page and trying again.' } })
  } else if (isString(error) && type) {
    dispatch({ type, payload: { message: error } })
  }
}

export const getAuthToken = () => {
  let token = JSON.parse(localStorage.getItem('token'));
  if (token) {
    return { 'Authorization': 'Bearer ' + token };
  } else {
    return {};
  }
}

export const setAuthToken = token => {
  if (token) {
    axios.defaults.headers.common['Authorization'] = token;
  } else {
    delete axios.defaults.headers.common['Authorization'];
  }
}

export const updateObject = (oldObject, updatedProperties) => {
  return {
    ...oldObject,
    ...updatedProperties
  }
}