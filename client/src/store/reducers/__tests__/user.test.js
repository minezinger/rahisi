import userReducer from 'store/reducers/user';
import {
  USER_LOGIN_START,
  USER_LOGIN_COMPLETE,
  USER_LOGOUT_COMPLETE
} from 'store/actions/types';
import { isEmpty } from 'lodash';

describe('user reducer', () => {
  it('handles actions of type unknown', () => {
    const newState = userReducer({}, {})
    expect(newState).toEqual({})
  })

  it('handles actions of type USER_LOGIN_START', () => {
    const action = {
      type: USER_LOGIN_START,
      payload: {
        email: 'test@livn.world',
        password: 'password'
      }
    }
    const newState = userReducer({}, action)
    expect(newState).toEqual({ loading: true })
  })

  it('handles actions of type USER_LOGIN_COMPLETE', () => {
    const token = 'testtoken';
    const action = {
      type: USER_LOGIN_COMPLETE,
      payload: {
        isAuthenticated: !isEmpty(token),
        tokenExpiry: 3600,
        user: {
          email: 'test@livn.world',
          name: 'Test User'
        }
      }
    }
    const newState = userReducer({}, action)
    expect(newState).toEqual({
      isAuthenticated: true,
      loading: false,
      user: {
        email: 'test@livn.world',
        name: 'Test User'
      }
    })
  })

  it('handles actions of type USER_LOGOUT_COMPLETE', () => {
    const action = {
      type: USER_LOGOUT_COMPLETE
    }
    const newState = userReducer({}, action)
    expect(newState).toEqual({
      isAuthenticated: false
    })
  })
})