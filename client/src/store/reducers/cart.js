import {
  CART_CCPAY_START,
  CART_CCPAY_COMPLETE,
  CART_CHECKOUT_START,
  CART_CHECKOUT_COMPLETE,
  CART_CLEAR,
  CART_ERROR_RESPONSE,
  CART_GET_START,
  CART_GET_COMPLETE,
  CART_LOADING_STOP,
  CART_POST_START,
  CART_POST_COMPLETE,
  CART_RETAILTOTALS_START,
  CART_RETAILTOTALS_COMPLETE,
  CART_TICKET_START,
  CART_TICKET_COMPLETE
} from 'store/actions/types';
import { updateObject } from 'store/helpers';

const INITIAL_STATE = {
  details: {},
  loading: false,
  postCart: '',
  responseCart: '',
  responseError: '',
  responseRetailTotals: ''
}

const cartCCPayComplete = (state, action) => {
  const { object, string } = action.payload;
  return updateObject(state, { details: object, loading: false, responseCart: string });
}

const cartCheckoutComplete = (state, action) => {
  const { object, string } = action.payload;
  return updateObject(state, { details: object, loading: false, responseCart: string });
}

const cartGetComplete = (state, action) => {
  const { object, string } = action.payload;
  return updateObject(state, { details: object, loading: false, responseCart: string });
}

const cartPostComplete = (state, action) => {
  const { post, object, string } = action.payload;
  return updateObject(state, { details: object, loading: false, postCart: post, responseCart: string });
}

const cartRetailTotalsComplete = (state, action) => {
  const { object, string } = action.payload;
  return updateObject(state, { loading: false, retailTotals: object, responseRetailTotals: string });
}

/**
 * Generic
 */
const cartClear = (state) => {
  return updateObject(state, { details: {}, postCart: '', responseCart: '' });
}

const cartErrorResponse = (state, action) => {
  return updateObject(state, { loading: false, responseError: action.payload });
}

const cartLoadingStart = (state) => {
  return updateObject(state, { loading: true });
}

const cartLoadingComplete = (state) => {
  return updateObject(state, { loading: false });
}

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case CART_CCPAY_START: return cartLoadingStart(state);
    case CART_CCPAY_COMPLETE: return cartCCPayComplete(state, action);
    case CART_CHECKOUT_START: return cartLoadingStart(state);
    case CART_CHECKOUT_COMPLETE: return cartCheckoutComplete(state, action);
    case CART_CLEAR: return cartClear(state);
    case CART_ERROR_RESPONSE: return cartErrorResponse(state, action);
    case CART_GET_START: return cartLoadingStart(state);
    case CART_GET_COMPLETE: return cartGetComplete(state, action);
    case CART_LOADING_STOP: return cartLoadingComplete(state);
    case CART_POST_START: return cartLoadingStart(state);
    case CART_POST_COMPLETE: return cartPostComplete(state, action);
    case CART_RETAILTOTALS_START: return cartLoadingStart(state);
    case CART_RETAILTOTALS_COMPLETE: return cartRetailTotalsComplete(state, action);
    case CART_TICKET_START: return cartLoadingStart(state);
    case CART_TICKET_COMPLETE: return cartLoadingComplete(state);
    default: return state;
  }
}