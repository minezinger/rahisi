import {
  RESERVATION_GET_START,
  RESERVATION_GET_COMPLETE,
  RESERVATION_SEARCH_START,
  RESERVATION_SEARCH_COMPLETE,
  RESERVATION_TICKET_START,
  RESERVATION_TICKET_COMPLETE
} from 'store/actions/types';
import { updateObject } from 'store/helpers';

const INITIAL_STATE = {
  details: {},
  list: [],
  loading: false,
  responseReservationSearch: ''
}

const reservationGetComplete = (state, action) => {
  return updateObject(state, { details: action.payload, loading: false });
}

const reservationSearchComplete = (state, action) => {
  return updateObject(state, { list: action.payload, loading: false, responseReservationSearch: JSON.stringify(action.payload, null, 2) });
}

/**
 * Generic Loading
 */
const reservationLoadingStart = (state) => {
  return updateObject(state, { loading: true });
}

const reservationLoadingComplete = (state) => {
  return updateObject(state, { loading: false });
}

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case RESERVATION_GET_START: return reservationLoadingStart(state);
    case RESERVATION_GET_COMPLETE: return reservationGetComplete(state, action);
    case RESERVATION_SEARCH_START: return reservationLoadingStart(state);
    case RESERVATION_SEARCH_COMPLETE: return reservationSearchComplete(state, action);
    case RESERVATION_TICKET_START: return reservationLoadingStart(state);
    case RESERVATION_TICKET_COMPLETE: return reservationLoadingComplete(state, action);
    default: return state;
  }
}