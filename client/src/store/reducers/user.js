import {
  USER_GET_START,
  USER_GET_COMPLETE,
  USER_LIST_START,
  USER_LIST_COMPLETE,
  USER_LOGIN_START,
  USER_LOGIN_COMPLETE,
  USER_LOGOUT_COMPLETE,
  USER_REGISTER_START,
  USER_REGISTER_COMPLETE
} from 'store/actions/types';
import { updateObject } from 'store/helpers';

const INITIAL_STATE = {
  current: {},
  details: {},
  isAuthenticated: false,
  list: [],
  loading: false
}

const userGetComplete = (state, action) => {
  return updateObject(state, { details: action.payload, loading: false });
}

const userListComplete = (state, action) => {
  return updateObject(state, { loading: false, list: action.payload });
}

const userLoginStart = (state) => {
  return updateObject(state, { current: undefined, loading: true });
}

const userLoginComplete = (state, action) => {
  const { isAuthenticated, user } = action.payload;
  return updateObject(state, { current: user, isAuthenticated, loading: false });
}

const userLogout = (state) => {
  return updateObject(state, { current: undefined, isAuthenticated: false });
}

/**
 * Generic Loading
 */
const userLoadingStart = (state) => {
  return updateObject(state, { loading: true });
}

const userLoadingComplete = (state) => {
  return updateObject(state, { loading: false });
}

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case USER_GET_START: return userLoadingStart(state);
    case USER_GET_COMPLETE: return userGetComplete(state, action);
    case USER_LIST_START: return userLoadingStart(state);
    case USER_LIST_COMPLETE: return userListComplete(state, action);
    case USER_LOGIN_START: return userLoginStart(state);
    case USER_LOGIN_COMPLETE: return userLoginComplete(state, action);
    case USER_LOGOUT_COMPLETE: return userLogout(state);
    case USER_REGISTER_START: return userLoadingStart(state);
    case USER_REGISTER_COMPLETE: return userLoadingComplete(state);
    default: return state;
  }
}