import {
  API_USER_GET_START,
  API_USER_GET_COMPLETE
} from 'store/actions/types';
import { updateObject } from 'store/helpers';

const INITIAL_STATE = {
  distributor: {},
  outletGroup: {}
}

const apiUserGetComplete = (state, action) => {
  return updateObject(state, action.payload);
}

/**
 * Generic Loading
 */
const apiUserLoadingStart = (state) => {
  return updateObject(state, { loading: true });
}

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case API_USER_GET_START: return apiUserLoadingStart(state);
    case API_USER_GET_COMPLETE: return apiUserGetComplete(state, action);
    default: return state;
  }
}