import {
  PRODUCT_DEPARTURES_MULTI_START,
  PRODUCT_DEPARTURES_MULTI_COMPLETE,
  PRODUCT_GET_START,
  PRODUCT_GET_COMPLETE,
  PRODUCT_OPTIONS_START,
  PRODUCT_OPTIONS_COMPLETE,
  PRODUCT_PAXDETAILS_START,
  PRODUCT_PAXDETAILS_COMPLETE,
  PRODUCT_PICKUPS_START,
  PRODUCT_PICKUPS_COMPLETE,
  PRODUCT_SELECTIONS_START,
  PRODUCT_SELECTIONS_COMPLETE,
  PRODUCT_TERMS_START,
  PRODUCT_TERMS_COMPLETE
} from 'store/actions/types';
import { updateObject } from 'store/helpers';

const INITIAL_STATE = {
  details: {},
  departures: [],
  loading: false,
  paxDetails: {},
  paxSelections: [],
  responseTerms: '',
  terms: []
}

const productDeparturesMultiComplete = (state, action) => {
  return updateObject(state, { departures: action.payload, loading: false });
}

const productGetComplete = (state, action) => {
  return updateObject(state, { details: action.payload, departures: [], loading: false, response: JSON.stringify(action.payload, null, 2) });
}

const productOptionsComplete = (state, action) => {
  return updateObject(state, { loading: false, paxSelections: action.payload });
}

const productPickupsComplete = (state, action) => {
  return updateObject(state, { loading: false, paxSelections: action.payload });
}

const productPaxDetailsComplete = (state, action) => {
  const { paxDetails } = action.payload;
  return updateObject(state, { loading: false, paxDetails });
}

const productSelectionsComplete = (state, action) => {
  return updateObject(state, { loading: false, paxSelections: action.payload });
}

const productTermsComplete = (state, action) => {
  const { object, string } = action.payload;
  return updateObject(state, { loading: false, responseTerms: string, terms: object });
}

/**
 * Generic Loading
 */
const productLoadingStart = (state) => {
  return updateObject(state, { loading: true });
}

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case PRODUCT_DEPARTURES_MULTI_START: return productLoadingStart(state);
    case PRODUCT_DEPARTURES_MULTI_COMPLETE: return productDeparturesMultiComplete(state, action);
    case PRODUCT_GET_START: return productLoadingStart(state);
    case PRODUCT_GET_COMPLETE: return productGetComplete(state, action);
    case PRODUCT_OPTIONS_START: return productLoadingStart(state);
    case PRODUCT_OPTIONS_COMPLETE: return productOptionsComplete(state, action);
    case PRODUCT_PAXDETAILS_START: return productLoadingStart(state);
    case PRODUCT_PAXDETAILS_COMPLETE: return productPaxDetailsComplete(state, action);
    case PRODUCT_PICKUPS_START: return productLoadingStart(state);
    case PRODUCT_PICKUPS_COMPLETE: return productPickupsComplete(state, action);
    case PRODUCT_TERMS_START: return productLoadingStart(state);
    case PRODUCT_TERMS_COMPLETE: return productTermsComplete(state, action);
    case PRODUCT_SELECTIONS_START: return productLoadingStart(state);
    case PRODUCT_SELECTIONS_COMPLETE: return productSelectionsComplete(state, action);
    default: return state;
  }
}