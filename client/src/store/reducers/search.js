import {
  SEARCH_DESTINATION_START,
  SEARCH_DESTINATION_COMPLETE,
  SEARCH_DESTINATION_FAILED,
  SEARCH_GET_START,
  SEARCH_GET_COMPLETE,
  SEARCH_GET_FAILED,
  SEARCH_OPERATOR_START,
  SEARCH_OPERATOR_COMPLETE,
  SEARCH_OPERATOR_PRODUCT_START,
  SEARCH_OPERATOR_PRODUCT_COMPLETE
} from 'store/actions/types';
import { updateObject } from 'store/helpers';

const INITIAL_STATE = {
  criteria: {},
  destinations: [],
  loading: false,
  operators: [],
  responseDestination: '',
  responseSearch: '',
  responseOperator: '',
  resultsOperator: []
}

const searchDestinationComplete = (state, action) => {
  return updateObject(state, { destinations: action.payload, loading: false, responseDestination: JSON.stringify(action.payload, null, 2) });
}

const searchGetComplete = (state, action) => {
  const { criteria, results: { object, string } } = action.payload;
  return updateObject(state, { criteria, loading: false, resultsDestination: object, responseSearch: string });
}

const searchOperatorComplete = (state, action) => {
  return updateObject(state, { loading: false, operators: action.payload, responseOperator: JSON.stringify(action.payload, null, 2) });
}

const searchOperatorProductComplete = (state, action) => {
  return updateObject(state, { loading: false, resultsOperator: action.payload, responseSearch: JSON.stringify(action.payload, null, 2) });
}

/**
 * Generic Loading
 */
const searchLoadingStart = (state) => {
  return updateObject(state, { loading: true });
}

const searchLoadingComplete = (state) => {
  return updateObject(state, { loading: false });
}

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case SEARCH_DESTINATION_START: return searchLoadingStart(state);
    case SEARCH_DESTINATION_COMPLETE: return searchDestinationComplete(state, action);
    case SEARCH_DESTINATION_FAILED: return searchLoadingComplete(state);
    case SEARCH_GET_START: return searchLoadingStart(state);
    case SEARCH_GET_COMPLETE: return searchGetComplete(state, action);
    case SEARCH_GET_FAILED: return searchLoadingComplete(state);
    case SEARCH_OPERATOR_START: return searchLoadingStart(state);
    case SEARCH_OPERATOR_COMPLETE: return searchOperatorComplete(state, action);
    case SEARCH_OPERATOR_PRODUCT_START: return searchLoadingStart(state);
    case SEARCH_OPERATOR_PRODUCT_COMPLETE: return searchOperatorProductComplete(state, action);
    default: return state;
  }
}