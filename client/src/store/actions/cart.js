import axios from 'store/actions/axios-config';
import {
  MESSAGE_CLEAR,
  MESSAGE_ERROR,
  CART_CCPAY_START,
  CART_CCPAY_COMPLETE,
  CART_CHECKOUT_START,
  CART_CHECKOUT_COMPLETE,
  CART_CLEAR,
  CART_ERROR_RESPONSE,
  CART_GET_START,
  CART_GET_COMPLETE,
  CART_LOADING_STOP,
  CART_POST_START,
  CART_POST_COMPLETE,
  CART_RETAILTOTALS_START,
  CART_RETAILTOTALS_COMPLETE,
  CART_TICKET_START,
  CART_TICKET_COMPLETE
} from 'store/actions/types';
import { download, errorHandler } from 'store/helpers';

/**
 * @name                cartStatusResolve
 * @description         Handles all the possible cart statuses in one spot.
 * @param               cart
 */
function cartStatusResolve(dispatch, cart, type, history, link, post) {
  switch (cart.status) {
    case 'COMPLETED': {
      dispatch({ type, payload: { object: cart, string: JSON.stringify(cart, null, 2) } });
      if (history && link) {
        history.push(link);
      }
      break;
    }
    case 'EXPIRED': {
      /** TODO: In this event, redirect the user to a page specifically made for
       * expired/timed out carts. This way it can be reused in other actions and
       * we won't be handling the same error in multiple containers.
       */
      dispatch({ type: CART_ERROR_RESPONSE, payload: JSON.stringify(cart, null, 2) })
      dispatch(errorHandler(dispatch, 'The cart has expired.', MESSAGE_ERROR))
      break;
    }
    case 'FAILED_CHECKOUT': {
      const { problems } = cart;
      let message = '';
      problems.map(problem => {
        const { code, details } = problem;
        switch (code) {
          case 'UNSPECIFIED': {
            message += details + ' ';
            break;
          }
          default: {
            break;
          }
        }
      })
      dispatch({ type: CART_ERROR_RESPONSE, payload: JSON.stringify(cart, null, 2) })
      dispatch(errorHandler(dispatch, message, MESSAGE_ERROR))
      break;
    }
    case 'FAILED_LIVE_CHECKS': {
      dispatch({ type: CART_ERROR_RESPONSE, payload: JSON.stringify(cart, null, 2) })
      dispatch({ type: MESSAGE_ERROR, payload: { message: 'The cart has failed its live checks.' } })
      break;
    }
    case 'PENDING_CREDIT_CARD_PAYMENT': {
      dispatch({ type, payload: { object: cart, string: JSON.stringify(cart, null, 2) } })
      if (history && link) {
        history.push(link);
      }
      break;
    }
    case 'READY_FOR_CHECKOUT': {
      dispatch({ type, payload: { post, object: cart, string: JSON.stringify(cart, null, 2) } })
      if (history && link) {
        history.push(link);
      }
      break;
    }
    case 'TIMED_OUT_AFTER_CHECKOUT': {
      /** TODO: In this event, redirect the user to a page specifically made for
       * expired/timed out carts. This way it can be reused in other actions and
       * we won't be handling the same error in multiple containers.
       */
      dispatch({ type: CART_ERROR_RESPONSE, payload: JSON.stringify(cart, null, 2) })
      dispatch(errorHandler(dispatch, 'The cart has timed out after checkout.', MESSAGE_ERROR))
      break;
    }
    default: {
      dispatch({ type: CART_LOADING_STOP })
      dispatch(errorHandler(dispatch, 'There was an error with the cart.', MESSAGE_ERROR))
      break;
    }
  }
}

/**
 * @name                getCart
 * @description         Gets a single cart by its id.
 * @param               cartId
 * @route               The Livn API route for this is https://*.livngds.com/livngds/api/api/carts/{cartId}
 */
export function getCart(cartId) {
  return function (dispatch) {
    dispatch({ type: MESSAGE_CLEAR })
    dispatch({ type: CART_GET_START })
    axios.get('/carts/' + cartId)
      .then(response => cartStatusResolve(dispatch, response.data, CART_GET_COMPLETE))
      .catch(error => {
        dispatch({ type: CART_LOADING_STOP })
        errorHandler(dispatch, error, MESSAGE_ERROR)
      })
  }
}

/**
 * @name                getCartCheckout
 * @description         Once a cart has been posted, pre-validated and live availability and prices checked, use this
 *                      request to start the process of making confirmed reservations in the respective supplier
 *                      reservation systems. As this process can take more than a few seconds, depending on the supplier
 *                      system/s, it has been implemented as an asynchronous call. Use getCart to poll the cart and
 *                      check for status changes.
 *                      Note: Depending on the API user's outlet group's relation to the distributor, a successfully
 *                      checked out cart may still be deemed pending while awaiting a payment authorisation, or
 *                      otherwise force a rollback/cancellation should it not be authorised within a specific timeframe.
 * @param               { cartId, parentId }, history
 * @route               The Livn API route for this is https://*.livngds.com/livngds/api/carts/{cartId}/checkout
 */
export function getCartCheckout({ cartId, parentId }, history, paymentType) {
  return function (dispatch) {
    dispatch({ type: MESSAGE_CLEAR })
    dispatch({ type: CART_CLEAR })
    dispatch({ type: CART_CHECKOUT_START })
    axios.get('/carts/' + cartId + '/checkout')
      .then(response => {
        const { data } = response;
        let link = '/be/cart/' + data.id + '/tour/' + parentId;
        switch (paymentType) {
          case 'NONE': {
            link += '/complete';
            break;
          }
          case 'CREDIT_CARD': {
            link += '/pay';
            break;
          }
          default: {
            break;
          }
        }
        cartStatusResolve(dispatch, data, CART_CHECKOUT_COMPLETE, history, link);
      })
      .catch(error => {
        dispatch({ type: CART_LOADING_STOP })
        errorHandler(dispatch, error, MESSAGE_ERROR)
      })
  }
}

/**
 * @name                getCartRetailTotals
 * @description         Manually requests the retail totals (gross, net, and commission totals) for a specified cart.
 * @param               cartId
 * @route               The Livn API route for this is https://*.livngds.com/livngds/api/carts/{cartId}/retailTotals
 */
export function getCartRetailTotals(cartId) {
  return function (dispatch) {
    dispatch({ type: MESSAGE_CLEAR })
    dispatch({ type: CART_RETAILTOTALS_START })
    axios.get('/carts/' + cartId + '/retailTotals')
      .then(response => {
        const { data } = response;
        dispatch({ type: CART_RETAILTOTALS_COMPLETE, payload: { object: data, string: JSON.stringify(data, null, 2) } })
      })
      .catch(error => {
        dispatch({ type: CART_LOADING_STOP })
        errorHandler(dispatch, error, MESSAGE_ERROR)
      })
  }
}

/**
 * @name                getCartTickets
 * @description         Retrieves a collated PDF with all tickets for the specified cart.
 * @param               cartId
 * @route               The Livn API route for this is https://*.livngds.com/livngds/api/carts/{cartId}/tickets
 */
export function getCartTickets(cartId) {
  return function (dispatch) {
    dispatch({ type: MESSAGE_CLEAR })
    dispatch({ type: CART_TICKET_START })
    axios.get('/carts/' + cartId + '/tickets', {
      headers: { 'Accept': 'application/pdf' },
      responseType: 'arraybuffer'
    })
      .then(response => {
        dispatch({ type: CART_TICKET_COMPLETE })
        dispatch(download(response.data, `Tickets-${cartId}.pdf`));
      })
      .catch(error => {
        dispatch({ type: CART_LOADING_STOP })
        errorHandler(dispatch, error, MESSAGE_ERROR)
      })
  }
}

/**
 * @name                postCart
 * @description         Post a new cart including product and pick-up selection and pax details. If supplied data passes
 *                      pre-commit validation, you will receive back your cart with a newly assigned id. At the same
 *                      time the system will perform a live availability and rates check in the background. Use
 *                      getCart to poll the cart and check for problems or status changes, including when the cart is
 *                      ready to be taken through checkout.
 * @param               { cartItems, parentId, paxes }, history
 * @route               The Livn API route for this is https://*.livngds.com/livngds/api/carts
 */
export function postCart({ cartItems, parentId, paxes, productId }, history) {
  return function (dispatch) {
    dispatch({ type: MESSAGE_CLEAR })
    dispatch({ type: CART_CLEAR })
    dispatch({ type: CART_POST_START })
    const cart = { cartItems, paxes };
    axios.post('/carts', cart)
      .then(response => {
        const { data } = response;
        const parentOrProductId = parentId ? parentId : productId;
        const link = '/be/cart/' + data.id + '/tour/' + parentOrProductId + '/checkout';
        cartStatusResolve(dispatch, data, CART_POST_COMPLETE, history, link, JSON.stringify(cart, null, 2));
      })
      .catch(error => {
        dispatch({ type: CART_LOADING_STOP })
        errorHandler(dispatch, error, MESSAGE_ERROR)
      })
  }
}

/**
 * @name                postCartCCPayment
 * @description         Once a cart has been sucessfully checked out and confirmed reservations have been made in the
 *                      respective supplier reservation systems, it may be required for the retailer, i.e. API user, to
 *                      make a payment with credit card.This request can be used to process credit card payment and
 *                      prevent the checked out cart from timing out and being rolled back.
 * @param               cartId, paymentDetails
 * @route               The Livn API route for this is https://*.livngds.com/livngds/api/carts/{cartId}/ccPayment
 */
export function postCartCCPayment(cartId, paymentDetails, parentId, history) {
  return function (dispatch) {
    dispatch({ type: MESSAGE_CLEAR })
    dispatch({ type: CART_CCPAY_START })
    axios.post('/carts/' + cartId + '/ccPayment', { creditCardPayment: paymentDetails })
      .then(response => {
        const { data } = response;
        const link = '/be/cart/' + data.id + '/tour/' + parentId + '/complete';
        cartStatusResolve(dispatch, data, CART_CCPAY_COMPLETE, history, link);
      })
      .catch(error => {
        dispatch({ type: CART_LOADING_STOP })
        errorHandler(dispatch, error, MESSAGE_ERROR)
      })
  }
}