import { isEmpty } from 'lodash';

import axios from 'store/actions/axios-config';
import {
  MESSAGE_CLEAR,
  MESSAGE_ERROR,
  MESSAGE_SUCCESS,
  USER_LOGIN_START,
  USER_LOGIN_COMPLETE,
  USER_DELETE_START,
  USER_DELETE_COMPLETE,
  USER_GET_START,
  USER_GET_COMPLETE,
  USER_LIST_START,
  USER_LIST_COMPLETE,
  USER_LOGOUT_START,
  USER_LOGOUT_COMPLETE,
  USER_REGISTER_START,
  USER_REGISTER_COMPLETE
} from 'store/actions/types';
import { getAPIUser } from 'store/actions/actions';
import { setAuthToken, errorHandler } from 'store/helpers';

/**
 * @name                checkAuthTimeout
 * @description         Sets a timeout after which the user is logged out via logoutUser.
 * @param               expirationTime
 */
export function checkAuthTimeout(expirationTime) {
  return function (dispatch) {
    setTimeout(() => {
      dispatch(logoutUser());
    }, expirationTime * 1000)
  }
}

/**
 * @name                deleteUser
 * @description         Delete a single user.
 * @param               userId
 */
export function deleteUser(userId) {
  return function (dispatch) {
    dispatch({ type: MESSAGE_CLEAR })
    dispatch({ type: USER_DELETE_START })
    axios.delete('/users/' + userId)
      .then(response => dispatch({ type: USER_DELETE_COMPLETE, payload: response.data }))
      .catch(error => errorHandler(dispatch, error, MESSAGE_ERROR))
  }
}

/**
 * @name                getUser
 * @description         Get the details for a single user by their id.
 * @param               userId
 */
export function getUser(userId) {
  return function (dispatch) {
    dispatch({ type: MESSAGE_CLEAR })
    dispatch({ type: USER_GET_START })
    axios.get('/users/' + userId)
      .then(response => dispatch({ type: USER_GET_COMPLETE, payload: response.data }))
      .catch(error => errorHandler(dispatch, error, MESSAGE_ERROR))
  }
}

/**
 * @name                getUserList
 * @description         Get the details for all users.
 */
export function getUserList() {
  return function (dispatch) {
    dispatch({ type: MESSAGE_CLEAR })
    dispatch({ type: USER_LIST_START })
    axios.get('/users/list')
      .then(response => dispatch({ type: USER_LIST_COMPLETE, payload: response.data }))
      .catch(error => errorHandler(dispatch, error, MESSAGE_ERROR))
  }
}

/**
 * @name                logoutUser
 * @description         Deauthorise a user.
 */
export function logoutUser() {
  return function (dispatch) {
    dispatch({ type: MESSAGE_CLEAR })
    dispatch({ type: USER_LOGOUT_START })
    localStorage.removeItem('jwtToken');
    dispatch({ type: USER_LOGOUT_COMPLETE })
  }
}

/**
 * @name                postUserLogin
 * @description         Authorise a user using form data.
 * @param               values, history
 */
export function postUserLogin(values, history) {
  return function (dispatch) {
    dispatch({ type: MESSAGE_CLEAR })
    dispatch({ type: USER_LOGIN_START })
    axios.post('/users/login', values)
      .then(response => {
        const { token, tokenExpiry, user } = response.data;
        localStorage.setItem('jwtToken', token);
        setAuthToken(token);
        dispatch({ type: USER_LOGIN_COMPLETE, payload: { isAuthenticated: !isEmpty(token), user } })
        dispatch(checkAuthTimeout(tokenExpiry));
        dispatch(getAPIUser());
        history.push('/be/destination');
      })
      .catch(error => errorHandler(dispatch, error, MESSAGE_ERROR))
  }
}

/**
 * @name                postUserRegister
 * @description         Register a user using form data.
 * @param               values
 */
export function postUserRegister(values) {
  return function (dispatch) {
    dispatch({ type: MESSAGE_CLEAR })
    dispatch({ type: USER_REGISTER_START })
    axios.post('/users/register', values)
      .then(response => {
        dispatch({ type: USER_REGISTER_COMPLETE })
        dispatch({ type: MESSAGE_SUCCESS, payload: response.data })
      })
      .catch(error => errorHandler(dispatch, error, MESSAGE_ERROR))
  }
}