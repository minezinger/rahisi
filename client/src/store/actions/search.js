import { isEmpty } from 'lodash';

import axios from 'store/actions/axios-config';
import {
  MESSAGE_CLEAR,
  MESSAGE_ERROR,
  MESSAGE_GET,
  SEARCH_DESTINATION_START,
  SEARCH_DESTINATION_COMPLETE,
  SEARCH_DESTINATION_FAILED,
  SEARCH_GET_START,
  SEARCH_GET_COMPLETE,
  SEARCH_GET_FAILED,
  SEARCH_OPERATOR_START,
  SEARCH_OPERATOR_COMPLETE,
  SEARCH_OPERATOR_PRODUCT_START,
  SEARCH_OPERATOR_PRODUCT_COMPLETE
} from 'store/actions/types';
import { errorHandler } from 'store/helpers';

/**
 * @name                getDestinations
 * @description         Get a list of all airports, in whose vicinity (radius 100km) there are bookable products, and
 *                      the nearest cities (which a population greater than 50,000) where there are bookable products.
 * @route               The Livn API routes for this are https://*.livngds.com/livngds/api/products/airports and
 *                      https://*.livngds.com/livngds/api/products/cities
 */
export function getDestinations() {
  return function (dispatch) {
    dispatch({ type: MESSAGE_CLEAR })
    dispatch({ type: SEARCH_DESTINATION_START })
    axios.get('/products/airportsAndCities')
      .then(response => {
        dispatch({ type: SEARCH_DESTINATION_COMPLETE, payload: response.data })
      })
      .catch(error => {
        dispatch({ type: SEARCH_DESTINATION_FAILED })
        errorHandler(dispatch, error, MESSAGE_ERROR)
      })
  }
}

/**
 * @name                getOperators
 * @description         Retrieves all bookable operators connected to the API user's distributor.
 * @route               The Livn API route for this is https://*.livngds.com/livngds/api/operators
 */
export function getOperators() {
  return function (dispatch) {
    dispatch({ type: MESSAGE_CLEAR })
    dispatch({ type: SEARCH_OPERATOR_START })
    axios.get('/operators')
      .then(response => {
        dispatch({ type: SEARCH_OPERATOR_COMPLETE, payload: response.data })
      })
      .catch(error => errorHandler(dispatch, error, MESSAGE_ERROR))
  }
}

/**
 * @name                getOperatorProducts
 * @description         Get a list of all products from a certain operator.
 * @param               operatorId 
 * @route               The Livn API route for this is https://*.livngds.com/livngds/api/operators/{opId}/products
 */
export function getOperatorProducts({ operator }) {
  return function (dispatch) {
    dispatch({ type: MESSAGE_CLEAR })
    dispatch({ type: SEARCH_OPERATOR_PRODUCT_START })
    axios.get('/operators/' + operator + '/products')
      .then(response => {
        const { data } = response;
        if (data.length < 1) {
          errorHandler(dispatch, 'There were no products found for this operator. Please select another operator and try again.', MESSAGE_GET)
        }
        dispatch({ type: SEARCH_OPERATOR_PRODUCT_COMPLETE, payload: data })
      })
      .catch(error => errorHandler(dispatch, error, MESSAGE_ERROR))
  }
}

/**
 * @name                getSearch
 * @description         Search for products by destination (either airport or city).
 * @route               The Livn API route for this is https://*.livngds.com/livngds/api/products/search
 */
export function getSearch(values) {
  return function (dispatch) {
    dispatch({ type: MESSAGE_CLEAR })
    dispatch({ type: SEARCH_GET_START })
    const { destination, endDate, keywords, passengers, startDate } = values;
    axios.get('/products/search', {
      params: {
        airport: destination && !isEmpty(destination.iata) ? destination : undefined,
        endDate: endDate ? endDate : undefined,
        fts: keywords ? keywords : undefined,
        location: destination && !!isEmpty(destination.iata) ? destination : undefined,
        requiredUnits: passengers ? passengers : undefined,
        radius: 100,
        startDate: startDate ? startDate : undefined
      }
    })
      .then(response => {
        const { data } = response;
        if (data.length < 1) {
          errorHandler(dispatch, 'There were no products found within your parameters. Please change the dates and try again.', MESSAGE_GET)
        }
        dispatch({ type: SEARCH_GET_COMPLETE, payload: { criteria: { endDate, passengers, startDate }, results: { object: data, string: JSON.stringify(data, null, 2) } } })
      })
      .catch(error => {
        dispatch({ type: SEARCH_GET_FAILED })
        errorHandler(dispatch, error, MESSAGE_ERROR)
      })
  }
}