export const MESSAGE_CLEAR = 'MESSAGE_CLEAR';
export const MESSAGE_ERROR = 'MESSAGE_ERROR';
export const MESSAGE_GET = 'MESSAGE_GET';
export const MESSAGE_SUCCESS = 'MESSAGE_SUCCESS';
export const MESSAGE_WARNING = 'MESSAGE_WARNING';

/**
 * API User Action Types
 */

/**
 * @function          getAPIUser
*/
export const API_USER_GET_START = 'API_USER_GET_START';
export const API_USER_GET_COMPLETE = 'API_USER_GET_COMPLETE';

/**
 * Cart Action Types
 */

export const CART_CLEAR = 'CART_CLEAR';
export const CART_ERROR_RESPONSE = 'CART_ERROR_RESPONSE';
export const CART_LOADING_STOP = 'CART_LOADING_STOP';

/**
 * @function          getCart
*/
export const CART_GET_START = 'CART_GET_START';
export const CART_GET_COMPLETE = 'CART_GET_COMPLETE';

/**
 * @function          getCartCheckout
*/
export const CART_CHECKOUT_START = 'CART_CHECKOUT_START';
export const CART_CHECKOUT_COMPLETE = 'CART_CHECKOUT_COMPLETE';

/**
 * @function          getCartRetailTotals
*/
export const CART_RETAILTOTALS_START = 'CART_RETAILTOTALS_START';
export const CART_RETAILTOTALS_COMPLETE = 'CART_RETAILTOTALS_COMPLETE';

/**
 * @function          getCartTickets
*/
export const CART_TICKET_START = 'CART_TICKET_START';
export const CART_TICKET_COMPLETE = 'CART_TICKET_COMPLETE';

/**
 * @function          postCart
*/
export const CART_POST_START = 'CART_POST_START';
export const CART_POST_COMPLETE = 'CART_POST_COMPLETE';

/**
 * @function          postCartCCPayment
*/
export const CART_CCPAY_START = 'CART_CCPAY_START';
export const CART_CCPAY_COMPLETE = 'CART_CCPAY_COMPLETE';

/**
 * Data Action Types
 */

/**
 * @function          getCountries
*/
export const DATA_COUNTRIES_START = 'DATA_COUNTRIES_START';
export const DATA_COUNTRIES_COMPLETE = 'DATA_COUNTRIES_COMPLETE';

/**
 * @function          getLanguages
*/
export const DATA_LANGUAGES_START = 'DATA_LANGUAGES_START';
export const DATA_LANGUAGES_COMPLETE = 'DATA_LANGUAGES_COMPLETE';

/**
 * Product Action Types
 */

/**
 * @function          getProductDeparturesMulti
*/
export const PRODUCT_DEPARTURES_MULTI_START = 'PRODUCT_DEPARTURES_MULTI_START';
export const PRODUCT_DEPARTURES_MULTI_COMPLETE = 'PRODUCT_DEPARTURES_MULTI_COMPLETE';

/**
 * @function          getProductPaxDetails
*/
export const PRODUCT_PAXDETAILS_START = 'PRODUCT_PAXDETAILS_START';
export const PRODUCT_PAXDETAILS_COMPLETE = 'PRODUCT_PAXDETAILS_COMPLETE';

/**
 * @function          getProduct
*/
export const PRODUCT_GET_START = 'PRODUCT_GET_START';
export const PRODUCT_GET_COMPLETE = 'PRODUCT_GET_COMPLETE';

/**
 * @function          getProductTerms 
*/
export const PRODUCT_TERMS_START = 'PRODUCT_TERMS_START';
export const PRODUCT_TERMS_COMPLETE = 'PRODUCT_TERMS_COMPLETE';

/**
 * @function          handleOptions 
*/
export const PRODUCT_OPTIONS_START = 'PRODUCT_OPTIONS_START';
export const PRODUCT_OPTIONS_COMPLETE = 'PRODUCT_OPTIONS_COMPLETE';

/**
 * @function          handlePaxSelections
*/
export const PRODUCT_SELECTIONS_START = 'PRODUCT_SELECTIONS_START';
export const PRODUCT_SELECTIONS_COMPLETE = 'PRODUCT_SELECTIONS_COMPLETE';

/**
 * @function          handlePickups
*/
export const PRODUCT_PICKUPS_START = 'PRODUCT_PICKUPS_START';
export const PRODUCT_PICKUPS_COMPLETE = 'PRODUCT_PICKUPS_COMPLETE';

/**
 * Reservation Action Types
 */

/**
 * @function          getReservation 
*/
export const RESERVATION_GET_START = 'RESERVATION_GET_START';
export const RESERVATION_GET_COMPLETE = 'RESERVATION_GET_COMPLETE';

/**
 * @function          getReservationSearch 
*/
export const RESERVATION_SEARCH_START = 'RESERVATION_SEARCH_START';
export const RESERVATION_SEARCH_COMPLETE = 'RESERVATION_SEARCH_COMPLETE';

/**
 * @function          getReservationTickets 
*/
export const RESERVATION_TICKET_START = 'RESERVATION_TICKET_START';
export const RESERVATION_TICKET_COMPLETE = 'RESERVATION_TICKET_COMPLETE';


/**
 * Search Action Types
 */

/**
 * @function          getDestinations 
*/
export const SEARCH_DESTINATION_START = 'SEARCH_DESTINATION_START';
export const SEARCH_DESTINATION_COMPLETE = 'SEARCH_DESTINATION_COMPLETE';
export const SEARCH_DESTINATION_FAILED = 'SEARCH_DESTINATION_FAILED';

/**
 * @function          getOperators 
*/
export const SEARCH_OPERATOR_START = 'SEARCH_OPERATOR_START';
export const SEARCH_OPERATOR_COMPLETE = 'SEARCH_OPERATOR_COMPLETE';

/**
 * @function          getOperatorProducts 
*/
export const SEARCH_OPERATOR_PRODUCT_START = 'SEARCH_OPERATOR_PRODUCT_START';
export const SEARCH_OPERATOR_PRODUCT_COMPLETE = 'SEARCH_OPERATOR_PRODUCT_COMPLETE';

/**
 * @function          getSearch 
*/
export const SEARCH_GET_START = 'SEARCH_GET_START';
export const SEARCH_GET_COMPLETE = 'SEARCH_GET_COMPLETE';
export const SEARCH_GET_FAILED = 'SEARCH_GET_FAILED';

/**
 * User Action Types
 */

/**
 * @function          postUserLogin 
*/
export const USER_LOGIN_START = 'USER_LOGIN_START';
export const USER_LOGIN_COMPLETE = 'USER_LOGIN_COMPLETE';

/**
 * @function          deleteUser 
*/
export const USER_DELETE_START = 'USER_DELETE_START';
export const USER_DELETE_COMPLETE = 'USER_DELETE_COMPLETE';

/**
 * @function          getUser 
*/
export const USER_GET_START = 'USER_GET_START';
export const USER_GET_COMPLETE = 'USER_GET_COMPLETE';

/**
 * @function          getUserList 
*/
export const USER_LIST_START = 'USER_LIST_START';
export const USER_LIST_COMPLETE = 'USER_LIST_COMPLETE';

/**
 * @function          logoutUser 
*/
export const USER_LOGOUT_START = 'USER_LOGOUT_START';
export const USER_LOGOUT_COMPLETE = 'USER_LOGOUT_COMPLETE';

/**
 * @function          postUserRegister 
*/
export const USER_REGISTER_START = 'USER_REGISTER_START';
export const USER_REGISTER_COMPLETE = 'USER_REGISTER_COMPLETE';