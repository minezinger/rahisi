import axios from 'store/actions/axios-config';
import {
  MESSAGE_CLEAR,
  MESSAGE_ERROR,
  DATA_COUNTRIES_START,
  DATA_COUNTRIES_COMPLETE,
  DATA_LANGUAGES_START,
  DATA_LANGUAGES_COMPLETE
} from 'store/actions/types';
import { errorHandler } from 'store/helpers';

/**
 * @name                getCountries
 * @description         Get a list of countries from the Livn API for usage with the filling out of passenger details.
 * @route               The Livn API route for this is https://*.livngds.com/livngds/api/public/countries
 */
export function getCountries() {
  return function (dispatch) {
    dispatch({ type: MESSAGE_CLEAR })
    dispatch({ type: DATA_COUNTRIES_START })
    axios.get('/public/countries')
      .then(response => dispatch({ type: DATA_COUNTRIES_COMPLETE, payload: response.data }))
      .catch(error => errorHandler(dispatch, error, MESSAGE_ERROR))
  }
}

/**
 * @name                getLanguages
 * @description         Get a list of languages from the Livn API for usage with the filling out of passenger details.
 * @route               The Livn API route for this is https://*.livngds.com/livngds/api/public/languages
 */
export function getLanguages() {
  return function (dispatch) {
    dispatch({ type: MESSAGE_CLEAR })
    dispatch({ type: DATA_LANGUAGES_START })
    axios.get('/public/languages')
      .then(response => dispatch({ type: DATA_LANGUAGES_COMPLETE, payload: response.data }))
      .catch(error => errorHandler(dispatch, error, MESSAGE_ERROR))
  }
}