import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import Hero from 'components/UI/Hero/Hero';

const Footer = ({ user: { current, isAuthenticated } }) => {
  const actionsMenu = isAuthenticated ? <aside className="menu">
    <ul className="menu-list">
      <li><Link to="/be/destination">Destinations</Link></li>
      <li><Link to="/be/operator">Operators</Link></li>
      <li><Link to="/be/reservations">Reservations</Link></li>
    </ul>
  </aside> : null;
  const userMenu = isAuthenticated ? <aside className="menu">
    <ul className="menu-list">
      <li><a href="https://gitlab.com/livnDevelopers/rahisi" rel="noopener noreferrer" target="_blank">GitLab Repository</a></li>
      <li><a href="https://dev1.livngds.com/livngds/api-docs/pdf/livn_api_docs.pdf" rel="noopener noreferrer" target="_blank">API Documentation PDF</a></li>
      <li><Link to={`/be/user/${current.id}`}>{current.name}</Link></li>
      <li><Link to="/logout">Logout</Link></li>
    </ul>
  </aside> : <aside className="menu">
      <ul className="menu-list">
        <li><Link to="/login">Login/Register</Link></li>
      </ul>
    </aside>;

  return <div>
    <div className="is-hidden-desktop">
      <Hero type="primary">
        {actionsMenu}
        {isAuthenticated ? <hr className="has-background-light" /> : null}
        {userMenu}
      </Hero>
    </div>

    <footer className="hero is-dark">
      <div className="hero-body">
        <div className="container">
          <div className="columns has-text-centered">

            <div className="column">
              <p>Created with care by the Livn Developers.</p>
            </div>
            <div className="column">
              <a href="http://www.livn.world">www.livn.world</a>
            </div>
            <div className="column">
              <a href="https://gitlab.com/livnDevelopers/rahisi" rel="noopener noreferrer" target="_blank">GitLab Repository</a>
            </div>

          </div>
        </div>
      </div>
    </footer>
  </div>
}

const mapStateToProps = state => ({
  user: state.user
})

export default connect(mapStateToProps)(Footer);
