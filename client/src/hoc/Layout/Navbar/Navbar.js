import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import LivnLogo from 'assets/images/logos/livn-developers.png';
import GitLabLogo from 'assets/images/logos/gitlab.png';
import IconPDF from 'assets/images/icons/pdf.png';

import './Navbar.css';

const Navbar = ({ user: { current, isAuthenticated } }) => {
  const actionsMenu = isAuthenticated ? <div className="navbar-menu">
    <div className="navbar-start">
      <Link className="navbar-item" to="/be/destination">Destinations</Link>
      <Link className="navbar-item" to="/be/operator">Operators</Link>
      <Link className="navbar-item" to="/be/reservations">Reservations</Link>
    </div>
  </div> : <div className="navbar-menu">
      <div className="navbar-start">
        <Link className="navbar-item" to="/about">The Livn API</Link>
        <Link className="navbar-item" to="/connect">Getting Connected</Link>
      </div>
    </div>;

  const gitLabLink = <div className="navbar-item">
    <a className="button is-light is-rounded is-small" href="https://gitlab.com/livnDevelopers/rahisi" rel="noopener noreferrer" target="_blank">
      <span className="navbar-icon">
        <img alt="Rahisi on GitLab" className="image" src={GitLabLogo} style={{ height: '18px' }} />
      </span>
      GitLab Repository
    </a>
  </div>;

  const pdfAPIGuideLink = <div className="navbar-item">
    <a className="button is-light is-rounded is-small" href="https://dev1.livngds.com/livngds/api-docs/pdf/livn_api_docs.pdf" rel="noopener noreferrer" target="_blank">
      <span className="navbar-icon">
        <img alt="Livn PDF API Guide" className="image" src={IconPDF} style={{ height: '18px' }} />
      </span>
      PDF API Guide
    </a>
  </div>;

  const userMenu = isAuthenticated ? <div className="navbar-end">
    {gitLabLink}
    {pdfAPIGuideLink}
    <Link className="navbar-item" to={`/be/user/${current.id}`}>{current.name}</Link>
    <Link className="navbar-item" to="/logout">Logout</Link>
  </div> : <div className="navbar-end">
      {gitLabLink}
      {pdfAPIGuideLink}
      <Link className="navbar-item" to="/login">Login/Register</Link>
    </div>;

  return <nav className="navbar is-dark has-background-dark" aria-label="main navigation">
    <div className="navbar-brand has-background-dark">
      <Link className="navbar-item" to="/"><img alt="Rahisi - Livn's Sample API Implementation" src={LivnLogo} /></Link>
    </div>
    {actionsMenu}
    <div className="navbar-menu">
      {userMenu}
    </div>
  </nav>
}

const mapStateToProps = state => ({
  user: state.user
})

export default connect(mapStateToProps)(Navbar);
