import React from 'react';

import Navbar from 'hoc/Layout/Navbar/Navbar';

import Hero from 'components/UI/Hero/Hero';

const Header = ({ currentLocation }) => {
  if (currentLocation === '/') {
    return <Hero containerClass="has-text-centered" head={<Navbar />} size="medium" type="primary">
      <h1 className="title is-size-1">Welcome to Rahisi</h1>
      <h3 className="subtitle is-size-3">Livn's Sample API Implementation</h3>
    </Hero>
  } else {
    return <header>
      <Navbar />
    </header>
  }
}

export default Header;
