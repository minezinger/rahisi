const boom = require('boom');

const index = require('./api/index');
const users = require('./api/users');
const products = require('./api/products');
const public = require('./api/public');
const carts = require('./api/carts');
const apiUser = require('./api/apiUser');
const operators = require('./api/operators');
const reservations = require('./api/reservations');

module.exports = app => {
  app.use('/api', index);
  app.use('/api/users', users);
  app.use('/api/products', products);
  app.use('/api/public', public);
  app.use('/api/carts', carts);
  app.use('/api/apiUser', apiUser);
  app.use('/api/operators', operators);
  app.use('/api/reservations', reservations);

  // catch 404 and forward it to error handler
  app.use(function (req, res, next) {
    next(boom.notFound());
  });
}