const ev = require('express-validation');
const express = require('express');
const router = express.Router();
const passport = require('passport');
const auth = passport.authenticate('jwt', {session: false});

const operatorController = require('../../controllers/operator');
const validator = require('../../controllers/validation/operator');

/**
 * @route         GET api/operators
 * @description   Retrieves all bookable operators connected to the API user's distributor.
 * @access        Private
 */
router.get('/', operatorController.getAll);

/**
 * @route         GET api/operators/:operatorId/products
 * @description   Returns bookable operator products
 * @access        Private
 */
router.get('/:operatorId/products', ev(validator.products), operatorController.products);

module.exports = router;
