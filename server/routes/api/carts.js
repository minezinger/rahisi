const ev = require('express-validation');
const express = require('express');
const router = express.Router();
const passport = require('passport');
const auth = passport.authenticate('jwt', {session: false});

const cartController = require('../../controllers/cart');
const validator = require('../../controllers/validation/cart');

/**
 * @route         POST api/carts
 * @description   Transmits product selection and pax details for checkout.
 * @access        Private
 */
router.post('/', ev(validator.post), cartController.post);

/**
 * @route         GET api/carts/:cartId
 * @description   Retrieves a cart by its id
 * @access        Private
 */
router.get('/:cartId', ev(validator.retrieve), cartController.retrieve);

/**
 * @route         GET api/carts/:cartId/checkout
 * @description   Initiates the actual checkout process
 * @access        Private
 */
router.get('/:cartId/checkout', ev(validator.checkout), cartController.checkout);

/**
 * @route         POST api/carts/:cartId/ccPayment
 * @description   Accepts credit card payment.
 * @access        Private
 */
router.post('/:cartId/ccPayment', ev(validator.ccPayment), cartController.ccPayment);

/**
 * @route         GET api/carts/:cartId/tickets
 * @description   Retrieves a collated PDF with all tickets for the specified cart.
 * @access        Private
 */
router.get('/:cartId/tickets', ev(validator.tickets), cartController.tickets);

/**
 * @route         GET api/carts/:cartId/retailTotals
 * @description   Manually requests the retail totals (gross, net and commission totals) for a specified cart.
 * @access        Private
 */
router.get('/:cartId/retailTotals', ev(validator.retailTotals), cartController.retailTotals);

module.exports = router;
