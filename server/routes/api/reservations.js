const ev = require('express-validation');
const express = require('express');
const router = express.Router();

const reservationController = require('../../controllers/reservation');
const validator = require('../../controllers/validation/reservation');

/**
 * @route         GET api/reservations/search
 * @description   Retrieves all reservations belonging to your API user's outlet group, by pax name/email, ignoring case and including partial matches.
 * @access        Private
 */
router.get('/search', ev(validator.search), reservationController.search);

/**
 * @route         GET api/reservations/:reservationId
 * @description   Retrieves a specific reservation.
 * @access        Private
 */
router.get('/:reservationId', ev(validator.retrieve), reservationController.retrieve);

/**
 * @route         GET api/reservations/:reservationId/tickets
 * @description   Retrieves a collated PDF with all tickets for the specified reservation.
 * @access        Private
 */
router.get('/:reservationId/tickets', ev(validator.tickets), reservationController.tickets);

module.exports = router;
