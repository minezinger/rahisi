const ev = require('express-validation');

const express = require('express');
const passport = require('passport');
const router = express.Router();
require('../../config/passport')(passport);

const userController = require('../../controllers/user');

const auth = passport.authenticate('jwt', {session: false});
const login = passport.authenticate('local', {session: false});

/**
 * @route         GET api/users/test
 * @description   Tests users route. 
 * @access        Public
 */
router.get('/test', (req, res) => res.json({
  message: 'Users route works.'
}));

/**
 * @route         GET api/users/list
 * @description   List all users
 * @access        Public
 */
router.get('/list', userController.list);

/**
 * @route         POST api/users/login
 * @description   Login user
 * @access        Public
 */
router.post('/login', login, userController.login);

/**
 * @route         POST api/users/register
 * @description   Register user
 * @access        Public
 */
router.post('/register', userController.create);

/**
 * @route         GET api/users/:userId
 * @description   Retrieve one user by their id
 * @access        Public
 */
router.get('/:userId', userController.retrieve);

module.exports = router;
