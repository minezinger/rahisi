const ev = require('express-validation');
const express = require('express');
const router = express.Router();
const passport = require('passport');
const auth = passport.authenticate('jwt', {session: false});

const productController = require('../../controllers/product');
const validator = require('../../controllers/validation/product');

/**
 * @route         GET api/products/search
 * @description   Searches for products
 * @access        Private
 */
router.get('/search', ev(validator.search), productController.search);

/**
 * @route         GET api/products/airportsAndCities
 * @description   retrieves a list of available airports and cities combined in one list
 * @access        Private
 */
router.get('/airportsAndCities', productController.airportsAndCities);

/**
 * @route         GET api/products/:productId
 * @description   retrieves a product by id
 * @access        Private
 */
router.get('/:productId', ev(validator.retrieve), productController.retrieve);

/**
 * @route         GET api/products/:productId/tnc
 * @description   retrieves the terms and conditions for a specific product
 * @access        Private
 */
router.get('/:productId/tnc', ev(validator.tnc), productController.tnc);

/**
 * @route         GET api/products/:productId/paxDetails
 * @description   Retrieves a specific product's customer data requirements.
 * @access        Private
 */
router.get('/:productId/paxDetails', ev(validator.paxDetails), productController.paxDetails);

/**
 * @route         GET api/products/:productId/checkAv
 * @description   Performs a live availability check for a specific product.
 * @access        Private
 */
router.get('/:productId/checkAv', ev(validator.checkAv), productController.checkAv);

/**
 * @route         GET api/products/:productId/checkRates
 * @description   Performs a live rates check for a specific product.
 * @access        Private
 */
router.get('/:productId/checkRates', ev(validator.checkRates), productController.checkRates);

/**
 * @route         GET api/products/:tourId/departures-multi
 * @description   Performs a departure check for an entire Tour, including where applicable all flavours and/or options, from our central cache. 
 *                Returns operating dates with available units, and if specified pricing information for all bookable products under the specified 
 *                top level tour ID.
 * @access        Private
 */
router.get('/:tourId/departures-multi', ev(validator.departuresMulti), productController.departuresMulti);

module.exports = router;
