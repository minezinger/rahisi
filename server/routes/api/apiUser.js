const express = require('express');
const router = express.Router();

const apiUserController = require('../../controllers/apiUser');

/**
 * @route         GET api/apiUser
 * @description   Retrieves the current apiUser
 * @access        Public
 */
router.get('/', apiUserController.apiUser);


module.exports = router;