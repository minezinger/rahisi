const express = require('express');
const router = express.Router();

/**
 * @route         GET api/
 * @description   Base api route.
 * @access        Public
 */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

module.exports = router;
