const expect = require('chai').expect;
const request = require('supertest');
let sandbox = require('sinon').createSandbox();
const config = require('../config/config');
const userConstants = require('../constants/user');
const jwt = require('jsonwebtoken');

const rewire = require('rewire');
let productController = rewire('../controllers/product');
let axios = productController.__get__('axios');

const models = require('../models');
const User = require('../models/index').User;
const {format, addDays} = require('date-fns');

const app = require('../app');


function generateToken(userId) {
  return 'Bearer ' + jwt.sign({sub: userId}, config.jwtSecret, {expiresIn: userConstants.TOKEN_EXPIRY});
}


describe('Product Tests', function () {
  beforeEach(async function () {
    // Removing tables
    await models.sequelize.sync({force: true, logging: false});

    // Seeding users
    await User.create({
      email: 'admin@test.com',
      name: 'Test Admin',
      password: 'password',
      role: userConstants.ROLE.USER
    });
  });

  afterEach(function () {
    sandbox.restore();
  });


  describe('GET /products/search', function () {
    it('should call axios method if the query values are valid', async function () {
      //Stub axios
      sandbox.stub(axios, 'get');

      const token = generateToken(1).toString();
      const futureDate = addDays(new Date(), 10);
      await request(app)
        .get(`/api/products/search?location=1,1&radius=50&startDate=${format(futureDate, 'YYYY-MM-DD')}&endDate=${format(futureDate, 'YYYY-MM-DD')}`)
        .set('Authorization', token);
      sandbox.assert.calledOnce(axios.get);
    });

    it('should not call axios method if the radius is missing', async function () {
      //Stub axios
      sandbox.stub(axios, 'get');

      const token = generateToken(1).toString();
      const response = await request(app)
        .get('/api/products/search?location=1,1')
        .set('Authorization', token);
      expect(response.statusCode).to.equal(400);
      expect(response.body.message).to.equal('Search by location or airport require a radius value specified.');
      sandbox.assert.notCalled(axios.get);
    });

    it('should not call axios method if the location and airport are passed together', async function () {
      //Stub axios
      sandbox.stub(axios, 'get');

      const token = generateToken(1).toString();
      const response = await request(app)
        .get('/api/products/search?location=1,1&radius=25&airport=SYD')
        .set('Authorization', token);
      expect(response.statusCode).to.equal(400);
      expect(response.body.message).to.equal('Search by location and airport cannot be used at the same time.');
      sandbox.assert.notCalled(axios.get);
    });

    it('should not call axios method if the startDate is in the past', async function () {
      //Stub axios
      sandbox.stub(axios, 'get');

      const token = generateToken(1).toString();
      const response = await request(app)
        .get('/api/products/search?location=1,1&radius=25&startDate=2000-01-01')
        .set('Authorization', token);
      expect(response.statusCode).to.equal(400);
      expect(response.body.message).to.equal('Start date cannot be in the past.');
      sandbox.assert.notCalled(axios.get);
    });

    it('should not call axios method if the startDate is after the endDate', async function () {
      //Stub axios
      sandbox.stub(axios, 'get');

      const token = generateToken(1).toString();
      const futureDate = addDays(new Date(), 10);
      const response = await request(app)
        .get(`/api/products/search?location=1,1&radius=25&startDate=${format(futureDate, 'YYYY-MM-DD')}&endDate=${format(new Date(), 'YYYY-MM-DD')}`)
        .set('Authorization', token);
      expect(response.statusCode).to.equal(400);
      expect(response.body.message).to.equal('End date cannot be before the start date.');
      sandbox.assert.notCalled(axios.get);
    });
  })


  describe('GET /products/:productId', function () {
    it('should call axios method if the query values are valid', async function () {
      //Stub axios    
      sandbox.stub(axios, 'get');

      const token = generateToken(1).toString();
      await request(app)
        .get('/api/products/1')
        .set('Authorization', token);
      sandbox.assert.calledOnce(axios.get);
    });

    it('should not call axios method if the query values are invalid', async function () {
      //Stub axios    
      sandbox.stub(axios, 'get');

      const token = generateToken(1).toString();
      const response = await request(app)
        .get('/api/products/a')
        .set('Authorization', token);
      expect(response.statusCode).to.equal(422);
      sandbox.assert.notCalled(axios.get);
    });
  })


  describe('GET /products/airportsAndCities', function () {
    it('should call axios method three times', async function () {
      //Stub axios    
      sandbox.stub(axios, 'get');

      const token = generateToken(1).toString();
      await request(app)
        .get('/api/products/airportsAndCities')
        .set('Authorization', token);
      sandbox.assert.calledThrice(axios.get);
    });
  });


  describe('GET /products/:tourId/departures-multi', function () {
    it('should call axios method once if query parameters are valid', async function () {
      //Stub axios    
      sandbox.stub(axios, 'get');
      const futureDate = addDays(new Date(), 10);
      const token = generateToken(1).toString();
      await request(app)
        .get(`/api/products/1/departures-multi?startDate=${format(futureDate, 'YYYY-MM-DD')}&endDate=${format(futureDate, 'YYYY-MM-DD')}&limit=5&minUnits=2`)
        .set('Authorization', token);
      sandbox.assert.calledOnce(axios.get);
    });

    it('should not call axios method if startDate is in the past', async function () {
      //Stub axios    
      sandbox.stub(axios, 'get');
      const token = generateToken(1).toString();
      const response = await request(app)
        .get('/api/products/1/departures-multi?startDate=2017-01-01')
        .set('Authorization', token);
      expect(response.statusCode).to.equal(400);
      expect(response.body.message).to.equal('Start date cannot be in the past.');
      sandbox.assert.notCalled(axios.get);
    });

    it('should not call axios method if startDate is after endDate', async function () {
      //Stub axios    
      sandbox.stub(axios, 'get');
      const futureDate = addDays(new Date(), 10);
      const token = generateToken(1).toString();
      const response = await request(app)
        .get(`/api/products/1/departures-multi?&startDate=${format(futureDate, 'YYYY-MM-DD')}&endDate=${format(new Date(), 'YYYY-MM-DD')}`)
        .set('Authorization', token);
      expect(response.statusCode).to.equal(400);
      expect(response.body.message).to.equal('End date cannot be before the start date.');
      sandbox.assert.notCalled(axios.get);
    });

    it('should not call axios method if limit value is invalid', async function () {
      //Stub axios    
      sandbox.stub(axios, 'get');
      const token = generateToken(1).toString();
      const response = await request(app)
        .get('/api/products/1/departures-multi?limit=0')
        .set('Authorization', token);
      expect(response.statusCode).to.equal(422);
      sandbox.assert.notCalled(axios.get);
    });

    it('should not call axios method if minUnits value is invalid', async function () {
      //Stub axios    
      sandbox.stub(axios, 'get');
      const token = generateToken(1).toString();
      const response = await request(app)
        .get('/api/products/1/departures-multi?limit=0')
        .set('Authorization', token);
      expect(response.statusCode).to.equal(422);
      sandbox.assert.notCalled(axios.get);
    });
  })


  describe('GET /products/:productId/tnc', function () {
    it('should call axios method once', async function () {
      //Stub axios    
      sandbox.stub(axios, 'get');

      const token = generateToken(1).toString();
      await request(app)
        .get('/api/products/1/tnc')
        .set('Authorization', token);
      sandbox.assert.calledOnce(axios.get);
    });

    it('should not call axios method if productId is invalid', async function () {
      //Stub axios    
      sandbox.stub(axios, 'get');

      const token = generateToken(1).toString();
      const response = await request(app)
        .get('/api/products/a/tnc')
        .set('Authorization', token);
      expect(response.statusCode).to.equal(422);
      sandbox.assert.notCalled(axios.get);
    });
  })


  describe('GET /products/:productId/paxDetails', function () {
    it('should call axios method once', async function () {
      //Stub axios    
      sandbox.stub(axios, 'get');

      const token = generateToken(1).toString();
      await request(app)
        .get('/api/products/1/paxDetails')
        .set('Authorization', token);
      sandbox.assert.calledOnce(axios.get);
    });

    it('should not call axios method if productId is invalid', async function () {
      //Stub axios    
      sandbox.stub(axios, 'get');

      const token = generateToken(1).toString();
      const response = await request(app)
        .get('/api/products/a/paxDetails')
        .set('Authorization', token);
      expect(response.statusCode).to.equal(422);
      sandbox.assert.notCalled(axios.get);
    });
  })


  describe('GET /products/:productId/checkAv', function () {
    it('should call axios method once if query values are valid', async function () {
      //Stub axios    
      sandbox.stub(axios, 'get');
      const futureDate = addDays(new Date(), 10);
      const token = generateToken(1).toString();
      await request(app)
        .get(`/api/products/1/checkAv?startDate=${format(futureDate, 'YYYY-MM-DD')}&requiredUnits=2`)
        .set('Authorization', token);
      sandbox.assert.calledOnce(axios.get);
    });

    it('should not call axios method if productId is invalid', async function () {
      //Stub axios    
      sandbox.stub(axios, 'get');

      const token = generateToken(1).toString();
      const response = await request(app)
        .get('/api/products/a/checkAv')
        .set('Authorization', token);
      expect(response.statusCode).to.equal(422);
      sandbox.assert.notCalled(axios.get);
    });

    it('should not call axios method if requiredUnits is missing', async function () {
      //Stub axios    
      sandbox.stub(axios, 'get');
      const futureDate = addDays(new Date(), 10);
      const token = generateToken(1).toString();
      const response = await request(app)
        .get(`/api/products/1/checkAv?startDate=${format(futureDate, 'YYYY-MM-DD')}`)
        .set('Authorization', token);
      expect(response.statusCode).to.equal(422);
      sandbox.assert.notCalled(axios.get);
    });

    it('should not call axios method if startDate is in the wrong format', async function () {
      //Stub axios    
      sandbox.stub(axios, 'get');
      const futureDate = addDays(new Date(), 10);
      const token = generateToken(1).toString();
      const response = await request(app)
        .get(`/api/products/1/checkAv?startDate=${format(futureDate, 'DD/MM/YYYY')}`)
        .set('Authorization', token);
      expect(response.statusCode).to.equal(422);
      sandbox.assert.notCalled(axios.get);
    });
  })


  describe('GET /products/:productId/checkRates', function () {
    it('should call axios method once if query values are valid', async function () {
      //Stub axios    
      sandbox.stub(axios, 'get');
      const futureDate = addDays(new Date(), 10);
      const token = generateToken(1).toString();
      await request(app)
        .get(`/api/products/1/checkRates?startDate=${format(futureDate, 'YYYY-MM-DD')}&paxAges=2,5`)
        .set('Authorization', token);
      sandbox.assert.calledOnce(axios.get);
    });

    it('should not call axios method if productId is invalid', async function () {
      //Stub axios    
      sandbox.stub(axios, 'get');

      const token = generateToken(1).toString();
      const response = await request(app)
        .get('/api/products/a/checkRates')
        .set('Authorization', token);
      expect(response.statusCode).to.equal(422);
      sandbox.assert.notCalled(axios.get);
    });

    it('should not call axios method if paxAges is missisng', async function () {
      //Stub axios    
      sandbox.stub(axios, 'get');
      const futureDate = addDays(new Date(), 10);
      const token = generateToken(1).toString();
      const response = await request(app)
        .get(`/api/products/1/checkRates?startDate=${format(futureDate, 'YYYY-MM-DD')}`)
        .set('Authorization', token);
      expect(response.statusCode).to.equal(422);
      sandbox.assert.notCalled(axios.get);
    });

    it('should not call axios method if startDate is in the wrong format', async function () {
      //Stub axios    
      sandbox.stub(axios, 'get');
      const futureDate = addDays(new Date(), 10);
      const token = generateToken(1).toString();
      const response = await request(app)
        .get(`/api/products/1/checkRates?startDate=${format(futureDate, 'DD/MM/YYYY')}`)
        .set('Authorization', token);
      expect(response.statusCode).to.equal(422);
      sandbox.assert.notCalled(axios.get);
    });
  })
});
