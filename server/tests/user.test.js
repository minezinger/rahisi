const expect = require('chai').expect;
const request = require('supertest');
const {beforeEach} = require('mocha');
const models = require('../models');
const User = require('../models/index').User;
const constant = require('../constants/user');

const app = require('../app');

describe('User Tests', () => {

  beforeEach(async () => {
    // Removing tables
    await models.sequelize.sync({force: true, logging: false});

    // Seeding users
    await User.create({
      email: 'admin@test.com',
      name: 'Test Admin',
      password: 'password',
      role: constant.ROLE.ADMINISTRATOR
    });
  });

  describe('POST /users/test', () => {
    it('should return a message string', (done) => {
      request(app)
        .get('/api/users/test')
        .set('Accept', 'application/json')
        .expect(200)
        .end(function (err, res) {
          if (err) {
            return done(err)
          }
          expect(res.body.message).to.equal('Users route works.');
          done();
        })
    })
  })

  describe('POST /users/register', () => {
    it('should return an error if the email is already in use', (done) => {
      request(app)
        .post('/api/users/register')
        .send({
          email: 'admin@test.com',
          name: 'Admin Test',
          password: 'password'
        })
        .set('Accept', 'application/json')
        .expect(412)
        .end(function (err, res) {
          if (err) {
            return done(err)
          }
          expect(res.body.message).to.equal('There is already an account with that email address.');
          done();
        });
    });

    it('should register a new user in the system', (done) => {
      request(app)
        .post('/api/users/register')
        .send({
          email: 'admin1@test.com',
          name: 'Admin 1 Test',
          password: 'password',
          role: 'USER'
        })
        .expect(201)
        .end(function (err, res) {
          if (err) {
            return done(err)
          }
          expect(res.body.message).to.equal('You have been registered');
          done();
        });
    });
  });

  describe('POST /users/login', () => {
    it('should login an existing user and send back a bearer token', async () => {
      const response = await request(app)
        .post('/api/users/login')
        .send({
          email: 'admin@test.com',
          password: 'password'
        });
      expect(response.statusCode).to.equal(200);
      expect(response.body.token).to.be.a('string');
      expect(response.body.token).to.include('Bearer');
    })

    it('should login an existing user and send back a token expiry of 3600 seconds', (done) => {
      request(app)
        .post('/api/users/login')
        .send({
          email: 'admin@test.com',
          password: 'password'
        })
        .set('Accept', 'application/json')
        .expect(200)
        .end(function (err, res) {
          if (err) {
            return done(err)
          }
          expect(res.body.tokenExpiry).to.equal(3600);
          done();
        })
    })

    it('should login an existing user and send back user details', (done) => {
      request(app)
        .post('/api/users/login')
        .send({
          email: 'admin@test.com',
          password: 'password'
        })
        .set('Accept', 'application/json')
        .expect(200)
        .end(function (err, res) {
          if (err) {
            return done(err)
          }
          expect(res.body.user).to.be.an('object');
          expect(res.body.user.email).to.equal('admin@test.com');
          expect(res.body.user.role).to.equal(constant.ROLE.ADMINISTRATOR);
          done();
        })
    })
  })

  describe('GET /users/list', () => {
    it('should return an array of users', (done) => {
      request(app)
        .get('/api/users/list')
        .set('Accept', 'application/json')
        .expect(200)
        .end(function (err, res) {
          if (err) {
            return done(err)
          }
          expect(res.body).to.be.lengthOf(1);
          done();
        })
    })
  })
});
