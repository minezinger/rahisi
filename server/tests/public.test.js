const request = require('supertest');
let sandbox = require('sinon').createSandbox();

const rewire = require('rewire');
let publicController = rewire('../controllers/public');
let axios = publicController.__get__('axios');

const app = require('../app');

describe('Public Tests', function () {

  afterEach(function () {
    sandbox.restore();
  });

  describe('GET /public/countries', function () {

    it('should call axios method', async function () {
      //Stub axios
      sandbox.stub(axios, 'get');
      await request(app)
        .get('/api/public/countries')
      sandbox.assert.calledOnce(axios.get);
    });
  })

  describe('GET /public/languages', function () {

    it('should call axios method', async function () {
      //Stub axios
      sandbox.stub(axios, 'get');
      await request(app)
        .get('/api/public/languages')
      sandbox.assert.calledOnce(axios.get);
    });
  })
});
