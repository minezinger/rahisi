const expect = require('chai').expect;
const request = require('supertest');
let sandbox = require('sinon').createSandbox();
const config = require('../config/config');
const userConstants = require('../constants/user');
const jwt = require('jsonwebtoken');

const rewire = require('rewire');
let cartController = rewire('../controllers/cart');
let axios = cartController.__get__('axios');

const models = require('../models');
const User = require('../models/index').User;

const app = require('../app');


function generateToken(userId) {
  return 'Bearer ' + jwt.sign({sub: userId}, config.jwtSecret, {expiresIn: userConstants.TOKEN_EXPIRY});
}


describe('Cart Tests', function () {
  beforeEach(async function () {
    // Removing tables
    await models.sequelize.sync({force: true, logging: false});

    // Seeding users
    await User.create({
      email: 'admin@test.com',
      name: 'Test Admin',
      password: 'password',
      role: userConstants.ROLE.USER
    });
  });

  afterEach(function () {
    sandbox.restore();
  });


  describe('POST /carts', function () {
    it('should call axios method if the cart data is valid', async function () {
      //Stub axios
      sandbox.stub(axios, 'post');

      const token = generateToken(1).toString();
      await request(app)
        .post('/api/carts')
        .send({
          paxes: [
            {
              salutation: 'MR',
              firstName: 'Adult 1',
              lastName: 'Adult 1',
              email: 'test@test.com',
              dob: '11/11/1980',
              phone: '11111111'
            }
          ],
          cartItems: [
            {
              paxIndex: 0,
              productId: 2887,
              productName: '9D Vietnam Tour',
              productCid: 38482,
              productDate: '2018-09-28',
              duration: 777600000
            }
          ]
        })
        .set('Authorization', token);
      sandbox.assert.calledOnce(axios.post);
    });

    it('should not call axios method if paxes cart data is invalid', async function () {
      //Stub axios
      sandbox.stub(axios, 'post');

      const token = generateToken(1).toString();
      const response = await request(app)
        .post('/api/carts')
        .send({
          paxes: [
            {
              // Missing salutation, firstName and lastName
              email: 'test@test.com',
              dob: '11/11/1980',
              phone: '11111111'
            }
          ],
          cartItems: [
            {
              paxIndex: 0,
              productId: 2887,
              productName: '9D Vietnam Tour',
              productCid: 38482,
              productDate: '2018-09-28',
              duration: 777600000
            }
          ]
        })
        .set('Authorization', token);
      expect(response.statusCode).to.equal(422);
      sandbox.assert.notCalled(axios.post);
    });

    it('should not call axios method if cartItems data is invalid', async function () {
      //Stub axios
      sandbox.stub(axios, 'post');

      const token = generateToken(1).toString();
      const response = await request(app)
        .post('/api/carts')
        .send({
          paxes: [
            {
              salutation: 'MR',
              firstName: 'Adult 1',
              lastName: 'Adult 1',
              email: 'test@test.com',
              dob: '11/11/1980',
              phone: '11111111'
            }
          ],
          cartItems: [
            {
              // Missing most of the required properties                
              duration: 777600000
            }
          ]
        })
        .set('Authorization', token);
      expect(response.statusCode).to.equal(422);
      sandbox.assert.notCalled(axios.post);
    });
  })


  describe('GET /carts/:cartId/checkout', function () {
    it('should call axios method once', async function () {
      //Stub axios
      sandbox.stub(axios, 'get');

      const token = generateToken(1).toString();
      await request(app)
        .get('/api/carts/1/checkout')
        .set('Authorization', token);
      sandbox.assert.calledOnce(axios.get);
    });

    it('should not call axios method if cartId is invalid', async function () {
      //Stub axios
      sandbox.stub(axios, 'get');

      const token = generateToken(1).toString();
      const response = await request(app)
        .get('/api/carts/a/checkout')
        .set('Authorization', token);
      expect(response.statusCode).to.equal(422);
      sandbox.assert.notCalled(axios.get);
    });
  })


  describe('GET /carts/:cartId', function () {
    it('should call axios method once if all parameters are correct', async function () {
      //Stub axios
      sandbox.stub(axios, 'get');

      const token = generateToken(1).toString();
      await request(app)
        .get('/api/carts/1?includeFullDetails=true')
        .set('Authorization', token);
      sandbox.assert.calledOnce(axios.get);
    });

    it('should not call axios method if cartId is invalid', async function () {
      //Stub axios
      sandbox.stub(axios, 'get');

      const token = generateToken(1).toString();
      const response = await request(app)
        .get('/api/carts/a?includeFullDetails=true')
        .set('Authorization', token);
      expect(response.statusCode).to.equal(422);
      sandbox.assert.notCalled(axios.get);
    });

    it('should not call axios method if includeFullDetails is invalid', async function () {
      //Stub axios
      sandbox.stub(axios, 'get');

      const token = generateToken(1).toString();
      const response = await request(app)
        .get('/api/carts/1?includeFullDetails=123')
        .set('Authorization', token);
      expect(response.statusCode).to.equal(422);
      sandbox.assert.notCalled(axios.get);
    });
  })


  describe('GET /carts/:cartId/tickets', function () {
    it('should call axios method once', async function () {
      //Stub axios
      sandbox.stub(axios, 'get').returns({
        response: {
          data: {
            test: 'test'
          }
        }
      });

      const token = generateToken(1).toString();
      await request(app)
        .get('/api/carts/1/tickets')
        .set('Authorization', token);
      sandbox.assert.calledOnce(axios.get);
    });

    it('should not call axios method if cartId is invalid', async function () {
      //Stub axios
      sandbox.stub(axios, 'get');

      const token = generateToken(1).toString();
      const response = await request(app)
        .get('/api/carts/a/tickets')
        .set('Authorization', token);
      expect(response.statusCode).to.equal(422);
      sandbox.assert.notCalled(axios.get);
    });
  })


  describe('POST /carts/:cartId/ccPayment', function () {
    // it('should call axios method once if all card details are correct', async function () {
    // TODO

    it('should not call axios method if currency is invalid', async function () {
      //Stub axios
      sandbox.stub(axios, 'post');

      const token = generateToken(1).toString();
      const response = await request(app)
        .post('/api/carts/1/ccPayment')
        .send({
          creditCardPayment: {
            currency: 'AU',
            amount: 1,
            expMonth: 11,
            expYear: 2017,
            number: '378282246310005',
            cvc: '123'
          }
        })
        .set('Authorization', token);
      expect(response.statusCode).to.equal(422);
      sandbox.assert.notCalled(axios.post);
    });

    it('should not call axios method if expMonth is invalid', async function () {
      //Stub axios
      sandbox.stub(axios, 'post');

      const token = generateToken(1).toString();
      const response = await request(app)
        .post('/api/carts/1/ccPayment')
        .send({
          creditCardPayment: {
            currency: 'AUD',
            amount: 1,
            expMonth: 13,
            expYear: 2017,
            number: '378282246310005',
            cvc: '123'
          }
        })
        .set('Authorization', token);
      expect(response.statusCode).to.equal(422);
      sandbox.assert.notCalled(axios.post);
    });

    it('should not call axios method if cvc is invalid', async function () {
      //Stub axios
      sandbox.stub(axios, 'post');

      const token = generateToken(1).toString();
      const response = await request(app)
        .post('/api/carts/1/ccPayment')
        .send({
          creditCardPayment: {
            currency: 'AUD',
            amount: 1,
            expMonth: 10,
            expYear: 2017,
            number: '378282246310005',
            cvc: 'abc'
          }
        })
        .set('Authorization', token);
      expect(response.statusCode).to.equal(422);
      sandbox.assert.notCalled(axios.post);
    });

    it('should not call axios method if number is invalid', async function () {
      //Stub axios
      sandbox.stub(axios, 'post');

      const token = generateToken(1).toString();
      const response = await request(app)
        .post('/api/carts/1/ccPayment')
        .send({
          creditCardPayment: {
            currency: 'AUD',
            amount: 1,
            expMonth: 10,
            expYear: 2017,
            number: 'abbbb246310005',
            cvc: '123'
          }
        })
        .set('Authorization', token);
      expect(response.statusCode).to.equal(422);
      sandbox.assert.notCalled(axios.post);
    });

    it('should not call axios method if amount is invalid', async function () {
      //Stub axios
      sandbox.stub(axios, 'post');

      const token = generateToken(1).toString();
      const response = await request(app)
        .post('/api/carts/1/ccPayment')
        .send({
          creditCardPayment: {
            currency: 'AUD',
            amount: -1,
            expMonth: 10,
            expYear: 2017,
            number: '378282246310005',
            cvc: '123'
          }
        })
        .set('Authorization', token);
      expect(response.statusCode).to.equal(422);
      sandbox.assert.notCalled(axios.post);
    });
  })
});
