const expect = require('chai').expect;
const request = require('supertest');
let sandbox = require('sinon').createSandbox();
const config = require('../config/config');
const userConstants = require('../constants/user');
const jwt = require('jsonwebtoken');

const rewire = require('rewire');
let operatorController = rewire('../controllers/operator');
let axios = operatorController.__get__('axios');

const models = require('../models');
const User = require('../models/index').User;

const app = require('../app');


function generateToken(userId) {
  return 'Bearer ' + jwt.sign({sub: userId}, config.jwtSecret, {expiresIn: userConstants.TOKEN_EXPIRY});
}


describe('Operator Tests', function () {
  beforeEach(async function () {
    // Removing tables
    await models.sequelize.sync({force: true, logging: false});

    // Seeding users
    await User.create({
      email: 'admin@test.com',
      name: 'Test Admin',
      password: 'password',
      role: userConstants.ROLE.USER
    });
  });

  afterEach(function () {
    sandbox.restore();
  });


  describe('GET /operators', function () {
    it('should call axios method', async function () {
      //Stub axios
      sandbox.stub(axios, 'get');

      const token = generateToken(1).toString();
      await request(app)
        .get('/api/operators')
        .set('Authorization', token);
      sandbox.assert.calledOnce(axios.get);
    });
  })


  describe('GET /operators/:operatorId/products', function () {
    it('should call axios method if the query values are valid', async function () {
      //Stub axios    
      sandbox.stub(axios, 'get');

      const token = generateToken(1).toString();
      await request(app)
        .get('/api/operators/1/products')
        .set('Authorization', token);
      sandbox.assert.calledOnce(axios.get);
    });

    it('should not call axios method if the query values are invalid', async function () {
      //Stub axios    
      sandbox.stub(axios, 'get');

      const token = generateToken(1).toString();
      const response = await request(app)
        .get('/api/operators/a/products')
        .set('Authorization', token);
      expect(response.statusCode).to.equal(422);
      sandbox.assert.notCalled(axios.get);
    });
  })
});