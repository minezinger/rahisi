const config = require('../config/config');
const axios = require('axios');
const apiRoutes = require('../constants/apiRoutes');
const boom = require('boom');
const {logger} = require('../config/logger');


/**
 * Post a new cart including product and pick-up selection and pax details. If supplied data passes pre-commit validation, 
 * you will receive back your cart with a newly assigned id. At the same time the system will perform a live availability 
 * and rates check in the background. Use getCart(cartId) to poll the cart and check for problems or status changes, 
 * including when the cart is ready to be taken through checkout.
 * 
 * @param req {Object} - The request.
 * @param res {Object} - The response. 
 * @param req.body {Object} - Cart object
 * @param next {Function} - Express next middleware function
 * 
 * @returns the cart with a newly assigned id.
 */
exports.post = async (req, res, next) => {
  try {
    const response = await axios.post(apiRoutes.CARTS_ROUTE, {'cart': req.body}, {
      auth: {username: config.apiUsername, password: config.apiPassword},
      timeout: apiRoutes.TIMEOUT_120 // After two minutes cancel the request if no response has been received.
    });
    logger.info({
      message: 'Posted cart to LIVN - success',
      cartId: response.data.cart.id,
      ip: req.ip
    });
    return res.send(response.data.cart);
  } catch (error) {
    if (!error.response) {
      return next(boom.failedDependency(`Unable to connect to the API, error: ${error.message}`));
    } else if (error.response.data.webApplicationException) {
      return next(boom.badRequest(error.response.data.webApplicationException.message));
    } else if (error.response.data.apiException) {
      return next(boom.badRequest(error.response.data.apiException.details.map((error) => {return error.description})));
    } else {
      return next(boom.badImplementation(error.message));
    }
  }
};


/**
 * Once a cart has been posted, pre-validated and live availability and prices checked, use this request to start the 
 * process of making confirmed reservations in the respective supplier reservation systems. 
 * Note: Depending on the API user's outlet group's relation to the distributor, a successfully checked out cart may still 
 * be deemed pending while awaiting a payment authorisation, or otherwise force a rollback/cancellation should it not be 
 * authorised within a specific time frame.
 * 
 * @param req {Object} - The request.
 * @param res {Object} - The response. 
 * @param req.params.cartId {integer}
 * @param next {Function} - Express next middleware function
 * 
 * @returns the cart with a new status code.
 * Notes: If parameter paymentAuthorisation is included, the value is used in lieu of a separate authorisePayment call.
 * The reservation process (same as the rates and availability checks following postCart) involves communication with our supplier
 * APIs and may take a while to complete. By default the checkout is handled synchronously, i.e. it will not return your response
 * until that reservation process is completed, unless the Synchronous header is used to explicitly opt for asynchronous processing,
 * in which case you need to poll the cart again, by calling getCart (ideally including the parameter includeFullDetails =
 * false), until its status changes to:
 * • PENDING_AUTHORISATION Proceed to authorisePayment
 * • PENDING_CREDIT_CARD_PAYMENT Proceed to makeCreditCardPayment
 * • COMPLETED Cart will include the individual reservations with the respective reservation items (one / pax & product)
 * • FAILED_CHECKOUT Check the problems element specifying the underlying cause(s) 
 */
exports.checkout = async (req, res, next) => {
  try {
    const response = await axios.get(apiRoutes.CARTS_ROUTE + `/${req.params.cartId}/checkout`, {
      auth: {username: config.apiUsername, password: config.apiPassword},
      timeout: apiRoutes.TIMEOUT_120 // After two minutes cancel the request if no response has been received.
    });
    logger.info({
      message: 'Checked-out cart - success',
      cartId: req.params.cartId,
      ip: req.ip
    });
    return res.send(response.data.cart);
  } catch (error) {
    if (!error.response) {
      return next(boom.failedDependency(`Unable to connect to the API, error: ${error.message}`));
    } else if (error.response.data.webApplicationException) {
      return next(boom.badRequest(error.response.data.webApplicationException.message));
    } else if (error.response.data.apiException) {
      return next(boom.badRequest(error.response.data.apiException.details.map((error) => {return error.description})));
    } else {
      return next(boom.badImplementation(error.message));
    }
  }
};


/**
 *  Returns the cart with the given id.
 * 
 * @param req {Object} - The request.
 * @param res {Object} - The response. 
 * @param req.params.cartId {integer}
 * @param next {Function} - Express next middleware function
 * 
 * @returns the cart object.
 * 
 */
exports.retrieve = async (req, res, next) => {
  try {
    const response = await axios.get(apiRoutes.CARTS_ROUTE + `/${req.params.cartId}`, {
      params: {
        includeFullDetails: req.query.includeFullDetails || 'true'
      },
      auth: {username: config.apiUsername, password: config.apiPassword},
      timeout: apiRoutes.TIMEOUT_120 // After two minutes cancel the request if no response has been received.
    });
    logger.info({
      message: 'Loaded cart from LIVN',
      cartId: req.params.cartId,
      ip: req.ip
    });
    return res.send(response.data.cart);
  } catch (error) {
    if (!error.response) {
      return next(boom.failedDependency(`Unable to connect to the API, error: ${error.message}`));
    } else if (error.response.data.webApplicationException) {
      return next(boom.badRequest(error.response.data.webApplicationException.message));
    } else if (error.response.data.apiException) {
      return next(boom.badRequest(error.response.data.apiException.details.map((error) => {return error.description})));
    } else {
      return next(boom.badImplementation(error.message));
    }
  }
};


/**
 *  processes the payment.
 * 
 * @param req {Object} - The request.
 * @param res {Object} - The response. 
 * @param req.body {object} - credit card details wrapped in creditCardPayment object
 * @param next {Function} - Express next middleware function
 * 
 * @returns the cart object after payment is completed.
 * 
 */
exports.ccPayment = async (req, res, next) => {
  // obtaining retails totals from the API
  let response = null;
  try {
    response = await getCartRetailTotals(req.params.cartId);
  } catch (error) {
    return next(error);
  }
  // comparing gross amount with the payment value
  if (response.netAmount === req.body.creditCardPayment.amount) {
    try {
      const response = await axios.post(apiRoutes.CARTS_ROUTE + `/${req.params.cartId}/ccPayment`, req.body, {
        auth: {username: config.apiUsername, password: config.apiPassword},
        timeout: apiRoutes.TIMEOUT_120 // After two minutes cancel the request if no response has been received.
      });
      logger.info({
        message: 'Paid cart',
        cartId: req.params.cartId,
        ip: req.ip
      });
      return res.send(response.data.cart);
    } catch (error) {
      if (!error.response) {
        return next(boom.failedDependency(`Unable to connect to the API, error: ${error.message}`));
      } else if (error.response.data.webApplicationException) {
        return next(boom.badRequest(error.response.data.webApplicationException.message));
      } else if (error.response.data.apiException) {
        return next(boom.badRequest(error.response.data.apiException.details.map((error) => {return error.description})));
      } else {
        return next(boom.badImplementation(error.message));
      }
    }
  } else {
    return next(boom.badRequest(`Gross amount: ${response.grossAmount} and amount received for payment: ${req.body.creditCardPayment.amount} don't match`));
  }
};


/**
 * Retrieves a collated PDF with all tickets for the specified cart
 */
exports.tickets = async (req, res, next) => {
  try {
    const response = await axios.get(apiRoutes.CARTS_ROUTE + `/${req.params.cartId}/tickets`, {
      auth: {username: config.apiUsername, password: config.apiPassword},
      responseType: 'arraybuffer',
      timeout: apiRoutes.TIMEOUT_120 // After two minutes cancel the request if no response has been received.
    });

    res.writeHead(200, {
      'Content-Type': 'application/pdf',
      'Content-disposition': 'attachment'
    });
    logger.info({
      message: 'Downloaded tickets',
      cartId: req.params.cartId,
      ip: req.ip
    });
    return res.end(response.data, 'binary');
  } catch (error) {
    if (!error.response) {
      return next(boom.failedDependency(`Unable to connect to the API, error: ${error.message}`));
    } else if (error.response.data.webApplicationException) {
      return next(boom.badRequest(error.response.data.webApplicationException.message));
    } else if (error.response.data.apiException) {
      return next(boom.badRequest(error.response.data.apiException.details.map((error) => {return error.description})));
    } else {
      return next(boom.badImplementation(error.message));
    }
  }
};


/**
 * Manually requests the retail totals (gross, net and commission totals) for a specified cart
 */
exports.retailTotals = async (req, res, next) => {
  try {
    const retailTotals = await getCartRetailTotals(req.params.cartId);
    logger.info({
      message: 'Requested retail totals',
      cartId: req.params.cartId,
      ip: req.ip
    });
    res.send(retailTotals);
  } catch (error) {
    return next(error);
  }
};


/**
 * Helper functions
 * 
 */

async function getCartRetailTotals(cartId) {
  try {
    const response = await axios.get(apiRoutes.CARTS_ROUTE + `/${cartId}/retailTotals`, {
      auth: {username: config.apiUsername, password: config.apiPassword},
      timeout: apiRoutes.TIMEOUT_120 // After two minutes cancel the request if no response has been received.
    });
    return response.data.retailTotals;
  } catch (error) {
    if (!error.response) {
      return boom.failedDependency(`Unable to connect to the API, error: ${error.message}`);
    } else if (error.response.data.webApplicationException) {
      return boom.badRequest(error.response.data.webApplicationException.message);
    } else if (error.response.data.apiException) {
      return boom.badRequest(error.response.data.apiException.details.map((error) => {return error.description}));
    } else {
      return boom.badImplementation(error.message);
    }
  }
}
