const config = require('../config/config');
const qs = require('querystring');
const axios = require('axios');
const apiRoutes = require('../constants/apiRoutes');
const sortBy = require('lodash/sortBy');
const {parse, isBefore, isPast, addMonths} = require('date-fns');
const boom = require('boom');
const {logger} = require('../config/logger');

/**
 * Search bookable products by multiple query parameters specified below.
 *
 * @param req {Object} - The request.
 * @param res {Object} - The response. 
 * @param req.query.startDate {String} - to filter search results to products, that have open, bookable departures after the specified date.
 * @param req.query.endDate {String} - to filter search results to products, that have open, bookable departures before the specified date.
 * @param req.query.location {String} - (optional, limits results by distance from reference coordinates).
 * @param req.query.airport {String} - (optional, limits results by distance from given airport. Format is {3-character IATA airport code}
 * @param req.query.radius {integer} - Limits results by distance from given location or airport
 * @param next {Function} - Express next middleware function
 * 
 * @returns A list of bookable products ordered by duration and then by name
 */
exports.search = async (req, res, next) => {
  const startDate = parse(req.query.startDate);
  const endDate = parse(req.query.endDate);
  const dateInAYear = addMonths(new Date(), 12);

  if (startDate && isPast(startDate)) {
    return next(boom.badRequest('Start date cannot be in the past.'));
  }
  if (endDate && isBefore(endDate, startDate)) {
    return next(boom.badRequest('End date cannot be before the start date.'));
  }
  if (endDate && isBefore(dateInAYear, endDate)) {
    return next(boom.badRequest('Date range must not extend more than one year into the future.'));
  }
  if (req.query.location && req.query.airport) {
    return next(boom.badRequest('Search by location and airport cannot be used at the same time.'));
  }
  if ((req.query.location || req.query.airport) && !req.query.radius) {
    return next(boom.badRequest('Search by location or airport require a radius value specified.'));
  }
  // for fts we check the keywords against all possible available Search fields using an (OR)
  let ftsString = undefined;
  if (req.query.fts) {
    ftsString = `name='${req.query.fts}'|highlights='${req.query.fts}'|tags='${req.query.fts}'|desc='${req.query.fts}'`;
  }

  /**
   * Building request query
   * treeview:
   * If set to true, call returns results as a tree, grouping bookable products, which are flavours or optional add-ons of the same tour or 
   * activity, under a common parent product. Nested child products will only contain fields, which override properties otherwise inherited 
   * from the parent. If parameter is omitted or set to false, the response is a flat list containing only directly bookable products. 
   * 
   * Please refer to the API documentation for more details on the other query parameters available:
   * - includeFullDetails
   * - matchAll
   * - matchPartial
   * - includeDisabled
   * - includeOptions z
   * - includePickups
   * - includeManualBooking
   */

  try {
    let response = await axios.get(apiRoutes.PRODUCTS_ROUTE + 'search', {
      params: {
        treeView: true,
        location: (req.query.location) ? req.query.location + ',' + req.query.radius || 100 : undefined,
        airport: (req.query.airport) ? req.query.airport + ',' + req.query.radius || 100 : undefined,
        startDate: req.query.startDate,
        endDate: req.query.endDate,
        requiredUnits: req.query.requiredUnits || 1,
        fts: ftsString
      },
      auth: {username: config.apiUsername, password: config.apiPassword},
      timeout: apiRoutes.TIMEOUT_120 // After two minutes cancel the request if no response has been received.
    });
    // Adding stock photos for product with no images
    for (let product of response.data.List) {
      if (!product.images || (product.images && product.images.length === 0)) {
        /** retrieving stock images from API
         * GET /public/stockPhotos/products
         * Retrieve a list of intentionally generic stock photos, that can be used in place of missing product images.
         *  */
        const responsePhotos = await axios.get(apiRoutes.PUBLIC_ROUTE + 'stockPhotos/products', {
          timeout: apiRoutes.TIMEOUT_120 // After two minutes cancel the request if no response has been received.
        });
        const randNumbers = generateRandomNumbers(3); // generate 3 unique random numbers between 0 - 10
        product.images = [
          responsePhotos.data.List[randNumbers[0]],
          responsePhotos.data.List[randNumbers[1]],
          responsePhotos.data.List[randNumbers[2]]
        ];
      }
    }
    logger.info(
      {
        message: 'Searched for products', searchValues: {
          location: req.query.location || req.query.airport,
          startDate: req.query.startDate,
          endDate: req.query.endDate,
          requiredUnits: req.query.requiredUnits
        }, ip: req.ip
      });
    return res.send(sortBy(response.data.List, ['duration', 'name']));
  } catch (error) {
    if (!error.response) {
      return next(boom.failedDependency(`Unable to connect to the API, error: ${error.message}`));
    } else if (error.response.data.webApplicationException) {
      return next(boom.badRequest(error.response.data.webApplicationException.message));
    } else {
      return next(boom.badImplementation(error.message));
    }
  }
};

/**
 *  Retrieves a specific product given its id
 * 
 * @param req {Object} - The request.
 * @param res {Object} - The response. 
 * @param req.params.productId {integer}
 * @param next {Function} - Express next middleware function
 * 
 * @returns The product information
 */
exports.retrieve = async (req, res, next) => {
  try {
    let response = await axios.get(apiRoutes.PRODUCTS_ROUTE + req.params.productId, {
      params: {
        includePickups: req.query.includePickups || 'true'
      },
      auth: {username: config.apiUsername, password: config.apiPassword},
      timeout: apiRoutes.TIMEOUT_120 // After two minutes cancel the request if no response has been received.
    });
    if (!response.data.product.images || (response.data.product.images && response.data.product.images.length === 0)) {
      /** retrieving stock images from API
       * GET /public/stockPhotos/products
       * Retrieve a list of intentionally generic stock photos, that can be used in place of missing product images.
       *  */
      const responsePhotos = await axios.get(apiRoutes.PUBLIC_ROUTE + 'stockPhotos/products', {
        timeout: apiRoutes.TIMEOUT_120 // After two minutes cancel the request if no response has been received.
      });
      const randNumbers = generateRandomNumbers(3); // generate 3 unique random numbers between 0 - 10
      response.data.product.images = [
        responsePhotos.data.List[randNumbers[0]],
        responsePhotos.data.List[randNumbers[1]],
        responsePhotos.data.List[randNumbers[2]]
      ];
    }
    logger.info({
      message: 'Loaded product',
      productId: response.data.product.id,
      productName: response.data.product.name,
      operatorName: response.data.product.operatorName,
      ip: req.ip
    });
    return res.send(response.data.product);
  } catch (error) {
    if (!error.response) {
      return next(boom.failedDependency(`Unable to connect to the API, error: ${error.message}`));
    } else if (error.response.data.webApplicationException) {
      return next(boom.badRequest(error.response.data.webApplicationException.message));
    } else {
      return next(boom.badImplementation(error.message));
    }
  }
};


/**
 * Retrieves a list of all airports, in whose vicinity (radius 100km) and nearest cities (population > 50,000) for all 
 * bookable products.
 * 
 * @param next {Function} - Express next middleware function
 * 
 * @returns List of airports and cities ordered by country and then city name
 */
module.exports.airportsAndCities = async (req, res, next) => {
  let [cityList, airports, countries] = [null, null];
  try {
    [cityList, airports, countries] = await Promise.all([
      axios.get(apiRoutes.PRODUCTS_ROUTE + 'cities', {
        auth: {username: config.apiUsername, password: config.apiPassword},
        timeout: apiRoutes.TIMEOUT_120 // After two minutes cancel the request if no response has been received.
      }),
      axios.get(apiRoutes.PRODUCTS_ROUTE + 'airports', {
        auth: {username: config.apiUsername, password: config.apiPassword},
        timeout: apiRoutes.TIMEOUT_120 // After two minutes cancel the request if no response has been received.
      }),
      axios.get(apiRoutes.PUBLIC_ROUTE + 'countries', {
        timeout: apiRoutes.TIMEOUT_120 // After two minutes cancel the request if no response has been received.
      })
    ]);
  } catch (error) {
    if (!error.response) {
      return next(boom.failedDependency(`Unable to connect to the API, error: ${error.message}`));
    } else if (error.response.data.webApplicationException) {
      return next(boom.badRequest(error.response.data.webApplicationException.message));
    } else {
      return next(boom.badImplementation(error.message));
    }
  }
  try {
    let {List} = countries.data;

    let cities = [];
    for (let index = List.length - 1; index >= 0; index--) {
      let country = List[index];
      let city = {
        country: country.names.en, // retrieving country english name (en)
        code: country.code,
        destinations: []
      };

      // populating city destinations from city
      for (let c of cityList.data.List) {
        if (c.country === country.code) {
          city.destinations.push({
            coordinates: c.latitude + ',' + c.longitude,
            name: c.name,
            state: c.state
          });
        }
      }
      // populating city destinations from airports
      for (let airport of airports.data.List) {
        if (airport.country === country.code) {
          city.destinations.push({
            iata: airport.iata,
            city: airport.city,
            coordinates: airport.latitude + ',' + airport.longitude,
            name: airport.name
          });
        }
      }
      // Check if the city has destinations before adding it to the list of cities.
      if (city.destinations.length > 0) {
        cities.push(city);
      }
      city.destinations = sortBy(city.destinations, 'name');
    }
    cities = sortBy(cities, 'country');
    return res.send(cities);
  } catch (error) {
    return next(boom.badImplementation(`We couldn't load the airports and cities this time. Details: ${error.message}`));
  }
}


/**
 *  Retrieves a specific product's terms and conditions, if they are defined.
 * 
 * @param req {Object} - The request.
 * @param res {Object} - The response. 
 * @param req.params.productId {integer}
 * @param next {Function} - Express next middleware function
 * 
 * @returns terms and conditions object for the product
 */
exports.tnc = async (req, res, next) => {
  try {
    const response = await axios.get(`${apiRoutes.PRODUCTS_ROUTE}${req.params.productId}/tnc`, {
      auth: {username: config.apiUsername, password: config.apiPassword},
      timeout: apiRoutes.TIMEOUT_120 // After two minutes cancel the request if no response has been received.
    });
    logger.info({
      message: 'Loaded Terms and conditions',
      productId: req.params.productId,
      ip: req.ip
    });
    return res.send(response.data);
  } catch (error) {
    if (!error.response) {
      return next(boom.failedDependency(`Unable to connect to the API, error: ${error.message}`));
    } else if (error.response.data.webApplicationException) {
      if (error.response.data.webApplicationException.status === 404) {
        return next(boom.notFound(`No terms and conditions found for productId: ${req.params.productId}`));
      } else {
        return next(boom.badRequest(error.response.data.webApplicationException.message));
      }
    } else {
      return next(boom.badImplementation(error.message));
    }
  }
};


/**
 *  Retrieves a specific product's customer data requirements.
 * 
 * @param req {Object} - The request.
 * @param res {Object} - The response. 
 * @param req.params.productId {integer}
 * @param next {Function} - Express next middleware function
 * 
 * @returns specific product's customer data requirements
 */
exports.paxDetails = async (req, res, next) => {
  try {
    const response = await axios.get(`${apiRoutes.PRODUCTS_ROUTE}${req.params.productId}/paxDetails`, {
      auth: {username: config.apiUsername, password: config.apiPassword},
      timeout: apiRoutes.TIMEOUT_120 // After two minutes cancel the request if no response has been received.
    });
    logger.info({
      message: 'Retrieved pax details for product',
      productId: req.params.productId,
      ip: req.ip
    });
    return res.send(response.data);
  } catch (error) {
    if (!error.response) {
      return next(boom.failedDependency(`Unable to connect to the API, error: ${error.message}`));
    } else if (error.response.data.webApplicationException) {
      return next(boom.badRequest(error.response.data.webApplicationException.message));
    } else {
      return next(boom.badImplementation(error.message));
    }
  }
};


/**
 * Performs a departure check for an entire Tour, including where applicable all flavours and/or options, from our central cache.  * 
 * 
 * @param req {Object} - The request.
 * @param res {Object} - The response. 
 * @param req.query.startDate {String} - First date to check (yyyy-MM-dd) 
 * @param req.query.endDate {String} - Last date to check (yyyy-MM-dd)
 * @param req.query.includeRates {boolean} - Include pricing information
 * @param req.query.limit {integer} - Limit the maximum number of results.
 * @param req.query.minUnits {integer}  - Only return results that have at least this number of available units
 * @param next {Function} - Express next middleware function
 * 
 * @returns operating dates with available units, and if specified pricing information for all bookable products under the specified
 * top level tour ID.
 */
exports.departuresMulti = async (req, res, next) => {

  // Validating the request query fields
  const startDate = parse(req.query.startDate);
  const endDate = parse(req.query.endDate);

  if (startDate && isPast(startDate)) {
    return next(boom.badRequest('Start date cannot be in the past.'));
  }
  if (endDate && isBefore(endDate, startDate)) {
    return next(boom.badRequest('End date cannot be before the start date.'));
  }

  try {
    const response = await axios.get(`${apiRoutes.PRODUCTS_ROUTE}${req.params.tourId}/departures-multi`, {
      params: {
        includeRates: true,
        limit: req.query.limit,
        minUnits: req.query.minUnits,
        startDate: req.query.startDate,
        endDate: req.query.endDate
      },
      auth: {username: config.apiUsername, password: config.apiPassword},
      timeout: apiRoutes.TIMEOUT_120 // After two minutes cancel the request if no response has been received.
    });
    return res.send(response.data.List);
  } catch (error) {
    if (!error.response) {
      return next(boom.failedDependency(`Unable to connect to the API, error: ${error.message}`));
    } else if (error.response.data.webApplicationException) {
      return next(boom.badRequest(error.response.data.webApplicationException.message));
    } else {
      return next(boom.badImplementation(error.message));
    }
  }
};


/**
 * Unfortunately some of our suppliers' reservation systems do/can not supply us with an exact number of available units on a given date, 
 * but instead require us to inquire for a given number of units and respond as to whether or not there is sufficient availability. 
 * As a result our availability check also requires you to specify the number of required units and responses will always and primarily 
 * be a boolean type verdict. In case we do retrieve the actual number of available units, we will of course pass this information on as 
 * part of the check response.
 * 
 * @param req {Object} - The request.
 * @param res {Object} - The response. 
 * @param req.params.productId {integer}
 * @param req.query.startDate {String} - First date to check (yyyy-MM-dd) 
 * @param req.query.endDate {String} - Last date to check (yyyy-MM-dd)
 * @param req.query.requiredUnits {integer} - number of required units
 * @param next {Function} - Express next middleware function
 * 
 * @returns an object indicating if there is availability for the specified product
 */
exports.checkAv = async (req, res, next) => {
  try {
    const response = await axios.get(`${apiRoutes.PRODUCTS_ROUTE}${req.params.productId}/checkAv`, {
      params: {
        startDate: req.query.startDate,
        endDate: req.query.endDate || req.query.startDate, // use start date if endDate is not provided
        requiredUnits: req.query.requiredUnits || 1
      },
      auth: {username: config.apiUsername, password: config.apiPassword},
      timeout: apiRoutes.TIMEOUT_120 // After two minutes cancel the request if no response has been received.
    });
    for (let i = response.data.availabilityCheck.availabilities.length - 1; i >= 0; i--) {
      let availability = response.data.availabilityCheck.availabilities[i];
      if (availability.available === false || availability.checkFailed === true) {
        response.data.availabilityCheck.availabilities.splice(i, 1);
      }
    }
    logger.info({
      message: 'Checked for availabilities', productId: req.params.productId,
      startDate: req.query.startDate,
      endDate: req.query.endDate || req.query.startDate,
      requiredUnits: req.query.requiredUnits || 1,
      ip: req.ip
    });
    return res.send(response.data.availabilityCheck.availabilities);
  } catch (error) {
    if (!error.response) {
      return next(boom.failedDependency(`Unable to connect to the API, error: ${error.message}`));
    } else if (error.response.data.webApplicationException) {
      return next(boom.badRequest(error.response.data.webApplicationException.message));
    } else {
      return next(boom.badImplementation(error.message));
    }
  }
};


/**
 * Performs a live rates check for a specific product.
 * 
 * @param req {Object} - The request.
 * @param res {Object} - The response. 
 * @param req.params.productId {integer}
 * @param req.query.startDate {String} - First date to check (yyyy-MM-dd) 
 * @param req.query.endDate {String} - Last date to check (yyyy-MM-dd)
 * @param req.query.paxAges {integer|array} - Array of the required pax ages
 * @param next {Function} - Express next middleware function
 * 
 * @returns an object indicating the rates for the different pax ages
 */
exports.checkRates = async (req, res, next) => {
  if (req.query.paxAges) {
    let values = req.query.paxAges.split(',');
    if (values.length === 0) {
      return next(boom.badRequest(`Invalid paxAges string: ${req.query.paxAges}, they should be comma separated, e.g. 38,35,7,1`));
    }
  }
  try {
    const response = await axios.get(`${apiRoutes.PRODUCTS_ROUTE}${req.params.productId}/checkRates`, {
      params: {
        startDate: req.query.startDate,
        endDate: req.query.endDate || req.query.startDate, // use startDate if endDate is not provided
        paxAges: req.query.paxAges || '35' // defaults to adult (35 years)
      },
      auth: {username: config.apiUsername, password: config.apiPassword},
      timeout: apiRoutes.TIMEOUT_120 // After two minutes cancel the request if no response has been received.
    });
    for (let i = response.data.ratesCheck.ratesDates.length - 1; i >= 0; i--) {
      let rate = response.data.ratesCheck.ratesDates[i];
      if (rate.checkFailed === true || rate.hasUsableRates === false) {
        response.data.ratesCheck.ratesDates.splice(i, 1);
      } else {
        rate.currency = response.data.ratesCheck.retailCurrency;
      }
    }
    logger.info({
      message: 'Checked for rates', productId: req.params.productId,
      startDate: req.query.startDate,
      endDate: req.query.endDate || req.query.startDate,
      paxAges: req.query.paxAges || '35',
      ip: req.ip
    });
    return res.send(response.data.ratesCheck.ratesDates);
  } catch (error) {
    if (!error.response) {
      return next(boom.failedDependency(`Unable to connect to the API, error: ${error.message}`));
    } else if (error.response.data.webApplicationException) {
      return next(boom.badRequest(error.response.data.webApplicationException.message));
    } else {
      return next(boom.badImplementation(error.message));
    }
  }
};


// -------------------------------------------------------------- Helper functions ------------------------------------------------------------


/**
 * Function that returns a variable number of unique random numbers between 0 and 9
 *  
 * @param count {integer} - number of unique random numbers between 0 and 9 to generate
 * 
 * @returns an array of {count} unique random numbers between 0 and 9
 */
function generateRandomNumbers(count) {
  const a = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
  let random = [];
  for (let n = 0; n < count; ++n) {
    var i = Math.floor(Math.random() * (9 - n));
    random.push(a[i]);
    a[i] = a[9 - n];
  }
  return random;
}
