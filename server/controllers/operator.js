const config = require('../config/config');
const axios = require('axios');
const sortBy = require('lodash/sortBy');
const apiRoutes = require('../constants/apiRoutes');
const boom = require('boom');
const {logger} = require('../config/logger');

/**
 *  Retrieves all bookable products for a specific operator.
 * 
 * @param req {Object} - The request.
 * @param res {Object} - The response. 
 * @param req.params.operatorId {integer}
 * @param next {Function} - Express next middleware function
 * 
 * @returns a list of all bookable products for the specified operator sorted by duration and then name.
 */
exports.products = async (req, res, next) => {
  /**
 * Building request query
 * - treeview:
 * If set to true, call returns results as a tree, grouping bookable products, which are flavours or optional add-ons of the same tour or activity, 
 * under a common parent product. Nested child products will only contain fields, which override properties otherwise inherited from the parent. 
 * If parameter is omitted or set to false, the response is a flat list containing only directly bookable products. 
 * - includeOptions:
 * Include optional add-ons, that can only booked in conjunction with a parent product. 
 */

  try {
    let response = await axios.get(apiRoutes.OPERATORS_ROUTE + `/${req.params.operatorId}/products`, {
      params: {
        treeView: true,
        includeOptions: false
      },
      auth: {username: config.apiUsername, password: config.apiPassword},
      timeout: apiRoutes.TIMEOUT_120 // After two minutes cancel the request if no response has been received.
    });
    // Adding stock photos for product with no images
    for (let product of response.data.List) {
      if (!product.images || (product.images && product.images.length === 0)) {
        /** retrieving stock images from API
         * GET /public/stockPhotos/products
         * Retrieve a list of intentionally generic stock photos, that can be used in place of missing product images.
         *  */
        const responsePhotos = await axios.get(apiRoutes.PUBLIC_ROUTE + 'stockPhotos/products', {
          timeout: apiRoutes.TIMEOUT_120 // After two minutes cancel the request if no response has been received.
        });
        const randNumbers = generateRandomNumbers(3); // generate 3 unique random numbers between 0 - 10
        product.images = [
          responsePhotos.data.List[randNumbers[0]],
          responsePhotos.data.List[randNumbers[1]],
          responsePhotos.data.List[randNumbers[2]]
        ];
      }
    }
    logger.info({
      message: 'Searched for operator products',
      operatorId: req.params.operatorId,
      ip: req.ip
    });
    return res.send(sortBy(response.data.List, ['duration', 'name']));
  } catch (error) {
    if (!error.response) {
      return next(boom.failedDependency(`Unable to connect to the API, error: ${error.message}`));
    } else if (error.response.data.webApplicationException) {
      return next(boom.badRequest(error.response.data.webApplicationException.message));
    } else {
      return next(boom.badImplementation(error.message));
    }
  }
};


/**
 *  Retrieves all operators connected to the API user's distributor.
 * 
 * @param req {Object} - The request.
 * @param res {Object} - The response.
 * @param next {Function} - Express next middleware function
 * 
 * @returns a list of all the operators connected to the API user's distributor sorted by name.
 */
exports.getAll = async (req, res, next) => {
  try {
    let response = await axios.get(apiRoutes.OPERATORS_ROUTE, {
      auth: {username: config.apiUsername, password: config.apiPassword},
      timeout: apiRoutes.TIMEOUT_120 // After two minutes cancel the request if no response has been received.
    });
    logger.info({
      message: 'Loaded operator list',
      ip: req.ip
    });
    return res.send(sortBy(response.data.List, 'name'));
  } catch (error) {
    if (!error.response) {
      return next(boom.failedDependency(`Unable to connect to the API, error: ${error.message}`));
    } else if (error.response.data.webApplicationException) {
      return next(boom.badRequest(error.response.data.webApplicationException.message));
    } else {
      return next(boom.badImplementation(error.message));
    }
  }
};
