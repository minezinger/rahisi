const joi = require('joi');
const Extension = require('joi-date-extensions');
const Joi = joi.extend(Extension);

/**
 * This file defines the validation rules for all the values received from the client in a request call.
 */

module.exports.post = {
  /* Cart contents */
  body: {
    paxes: joi.array().items(joi.object().keys({
      age: joi.number().integer().min(1),
      salutation: joi.string().valid(['MR', 'MRS', 'MISS', 'MS']).required(),
      firstName: joi.string().required(),
      lastName: joi.string().required(),
      address1: joi.string().max(120),
      city: joi.string().max(70),
      state: joi.string().max(70),
      postcode: joi.string().max(15),
      country: joi.string().length(2),
      language: joi.string().length(2),
      nationality: joi.string().length(2),
      email: joi.string().email().required(),
      phone: joi.string(),
      mobile: joi.string(),
      dob: joi.string()
    })).min(1).required(),
    cartItems: joi.array().items(joi.object().keys({
      paxIndex: joi.number().integer().min(0).required(),
      productName: joi.string(),
      paxNote: joi.string().max(250),
      operatorNote: joi.string().max(250),
      productId: joi.number().integer().min(1).required(),
      productCid: joi.number().integer().min(1),
      productDate: Joi.date().format('YYYY-MM-DD'),
      pickupId: joi.number().integer().min(1),
      duration: joi.number().positive(),
    }).min(1).required())
  }
};

module.exports.checkout = {
  params: {
    /* id of the cart */
    cartId: joi.number().integer().min(1).required()
  },
};

module.exports.retrieve = {
  params: {
    /* id of the cart */
    cartId: joi.number().integer().min(1).required()
  },
  query: {
    // If set to false, it will not include full details (true by default)
    includeFullDetails: joi.string().valid(['false', 'true'])
  }
};

module.exports.tickets = {
  params: {
    /* id of the cart */
    cartId: joi.number().integer().min(1).required()
  },
};

module.exports.ccPayment = {
  params: {
    /* id of the cart */
    cartId: joi.number().integer().min(1).required()
  },  
  body: {
    /* Credit card details */
    creditCardPayment: joi.object().keys({
      currency: joi.string().length(3).required(),
      amount: joi.number().min(0.0).required(),
      expMonth: joi.number().integer().min(1).max(12).required(),
      expYear: joi.number().integer().required(),
      number: joi.string().creditCard().required(),
      cvc: joi.number().integer().required()
    }).required()
  }
}

module.exports.retailTotals = {
  params: {
    /* id of the cart */
    cartId: joi.number().integer().min(1).required()
  },
};