const joi = require('joi');
const Extension = require('joi-date-extensions');
const Joi = joi.extend(Extension);

/**
 * This file defines the validation rules for all the values received from the client in a request call.
 */

module.exports.search = {
  query: {
    /*A full text search query. The search key must be in single quotes. The available Search fields are tags, name, desc 
      and highlights. The query syntax is {searchField}={'searchValue'}}. To chain criteria use "+" for AND and use "|" 
      for OR e.g fts=desc='Harbour Bridge'+tags='Adventure'
    */
    fts: joi.string(),

    // Search by geographic coordinates
    location: joi.string(),

    // Search by IATA airport code 
    airport: joi.string().length(3),

    // Radius in km used for location or airport search (min valid value is 0)
    radius: joi.number().integer().min(0),

    /* If set, limit search results to products that operate and have availability on or after this date, depending on 
       second parameter endDate. Format (yyyy-MM-dd)
    */
    startDate: Joi.date().format('YYYY-MM-DD'),

    /* Include this to limit results to those products, that have been added or modified since a specific time instant, 
       including where the change was made on the parent tour or operator. Format: (yyyy-MM-dd)
    */
    endDate: Joi.date().format('YYYY-MM-DD'),

    /* Used in conjunction with startDate (and optionally endDate), to specify the minimum number of units of the product 
      required on any suitable departure date, for it to be included in the search results. (1-10)
    */
    requiredUnits: joi.number().integer().min(1).max(10)
  }
}

module.exports.retrieve = {
  params: {
    /* id of the product
    */
    productId: joi.number().integer().min(1).required()
  },
  query: {
    // If set to false, it will not include pickups (true by default)
    includePickups: joi.string().valid(['false', 'true'])
  }
}

module.exports.tnc = {
  params: {
    /* id of the product
    */
    productId: joi.number().integer().min(1).required()
  },
}

module.exports.paxDetails = {
  params: {
    /* id of the product
    */
    productId: joi.number().integer().min(1).required()
  },
}

module.exports.departuresMulti = {
  params: {
    /* id of the tour
    */
    tourId: joi.number().integer().min(1).required()
  },
  query: {
    // Only return results that have at least this number of available units 
    minUnits: joi.number().integer().min(1),

    // Limit the maximum number of results.
    limit: joi.number().integer().min(1),

    // If set, First date to check (yyyy-MM-dd)    
    startDate: Joi.date().format('YYYY-MM-DD'),

    // If set, Last date to check (yyyy-MM-dd)
    endDate: Joi.date().format('YYYY-MM-DD')
  }
}

module.exports.checkAv = {
  params: {
    /* id of the product
    */
    productId: joi.number().integer().min(1).required()
  },
  query: {
    // If set, First date to check (yyyy-MM-dd)    
    startDate: Joi.date().format('YYYY-MM-DD').required(),

    // If set, Last date to check (yyyy-MM-dd)
    endDate: Joi.date().format('YYYY-MM-DD'),

    // Units of the product to check availability for. (1-10) 
    requiredUnits: joi.number().integer().min(1).max(10).required()
  }
}

module.exports.checkRates = {
  params: {
    /* id of the product
    */
    productId: joi.number().integer().min(1).required()
  },
  query: {
    // If set, First date to check (yyyy-MM-dd)    
    startDate: Joi.date().format('YYYY-MM-DD').required(),

    // If set, Last date to check (yyyy-MM-dd)
    endDate: Joi.date().format('YYYY-MM-DD'),

    // Pax ages (0-125), comma separated, e.g. 38,35,7,1
    paxAges: joi.string().required()
  }
}
