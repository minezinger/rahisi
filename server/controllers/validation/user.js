const joi = require('joi');
const constant = require('../constants/user');


/**
 * This file defines the validation rules for all the values received from the client in a request call.
 */

module.exports.create = {
  body: {
    email: joi.string().email().required(),
    name: joi.string().required(),
    password: joi.string().required().min(6)
  }
}

module.exports.login = {
  body: {
    email: joi.string().email().required(),
    password: joi.string().required().min(6)
  }
}

module.exports.retrieve = {
  body: {
    id: joi.number().required()
  }
}