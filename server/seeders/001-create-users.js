'use strict';

const constant = require('../constants/user');
const bcrypt = require('bcryptjs');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'Users',
      [
        {
          email: 'test@livn.world',
          name: 'Test Admin',
          password: bcrypt.hashSync('password', bcrypt.genSaltSync(10), null),
          role: constant.ROLE.ADMINISTRATOR,
          createdAt: Sequelize.literal('NOW()'),
          updatedAt: Sequelize.literal('NOW()')
        }
      ],
      {ignoreDuplicates: true}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Users', null, {});
  }
};
