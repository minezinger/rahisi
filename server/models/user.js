const bcrypt = require('bcryptjs');
const constant = require('../constants/user');

/**
 * Sequelize model for the User
 */


module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    email: {
      type: DataTypes.STRING(120),
      allowNull: false,
      isLowercase: true,
      unique: true,
      isEmail: true
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING(50)
    },
    password: {
      allowNull: false,
      type: DataTypes.STRING(100)
    },
    role: {
      type: DataTypes.ENUM,
      values: [
        constant.ROLE.ADMINISTRATOR,
        constant.ROLE.USER
      ]
    }
  },
    {
      indexes: [{fields: ['email']}]
    });

  User.addHook('beforeCreate', (user) => {
    if (user.password) {
      user.password = bcrypt.hashSync(user.password, bcrypt.genSaltSync(10), null);
    }
  });

  User.addHook('beforeUpdate', (user) => {
    if (user.password) {
      user.password = bcrypt.hashSync(user.password, bcrypt.genSaltSync(10), null);
    }
  });

  User.prototype.verifyPassword = async function (candidatePassword) {
    return await bcrypt.compare(candidatePassword, this.password);
  };

  return User;
}
