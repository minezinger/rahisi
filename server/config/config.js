// All the credentials and secrets are loaded via environment variables set in docker-compose file

module.exports = {  
  jwtSecret: process.env.jwt_secret,
  apiUsername: process.env.api_username,
  apiPassword: process.env.api_password,

  development: {
    // Database credentials
    database: process.env.db_name,    
    username: process.env.db_user,
    password: process.env.db_pass,
    logging: false, // No sequelize ORM logging
    host: process.env.db_url,
    operatorsAliases: false, // disabled sequelize operator aliases in order to improve security
    port: process.env.db_port,
    provider: 'pg',
    dialect: 'postgres'
  },
  test: {
    // Test database credentials
    username: process.env.db_user_test,
    password: process.env.db_pass_test,
    operatorsAliases: false, // disabled sequelize operator aliases in order to improve security
    database: process.env.db_name_test,
    logging: false, // No sequelize ORM logging
    host: process.env.db_url_test,
    port: process.env.db_port,    
    provider: 'pg',
    dialect: 'postgres'
  }
}