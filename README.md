# Rahisi

In case you are wondering, **Rahisi** means easy, facile or inexpensive in Swahili, as Livn developers we are very much into rahisi. 
We believe that the distribution of tours and activities should be easy, that building a client application becomes facile when you can trust the API and that starting a whole new project should be inexpensive.

As fellow coders we also know that documentation is dry and time-consuming to read, so we decided to share this demo application project with added spice.

We have combined a base implementation with help texts and code samples for you to explore, and we welcome anyone who is willing to contribute, suggest improvements or likes to get involved in other ways.

We divided the project into two sections (server and client) and to make things easier from a developer's perspective, the application can be deployed using Docker.

## Table of Contents
- [Rahisi](#rahisi)
	- [Table of Contents](#table-of-contents)
- [Docker project](#docker-project)
	- [How to run the demo application](#how-to-run-the-demo-application)
		- [Requirements](#requirements)
		- [Deployment steps](#deployment-steps)
	- [Node Express Server](#node-express-server)
		- [Folder Structure](#folder-structure)
	- [Purpose of the PostgreSQL database](#purpose-of-the-postgresql-database)
	- [Appendix](#appendix)
		- [Included Packages](#included-packages)
			- [Development Packages](#development-packages)
- [React Client](#react-client)
	- [Folder Structure](#folder-structure-1)
	- [Demo Elements](#demo-elements)
		- [Routing with react-router-dom](#routing-with-react-router-dom)
	- [Using Redux state](#using-redux-state)
	- [Simple imports](#simple-imports)
	- [Package Decisions/Notes](#package-decisionsnotes)
		- [formik? Why not redux-form?](#formik-why-not-redux-form)
	- [Appendix](#appendix-1)
		- [Included Packages](#included-packages-1)
		- [Development Packages](#development-packages-1)

# Docker project
**Docker version used:** `Version 2.0.0.0-win78 (28905), Channel: stable, Build: c404a62`

## How to run the demo application
The application was created in Docker using containers for easier deployment, in total three containers were used:

 1. Node Express Server
 2. React client
 3. PostgreSQL database
 
The PostgreSQL container was created with the sole purpose of integrating user management into the solution, i.e. create, update, and delete users, but **it is not required** in order to connect to the Livn API.

### Requirements
|Software|Purpose|Version|website
|--|--|--|--|
| Git |Used to clone the GitLab repository|Recommended version 2.19 or higher|[link](https://git-scm.com/downloads)
|Docker|Used to deploy the application|Recommended version 2.0.0.0-win78 (28905) or higher| [link](https://store.docker.com/search?offering=community&type=edition)

The application can be deployed in any system that can run Docker (Windows, Mac, Linux, AWS).

### Deployment steps
1. Clone the Rahisi project by executing the following command:

    `git clone https://gitlab.com/livnDevelopers/rahisi.git`

    Once the project is cloned, you can see that the application is divided in a *server* section and a *client* section indicated by different folders under the aforementioned names and some required Docker configuration files.

	All the configuration parameters and credentials are injected into the code by using environment variables located in the root folder of the project under `docker-compose.yml file -> environment section` for both server and client.
	
	#### Server and client environment variables

		// Server
	    - NODE_ENV=development              //(environment set for development)
	    - api_username=*****                //(Replace with your LIVN API username)
	    - api_password=*****                //(Replace with your LIVN API password)
	    - jwt_secret=sock.profile.peat      //(Default JWT secret)
	    - db_name=template-db               //(Default container postgres SQL database name)
	    - db_pass=tinder.woollen.farther    //(Default container postgres SQL database password)
	    - db_port=5432                      //(Default container postgres SQL database port number)
	    - db_url=db                         //(Default container postgres SQL database url -> pointing to the db Docker container)
	    - db_user=postgres                  //(Default container postgres SQL database user)

	    // Client
	    - NODE_ENV=development              //(environment set for development)

	**You will be required to enter your own credentials for `api_username` and `api_password` from the server environment variables section**. These are the credentials required to access the LIVN API. Feel free to modify all the other key-value pairs in order to accommodate your needs.
	**Note:** If you change any of the db_***** values, make sure to update the `db` service section of the same file accordingly.

2.  After `api_username` and `api_password` are configured, in order to run the application, execute the following command:

    ```docker-compose -f "docker-compose.yml" up -d --build```

	This command builds the docker images and starts both the server and client.

3. In order to view the running application, please open a web browser of your choice and navigate to:

	```http://localhost:3000/```
    
	You will be presented with the welcome page where you can log in as a pre-seeded admin account or you can register a new user if desired.
After logging in, you will be presented with our booking flow created for this demo application including thorough descriptions for each of the booking steps.

	In the next sections, you will find more details on each of the containers created, such as the folder structure for both the client and server, and also the packages used in development.

## Node Express Server

### Folder Structure

- **config**: App configuration files.
- **constants**: Constants used thorough the application.
- **controllers**: Request managers for all the different routes.
- **validation**: Validation rules for the body, params, query, headers and cookies of a request.
- **middleware**: Middleware functions.
- **migrations**: Set of migrations files used to keep track of changes to the database. (Only create user table migration was applied).
- **models**: Stores all the mappings between models and database tables.
- **routes**: Contains all the routes of the API.
- **seeders**: Contains a file that seeds initial or required data to the database. (Only creates admin user).
- **tests**: Contains all the integration tests for the application.

## Purpose of the PostgreSQL database

The purpose of our PostgreSQL database is to manage our own users. This is not necessary for using the Livn API - it is merely an example of how the one Livn API user can be used for an entire store of agents with varying roles. However, because we display a copy of this project publicy with open registration (which is entirely optional), we need to do some user table housekeeping now and then. Therefore, on the first of every month, we wipe all the users except for the main test@livn.world account.

## Appendix

### Included Packages

- **axios** ([GitHub page](https://github.com/axios/axios)): Promise based HTTP client for the browser.
- **bcryptjs** ([GitHub page](https://github.com/dcodeIO/bcrypt.js#readme)): Optimized bcrypt in JavaScript with zero dependencies.
- **body-parser** ([GitHub page](https://github.com/expressjs/body-parser#readme)): Node.js body parsing middleware.
- **boom** ([GitHub page](https://github.com/hapijs/boom)): HTTP-friendly error objects.
- **cors** ([GitHub page](https://github.com/dcodeIO/bcrypt.js#readme)): package for providing a Connect/Express middleware that can be used to enable CORS with various options.
- **date-fns** ([GitHub page](https://github.com/date-fns/date-fns#readme)): Modern JavaScript date utility library.
- **debug** ([GitHub page](https://github.com/visionmedia/debug#readme)): A tiny JavaScript debugging utility modelled after Node.js core's debugging technique.
- **express** ([GitHub page](https://github.com/expressjs/express)): Fast, minimalist web framework for Node.
- **express-validation** ([GitHub page](https://github.com/andrewkeig/express-validation)): Middleware that validates the body, params, query, headers of a request.
- **helmet** ([GitHub page](https://github.com/helmetjs/helmet)): Help secure Express applications with various HTTP headers.
- **joi** ([GitHub page](https://github.com/hapijs/joi)): Object schema validation used by express-validation.
- **joi-date-extensions** ([GitHub page](https://github.com/hapijs/joi-date-extensions)): Joi extension for dates.
- **jsonwebtoken** ([GitHub page](https://github.com/auth0/node-jsonwebtoken#readme)): Creates JWT.
- **lodash** ([GitHub page](https://github.com/lodash/lodash)): A modern JavaScript utility library delivering modularity, performance & extras.
- **winston** ([GitHub page](https://github.com/winstonjs/winston)): A simple and universal logging library with support for multiple transports.
- **moment-timezone** ([GitHub page](https://github.com/moment/moment-timezone)): Timezone support for moment.js.
- **passport** ([GitHub page](https://github.com/jaredhanson/passport)): Simple, unobtrusive authentication for Node.js.
- **pg** ([GitHub page](https://github.com/brianc/node-postgres)): PostgreSQL client for node, used by Sequelize.
- **pg-hstore** ([GitHub page](https://github.com/scarney81/pg-hstore)): A node package for serializing and de-serializing JSON data to hstore format, used by Sequelize.
- **sequelize** ([GitHub page](https://github.com/sequelize/sequelize#readme)): An easy-to-use multi SQL dialect ORM for Node.js.

#### Development Packages

- **chai** ([GitHub page](https://github.com/chaijs/chai)): BDD / TDD assertion framework for node.js.
- **eslint** ([GitHub page](https://github.com/eslint/eslint)): A fully pluggable tool for identifying and reporting on patterns in JavaScript.
- **mocha** ([GitHub page](https://github.com/mochajs/mocha)): Simple, flexible, fun javascript test framework for node.js.
- **nodemon** ([GitHub page](https://github.com/remy/nodemon)): Monitor for any changes in your node.js application and automatically restart the server.
- **rewire** ([GitHub page](https://github.com/jhnns/rewire)): Easy monkey-patching for node.js unit tests.
- **sinon** ([GitHub page](https://github.com/sinonjs/sinon)): Test spies, stubs and mocks for JavaScript.
- **supertest** ([GitHub page](https://github.com/visionmedia/supertest#readme)): HTTT assertions module.

# React Client

## Folder Structure

- **assets**: This will include folders such as "fonts", "images", "styles", and any downloaded packages that don't come via npm.
- **components**: All the reusable components. These should be functional components - no manipulation of the application state within these.
- **containers**: All the "pages". These often contain and manipulate parts of the application state.
- **hoc** (higher order component):
- **_Aux**: `_Aux.js` allows us to return multiple separate elements as opposed to one single element (without having to return an array).
- **Layout**: All the main layout elements, e.g. the Header and Footer.
- **routes**: All the route files which are used to render the various routes. We start with one, `routes.js`, but it's possible to add more.
- **store**: All the action files. There is a separate "types" file for containing all action types and an `actions.js` file which is used to export all the actions. The `actions.js` file is for the sake of clarity - an easier overview of all actions that there are - because the individual action files themselves can become quite large and not the best to browse through.
- **reducers**: All the reducer files. There is an `reducers.js` file which is used to export all the reducers.

## Demo Elements

### Routing with react-router-dom

The routes available are determined as arrays of routes in `src/routes/routes.js` and `src/routes/admin.js`. Each route requires the following information:

```json
{
	component: Home,
	exact: true,
	path: '/'
}
```

But more details can be housed here. For example, if you want to use the same array of routes to render a menu where each item has an icon, you could add `icon: 'user-alt'` or something similar, depending on your method of handling these icons. However, you will have to complete the set up for these menu item icons yourself.

Nevertheless, for those two main sets of routes to work, they are included in `src/App.js` as:

```jsx
<BrowserRouter>
<Switch>
<Route  path="/admin"  component={Admin} />
<Route  path="/"  component={Index} />
</Switch>
</BrowserRouter>
```

Then the arrays of routes are mapped over in `src/containers/Index.js`.

```jsx
{ routes.map(route  => {
	return <Route
	key={route.id}
	path={route.path}
	exact={route.exact}
	component={route.component}
/>})}
```

For more detailed explanations, see the documentation on the [React Training website](https://reacttraining.com/react-router/web/guides/philosophy).

## Using Redux state

The main actions files that you will need to be aware of are `src/store/actions/actions.js` and `src/store/actions/types.js`. The former exports all of the actions created in the other files for ease of routing and organisation. The latter contains all of the action types, treated like constants, to help ensure consistency. Organise these however you wish, but I recommend commenting them enough so that it's easy to browse through them.

The main reducers file that you will need to be aware of is `src/store/reducers/reducers.js`. Like the `actions.js`, this is where all of the individual reducers are exported. However, using `combineReducers` here is required to ensure that all of your reducers are included in the application. The `rootReducer` that is exported from this file gets picked up in `src/store/store.js` and `createStore` is applied to it. The `store` that is exported from there gets picked up in `src/index.js` where it's included in the render method via `<Provider store={store}>`.

In order to ensure that everything works from this point on however, all you need to do is make sure that every action you create is exported in `actions.js`, for example:

```jsx
export {
	changeName,
	increaseClicks,
	resetClicks
} from  './user';
```
And every reducer you create is included in `reducers.js`, for example:

```jsx
const appReducer =  combineReducers({
	user: userReducer
})
```

Optionally, I would also recommend using the aforementioned `types.js` to keep track of your action types. The format for this usually takes the form of the following:

```js
export  const USER_ACTION_TYPE =  'USER_ACTION_TYPE';
```

You will see that `connect` is required and that the items coming from state can then be mapped to props like so:

```js
const  mapStateToProps  =  state  => ({
	user: state.user
})
```

For more detailed explanations, see the documentation for [Redux](https://redux.js.org/basics), in particular, [[Redux] Usage with React](https://redux.js.org/basics/usage-with-react).

## Simple imports

This demo app has been set up so that imports are really simple. For example, instead of this:

```js
import Buttom from '../../../components/UI/Button/Button';
```

We can do this:

```js
import Buttom from 'components/UI/Button/Button';
```

This is because of a few elements which allow the development version of the client to know it can use the root at all times. Firstly, we have installed `eslint-plugin-import`. Then, in the root folder of the client, we set `NODE_PATH=src/` in a `.env` file and added a `jsconfig.json` with the following:

```json
{
  "compilerOptions": {
    "baseUrl": ".",
      "paths": {
      "*": ["src/*"]
    }
  }
}
```

## Package Decisions/Notes

### formik? Why not redux-form?

- See [React State versus Redux State](#react-state-versus-redux-state).
- redux-form calls your entire top-level Redux reducer multiple times ON EVERY SINGLE KEYSTROKE. This is fine for small apps, but as your app grows, input latency will continue to increase.
- redux-form is 22.5 kB minified gzipped. formik is 7.8 kB.

For more detailed explanations, see the [formik documentation on GitHub](https://github.com/jaredpalmer/formik).

## Appendix

### Included Packages

The following packages are included in this demo app.

- **axios** ([GitHub page](https://github.com/axios/axios)): Promise based HTTP client for the browser.
- **enzyme** ([GitHub page](https://github.com/airbnb/enzyme)): Enzyme is a JavaScript Testing utility for React that makes it easier to assert, manipulate, and traverse your React Components' output.
- **enzyme-adapter-react-16** ([GitHub page](https://github.com/airbnb/enzyme/tree/master/packages/enzyme-adapter-react-16))
- **formik** ([GitHub page](https://github.com/jaredpalmer/formik)): Formik is a small library that helps you with the 3 most annoying parts: getting values in and out of form state, validation and error messages, and handling form submission.ormik will keep things organized--making testing, refactoring, and reasoning about your forms a breeze.
- **highlight.js** ([GitHub page](https://github.com/highlightjs/highlight.js)): A yntax highlighter written in JavaScript.
- **jest** ([GitHub page](https://github.com/facebook/jest)): Complete and ready to set-up JavaScript testing solution. Fast interactive watch mode runs only test files related to changed files and is optimized to give signal quickly.
- **lodash** ([GitHub page](https://github.com/lodash/lodash)): Lodash makes JavaScript easier by taking the hassle out of working with arrays, numbers, objects, strings, etc.
- **moment** ([GitHub page](https://github.com/moment/moment/)): Parse, validate, manipulate, and display dates and times in JavaScript.
- **react** ([GitHub page](https://github.com/facebook/react)): A declarative, efficient, and flexible JavaScript library for building user interfaces.
- **react-dom** ([GitHub page](https://github.com/facebook/react/tree/master/packages/react-dom)): The entry point to the DOM and server renderers for React.
- **react-redux** ([GitHub page](https://github.com/reduxjs/react-redux)): Official React bindings for Redux.
- **react-router-dom** ([GitHub page](https://github.com/ReactTraining/react-router))
- **react-scripts** ([GitHub page](https://github.com/nearform/react-scripts)): Create React apps with no build configuration.
- **react-syntax-highlighter** ([GitHub page](https://github.com/conorhastings/react-syntax-highlighter)): A syntax highlighting component for React using the seriously super amazing lowlight and refractor by wooorm.
- **redux** ([GitHub page](https://github.com/reduxjs/redux)): Redux is a predictable state container for JavaScript apps. It helps you write applications that behave consistently, run in different environments (client, server, and native), and are easy to test. On top of that, it provides a great developer experience, such as live code editing combined with a time traveling debugger.
- **redux-thunk** ([GitHub page](https://github.com/reduxjs/redux-thunk)): Thunk middleware for Redux.
- **yup** ([GitHub page](https://github.com/jquense/yup)): Yup is a JavaScript object schema validator and object parser. The API and style is heavily inspired by Joi, which is an amazing library but is generally too large and difficult to package for use in a browser.

### Development Packages

- **eslint-plugin-import** ([GitHub page](https://github.com/benmosher/eslint-plugin-import)): Supports linting of ES2015+ (ES6+) import/export syntax and prevents issues with misspelling of file paths and import names.
- **redux-immutable-state-invariant** ([GitHub page](https://github.com/leoasis/redux-immutable-state-invariant)): Redux middleware that spits an error on you when you try to mutate your state either inside a dispatch or between dispatches.